#!/bin/bash

filename_neu='/var/www/html/ameisen/status.html'
captionStyle=' class="caption"'
filter_1='gekauft'
filter_2='Zucker'
filter_3='entfernt'
filter_4='Anzucht'

tail_limit='-n 50'

echo '<html>' > $filename_neu
echo '<head>' >> $filename_neu
echo '<title>Ameisen-Status</title>' >> $filename_neu
echo '<meta charset="utf-8">' >> $filename_neu
echo '<meta http-equiv="refresh" content="300">' >> $filename_neu
cat /root/scripts/webWriter.style.css >> $filename_neu
echo '</head>' >> $filename_neu

echo '<body>' >> $filename_neu
echo '<div style="display: flex;">' >> $filename_neu
echo '<a class="btn" href="ankaufkurse.html" target="_blank">AmeisenAnkaufKurse</a><br /><br />' >> $filename_neu
echo '<a class="btn" href="wachstum.html" target="_blank">Ameisenwachstum</a><br /><br />' >> $filename_neu
echo '<a class="btn" href="anzucht.html" target="_blank">Anzucht</a><br /><br />' >> $filename_neu
echo '</div>' >> $filename_neu
echo '<pre>' >> $filename_neu
echo '<p style="border: 1px solid; margin: 5px; padding: 10px;">' >> $filename_neu
echo '<span class="caption">Farming Accounts</span>' >> $filename_neu
tail -20 /root/AntWebWorker.log >> $filename_neu
echo '</p>' >> $filename_neu
tac /root/EatenByAnts_default.log >> $filename_neu
echo '</pre>' >> $filename_neu
echo '</body>' >> $filename_neu
echo '</html>' >> $filename_neu
