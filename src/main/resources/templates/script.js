//<!--
function toggleGraph(id)
{
	var button = document.getElementById("button" + id);
	var polyline = document.getElementById("polyline" + id);
	if (polyline.style.display === "none")
	{
		button.style.backgroundSize = "100%";
		polyline.style.display = "block";
	}
	else
	{
		button.style.backgroundSize = "0";
		polyline.style.display = "none";
	}
}
function toggleGraphBackground(id, off)
{
	var background = document.getElementById("polygon" + id);
	if (off === '0')
	{
		background.style.fillOpacity = "0.0";
	}
	else
	{
		background.style.fillOpacity = [OPACITY];
	}
}
function openContent(evt, contentName)
{
	// Declare all variables
	var i, tabcontent, tablinks;

	// Get all elements with class="tabcontent" and hide them
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++)
	{
		tabcontent[i].style.display = "none";
	}

	// Get all elements with class="tablinks" and remove the class "active"
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++)
	{
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}

	// Show the current tab, and add an "active" class to the button that opened the tab
	document.getElementById(contentName).style.display = "block";
	evt.currentTarget.className += " active";
}
//-->