package pa.soft.http;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Formicarium;
import pa.soft.data.entities.ameisenzimmer.details.FutterItem;
import pa.soft.data.entities.internet.Immobilie;
import pa.soft.data.entities.internet.details.KleinanzeigeAmeisen;
import pa.soft.data.managers.SpielerManager;
import pa.soft.data.types.FutterType;
import pa.soft.processors.Util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Dispatcher
{
	public static Document feedZucker(Spieler player, Formicarium formicarium)
	{
		return feedZuckerMenge(player, formicarium, 1);
	}

	public static Document feedSamen(Spieler player, Formicarium formicarium)
	{
		return feedSamenMenge(player, formicarium, 1);
	}

	public static Document feedProtein(Spieler player, Formicarium formicarium)
	{
		return feedProteinMenge(player, formicarium, 1);
	}

	public static Document feedBlätter(Spieler player, Formicarium formicarium)
	{
		return feedBlätterMenge(player, formicarium, 1);
	}

	public static Document feedZuckerMenge(Spieler player, Formicarium formicarium, Integer menge)
	{
		return feedTypeMenge(player, formicarium, menge, FutterType.ZUCKER);

	}

	public static Document feedSamenMenge(Spieler player, Formicarium formicarium, Integer menge)
	{
		return feedTypeMenge(player, formicarium, menge, FutterType.SAMEN);

	}

	public static Document feedProteinMenge(Spieler player, Formicarium formicarium, Integer menge)
	{
		return feedTypeMenge(player, formicarium, menge, FutterType.PROTEIN);

	}

	public static Document feedBlätterMenge(Spieler player, Formicarium formicarium, Integer menge)
	{
		return feedTypeMenge(player, formicarium, menge, FutterType.BLÄTTER);
	}

	public static Document feedTypeMenge(Spieler player, Formicarium formicarium, Integer menge, FutterType futterType)
	{
		String foodId = fetchFoodId(formicarium, menge, futterType);

		return feedMenge(player, formicarium, menge, foodId);
	}

	private static String fetchFoodId(Formicarium formicarium, Integer menge, FutterType futterType)
	{
		String foodId = "";

		for(FutterItem futterItem : formicarium.getFormicariumDetails().getAvailableFutterItems())
		{
			if(futterItem.getFutterType().equals(futterType) && futterItem.getGramm() > menge)
			{
				foodId = futterItem.getId() + "";
				break;
			}
		}

		return foodId;
	}

	private static Document feedMenge(Spieler player, Formicarium formicarium, Integer menge, String foodId)
	{
		Map<String, String> data = new HashMap<>();
		data.put("cat", "9");
		data.put("action", "addfood");
		data.put("id", formicarium.getId() + "");
		data.put("foodid", foodId);
		data.put("amount", menge + "");

		return post(player, Links.AMEISENZIMMER, data);
	}

	public static void buyAnt(Spieler player, KleinanzeigeAmeisen kleinanzeigeAmeise)
	{
		Map<String, String> httpData = new HashMap<>();
		httpData.put("action", "buyant");
		httpData.put("antid", kleinanzeigeAmeise.getId());
		httpData.put("sort", "");
		httpData.put("cat", "2");

		post(player, Links.INTERNET, httpData);
	}

	public static void buyImmobilie(Spieler player, Immobilie immobilie)
	{
		Map<String, String> data = new HashMap<>();
		data.put("cat", "4");
		data.put("action", "buy");
		data.put("id", immobilie.getId() + "");

		post(player, Links.INTERNET, data);
	}

	public static Document post(Spieler player, String link, Map<String, String> data)
	{
		Connection.Response response = null;
		try
		{
			response = Jsoup.connect(link)
							.data(data)
							.cookies(player.getCookies())
							.method(Connection.Method.POST)
							.execute();
			SpielerManager.updateMoney(player, Jsoup.parse(response.body()));
			Collector.sleep();
		}
		catch(IOException exc)
		{
			Util.logException(exc);
			exc.printStackTrace();
		}

		return Jsoup.parse(response != null ? response.body() : "");
	}

	public static void entferneFutteritem(Spieler player, Formicarium formicarium, FutterItem futterItem)
	{
		String link = Links.AMEISENZIMMER + "?action=deletefood&cat=9&id=" + formicarium.getId() + "&foodid=" +
				futterItem.getId();
		get(link, player);
	}

	public static void storeFormicarium(Spieler player, Formicarium formicarium)
	{
		String link = Links.AMEISENZIMMER + "?loc=backtolager&cat=9&id=" + formicarium.getId()
				+ "&formicariumid=" + formicarium.getId();
		get(link, player);
		link = Links.AMEISENZIMMER + "?loc=backtolager&cat=1&id=" + formicarium.getId()
				+ "&formicariumid=" + formicarium.getId();
		get(link, player);
	}

	public static void deleteMessage(Spieler player, Integer id)
	{
		get(Links.MESSAGE_DELETE + id, player);
	}

	public static void get(String link, Spieler player)
	{
		try
		{
			Document response = Jsoup.connect(link).cookies(player.getCookies()).get();
			SpielerManager.updateMoney(player, response);
			Collector.sleep();
		}
		catch(Exception exc)
		{
			Util.logException(exc);
			exc.printStackTrace();
		}
	}
}
