package pa.soft.http;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.processors.Util;

import java.io.IOException;
import java.util.Map;

public class Collector
{
	public static Connection.Response cookieValidation(Spieler spieler)
	{
		Connection.Response response = null;
		try
		{
			response = Jsoup.connect(Links.AMEISENZIMMER)
							.data(spieler.getCookies())
							.method(Connection.Method.POST)
							.execute();
			sleep();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return response;
	}

	public static Connection.Response login(Spieler spieler)
	{
		Connection.Response response = null;

		try
		{
			response = Jsoup.connect(Links.LOGIN)
						   .data("username", spieler.getName())
						   .data("password", spieler.getPasswort())
						   .data("autologin", "on")
						   .data("loginbutton", "Login")
						   .data("loginaction", "login")
						   .method(Connection.Method.POST)
						   .execute();
			System.out.println("############################# Logged in as " + spieler.getName() + " #############################");
			sleep();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return response;
	}

	public static Elements fetchFormicariumElements(Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.AMEISENZIMMER_FORMICARIEN);
	}

	public static Elements fetchFormicariumDetails(Integer formicariumId, Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.AMEISENZIMMER_FORMICARIUM + formicariumId);
	}

	public static Elements fetchAktionenElements(Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.AMEISENZIMMER_AKTIONEN);
	}

	public static Elements fetchFuttertierboxElements(Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.AMEISENZIMMER_FUTTERTIERBOXEN);
	}

	public static Elements fetchArbeitenElements(Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.INTERNET_ARBEITSAMT);
	}

	public static Elements fetchForschungenElements(Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.INTERNET_FORSCHUNG);
	}

	public static Elements fetchAmeisenSuchenElements(Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.DRAUSSEN_AMEISEN_SUCHEN);
	}

	public static Elements fetchFutterSuchenElements(Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.DRAUSSEN_FUTTER_SUCHEN);
	}

	public static Elements fetchMöbel(Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.AMEISENZIMMER_MÖBEL);
	}

	public static Elements fetchImmobilienElements(Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.INTERNET_IMMOBILIEN);
	}

	public static Elements fetchMöbelAngebote(Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.INTERNET_MÖBELHAUS);
	}

	public static Elements fetchAmeisenSchutzWarteAktionen(Map<String, String> cookies)
	{
		return fetchShopitems(cookies, Links.DRAUSSEN_AMEISENSCHUTZWARTE_AKTIONEN);
	}

	public static Elements fetchLexikonAmeisen(Map<String, String> cookies)
	{
		Elements elements = fetchShopitems(cookies, Links.LEXIKON_EINHEIMISCHE_AMEISEN);
		elements.addAll(fetchShopitems(cookies, Links.LEXIKON_SÜDEUROPÄISCHE_AMEISEN));
		elements.addAll(fetchShopitems(cookies, Links.LEXIKON_EXOTISCHE_AMEISEN));

		return elements;
	}

	private static Elements fetchShopitems(Map<String, String> cookies, String link)
	{
		Elements elements = null;

		try
		{
			elements = Jsoup.connect(link)
							.cookies(cookies)
							.get()
							.getAllElements()
							.select("#shopitem");
			sleep();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return elements;
	}

	public static Elements fetchEreignisse(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.AMEISENZIMMER_EREIGNISSE,"center>.liste tr:not(.liste)");
	}

	public static Elements fetchLager(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.AMEISENZIMMER_LAGER, "center #text");
	}

	public static Elements fetchKleinanzeigenAmeisen(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.INTERNET_KLEINANZEIGEN_AMEISEN, "tr[class^=\"color\"]");
	}

	public static Elements fetchKleinanzeigenSonstiges(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.INTERNET_KLEINANZEIGEN_SONSTIGES, "tr[class^=\"color\"]");
	}

	public static Elements fetchKleinanzeigenSellableAmeisen(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.INTERNET_KLEINANZEIGEN_AMEISEN, "select option");
	}

	public static Elements fetchKleinanzeigenSellableSonstiges(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.INTERNET_KLEINANZEIGEN_SONSTIGES, "select option");
	}

	public static Elements fetchDraussenErgebnisseAmeisen(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.DRAUSSEN_ERGEBNISSE, "table.liste").get(0).select("tr[class^=\"color\"]");
	}

	public static Elements fetchDraussenErgebnisseFuttertiere(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.DRAUSSEN_ERGEBNISSE, "table.liste").get(1).select("tr[class^=\"color\"]");
	}

	public static Elements fetchAmeisenSchutzWarteAuswilderKolonien(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.DRAUSSEN_AMEISENSCHUTZWARTE_KOLONIE_AUSWILDERN, "select option");
	}

	public static Elements fetchAmeisenSchutzWarteEhemaligeKolonien(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.DRAUSSEN_AMEISENSCHUTZWARTE_EHEMALIGE_KOLONIEN, "td[width=\"700px\"] table tr:not(:first-child)");
	}

	public static Elements fetchAmeisenAnkaufPreise(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.AMEISENSHOP_ANKAUF, "table[class=\"liste\"] tr");
	}

	public static Elements fetchMessages(Map<String, String> cookies)
	{
		return fetchBySelect(cookies, Links.MESSAGES, "td[width=\"700px\"] table tr[class^=\"color\"]tr:not(:first-child)");
	}

	public static Elements fetchColonyElements(Spieler spieler)
	{
		Document document = get(spieler, Links.AMEISENZIMMER + "?loc=createFormi");

		return document.getElementsByAttributeValueMatching("name", "colo").select("option");
	}

	public static Elements fetchMessageContent(Map<String, String> cookies, Integer messageId)
	{
		return fetchShopitems(cookies, Links.MESSAGE_READ + messageId);
	}

	private static Elements fetchBySelect(Map<String, String> cookies, String link, String select)
	{
		Elements elements = null;

		try
		{
			elements = Jsoup.connect(link)
							.cookies(cookies)
							.get()
							.getAllElements()
							.select(select);
			sleep();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return elements;
	}

	public static Document get(Spieler spieler, String link)
	{
		Document document = null;
		try
		{
			document = Jsoup.connect(link)
							.cookies(spieler.getCookies())
							.get();
			sleep();
		}
		catch(IOException exc)
		{
			Util.logException(exc);
			exc.printStackTrace();
		}
		sleep();

		return document;
	}

	static void sleep()
	{
		Long outlong = Long.parseLong((Math.random() + "").substring(2, 10));
		outlong %= 5000;
		outlong += 2500;

		try
		{
			Thread.sleep(outlong);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
