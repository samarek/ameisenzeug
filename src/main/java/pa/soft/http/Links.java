package pa.soft.http;

public class Links
{
	public static String EATEN_BY_ANTS = "http://eatenbyants.de";
	public static String LOGIN = "http://eatenbyants.de/login.php";

	// Ameisenzimmer
	public static final String AMEISENZIMMER = "http://eatenbyants.de/ameisenzimmer.php";
	public static final String AMEISENZIMMER_FORMICARIEN = "http://eatenbyants.de/ameisenzimmer.php?cat=1";
	public static final String AMEISENZIMMER_FORMICARIUM = "http://eatenbyants.de/ameisenzimmer.php?cat=9&id=";
	public static final String AMEISENZIMMER_LAGER = "http://eatenbyants.de/ameisenzimmer.php?cat=2";
	public static final String AMEISENZIMMER_EREIGNISSE = "http://eatenbyants.de/ameisenzimmer.php?cat=3";
	public static final String AMEISENZIMMER_MÖBEL = "http://eatenbyants.de/ameisenzimmer.php?cat=4";
	public static final String AMEISENZIMMER_AKTIONEN = "http://eatenbyants.de/ameisenzimmer.php?cat=5";
	public static final String AMEISENZIMMER_FUTTERTIERBOXEN = "http://eatenbyants.de/ameisenzimmer.php?cat=8";

	// Internet
	public static final String INTERNET = "http://eatenbyants.de/internet.php";
	public static final String INTERNET_FORSCHUNG = "http://eatenbyants.de/internet.php?cat=1";
	public static final String INTERNET_KLEINANZEIGEN_AMEISEN = "http://eatenbyants.de/internet.php?cat=2&subcat=0&action=showants";
	public static final String INTERNET_KLEINANZEIGEN_SONSTIGES = "http://eatenbyants.de/internet.php?cat=2&subcat=0&action=showitems";
	public static final String INTERNET_ARBEITSAMT = "http://eatenbyants.de/internet.php?cat=3";
	public static final String INTERNET_IMMOBILIEN = "http://eatenbyants.de/internet.php?cat=4";
	public static final String INTERNET_MÖBELHAUS = "http://eatenbyants.de/internet.php?cat=5";

	// Ameisenshop
	public static final String AMEISENSHOP = "http://eatenbyants.de/shop.php";
	public static final String AMEISENSHOP_EINHEIMISCHE_AMEISEN = "http://eatenbyants.de/shop.php?cat=1";
	public static final String AMEISENSHOP_SÜDEUROPÄISCHE_AMEISEN = "http://eatenbyants.de/shop.php?cat=2";
	public static final String AMEISENSHOP_EXOTISCHE_AMEISEN = "http://eatenbyants.de/shop.php?cat=3";
	public static final String AMEISENSHOP_FUTTER = "http://eatenbyants.de/shop.php?cat=4";
	public static final String AMEISENSHOP_ZUBEHÖR = "http://eatenbyants.de/shop.php?cat=5";
	public static final String AMEISENSHOP_FORMICARIEN = "http://eatenbyants.de/shop.php?cat=6";
	public static final String AMEISENSHOP_NESTER = "http://eatenbyants.de/shop.php?cat=7";
	public static final String AMEISENSHOP_ANKAUF = "http://eatenbyants.de/shop.php?cat=99";

	// Draussen
	public static final String DRAUSSEN = "http://eatenbyants.de/draussen.php";
	public static final String DRAUSSEN_AMEISEN_SUCHEN = "http://eatenbyants.de/draussen.php?cat=1";
	public static final String DRAUSSEN_FUTTER_SUCHEN = "http://eatenbyants.de/draussen.php?cat=2";
	public static final String DRAUSSEN_ERGEBNISSE = "http://eatenbyants.de/draussen.php?cat=3";
	public static final String DRAUSSEN_AMEISENSCHUTZWARTE_STATISTIK = "http://eatenbyants.de/draussen.php?cat=4&subcat=1";
	public static final String DRAUSSEN_AMEISENSCHUTZWARTE_KOLONIE_AUSWILDERN = "http://eatenbyants.de/draussen.php?cat=4&subcat=2";
	public static final String DRAUSSEN_AMEISENSCHUTZWARTE_EHEMALIGE_KOLONIEN = "http://eatenbyants.de/draussen.php?cat=4&subcat=4";
	public static final String DRAUSSEN_AMEISENSCHUTZWARTE_AKTIONEN = "http://eatenbyants.de/draussen.php?cat=4&subcat=3";

	// Messages
	public static final String MESSAGES = "http://eatenbyants.de/message.php";
	public static final String MESSAGE_READ = "http://eatenbyants.de/message.php?action=readmessage&id=";
	public static final String MESSAGE_DELETE = "http://eatenbyants.de/message.php?action=deletemessage&sent=&id=";

	// Lexikon
	public static final String LEXIKON = "http://eatenbyants.de/lexikon.php";
	public static final String LEXIKON_EINHEIMISCHE_AMEISEN = "http://eatenbyants.de/lexikon.php?cat=1";
	public static final String LEXIKON_SÜDEUROPÄISCHE_AMEISEN = "http://eatenbyants.de/lexikon.php?cat=2";
	public static final String LEXIKON_EXOTISCHE_AMEISEN = "http://eatenbyants.de/lexikon.php?cat=3";
	public static final String LEXIKON_FUTTER = "http://eatenbyants.de/lexikon.php?cat=4";
	public static final String LEXIKON_FORMICARIEN = "http://eatenbyants.de/lexikon.php?cat=5";
	public static final String LEXIKON_NESTER = "http://eatenbyants.de/lexikon.php?cat=6";
	public static final String LEXIKON_AUSBRUCHSCHUTZ = "http://eatenbyants.de/lexikon.php?cat=7";
	public static final String LEXIKON_ZUCHTBEHÄLTER = "http://eatenbyants.de/lexikon.php?cat=8";
}
