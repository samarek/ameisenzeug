package pa.soft;

import pa.soft.processors.Util;
import java.util.HashMap;
import java.util.Map;

public class Main
{
	public static Map<String, String> ARGS = new HashMap<>();

	private static final String RUNFILE = "runfile";
	private static final String RUNMODE = "runmode";

	public static void main(String[] args)
	{
		Long start = System.currentTimeMillis();
		try
		{
			makeArgs(args);
			Starter.run(ARGS.get(RUNMODE));
		}
		catch(Exception e)
		{
			Util.logException(e);
			e.printStackTrace();
		}
		finally
		{
			Util.logFlush();
			Long end = System.currentTimeMillis() - start;
			String millis = end % 1_000 + "";
			millis = millis.length() == 1 ? "00" + millis : millis.length() == 2 ? "0" + millis : millis;
			System.out.println("Runtime " + end / 1_000 + "," + millis);
		}
	}

	private static void makeArgs(String[] args)
	{
		ARGS.clear();
		for(String arg : args)
		{
			if(arg.contains("="))
			{
				String[] argument = arg.split("=");
				if(argument[0].equalsIgnoreCase(RUNFILE))
				{
					ARGS = Util.readCsv(argument[1]);
				}
			}
		}
		if(ARGS.isEmpty())
		{
			throw new RuntimeException("Invalid Runfile");
		}
	}
}
