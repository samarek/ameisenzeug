package pa.soft.data.types;

public enum AmeisenType
{
	CAMPONOTUS_HERCULEANUS("Camponotus herculeanus", 10000, 0.5, 0.4, 0.4, NestType.MEDIUM_YTONG_NEST, FormicariumType.MEDIUM_INSTALLATION),
	CAMPONOTUS_LIGNIPERDA("Camponotus ligniperda", 10000, 0.4, 0.4, 0.4, NestType.MEDIUM_YTONG_NEST, FormicariumType.MEDIUM_INSTALLATION),
	FORMICA_FUSCA("Formica fusca", 5000, 0.25, 0.2, 0.2, NestType.BIG_NEST, FormicariumType.BIG_FORMICARY),
	FORMICA_SANGUINEA("Formica sanguinea", 10000, 1.0, 0.3, 0.3, NestType.MEDIUM_YTONG_NEST, FormicariumType.MEDIUM_INSTALLATION),
	LASIUS_EMARGINATUS("Lasius emarginatus", 50000, 0.3, 0.1, 0.11, NestType.MEDIUM_YTONG_NEST, FormicariumType.MEDIUM_INSTALLATION),
	LASIUS_FLAVUS("Lasius flavus", 100000, 0.12, 0.1, 0.02, NestType.MEDIUM_YTONG_NEST, FormicariumType.BIG_FORMICARY),
	LASIUS_FULIGINOSUS("Lasius fuliginosus", 1000000, 0.5, 0.1, 0.11, NestType.BIG_YTONG_NEST, FormicariumType.GIANT_INSTALLATION),
	LASIUS_NIGER("Lasius niger", 50000, 0.1, 0.1, 0.15, NestType.MEDIUM_YTONG_NEST, FormicariumType.MEDIUM_INSTALLATION),
	MYRMICA_RUBRA("Myrmica rubra", 20000, 0.2, 0.15, 0.2, NestType.MEDIUM_YTONG_NEST, FormicariumType.MEDIUM_INSTALLATION),
	MYRMICA_RUGINODIS("Myrmica ruginodis", 100000, 0.3, 0.2, 0.2, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	PONERA_COARCTATA("Ponera coarctata", 125, 20.0, 0.05, 0.1, NestType.TEST_TUBE, FormicariumType.MINI_ARENA),
	SOLENOPSIS_FUGAX("Solenopsis fugax", 250000, 0.5, 0.07, 0.1, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	TEMNOTHORAX_NYLANDERI("Temnothorax nylanderi", 250, 1.0, 0.05, 0.1, NestType.SMALL_NEST, FormicariumType.MINI_ARENA),

	APHAENOGASTER_SENILIS("Aphaenogaster senilis", 10000, 0.5, 0.3, 0.3, NestType.MEDIUM_YTONG_NEST, FormicariumType.MEDIUM_INSTALLATION),
	CAMPONOTUS_CRUENTATUS("Camponotus cruentatus", 100000, 0.7, 0.45, 0.45, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	CAMPONOTUS_LATERALIS("Camponotus lateralis", 10000, 7.5, 0.35, 0.35, NestType.MEDIUM_YTONG_NEST, FormicariumType.MEDIUM_INSTALLATION),
	CATAGLYPHIS_VELOX("Cataglyphis velox", 10000, 1.3, 0.4, 0.6, NestType.MEDIUM_YTONG_NEST, FormicariumType.MEDIUM_INSTALLATION),
	CREMATOGASTER_SCUTELLARIS("Crematogaster scutellaris", 100000, 0.35, 0.1, 0.2, NestType.MEDIUM_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	LASIUS_GRANDIS("Lasius grandis", 100000, 0.8, 0.1, 0.09, NestType.MEDIUM_YTONG_NEST, FormicariumType.MEDIUM_INSTALLATION),
	MESSOR_BARBARUS("Messor barbarus", 100000, 0.35, 0.4, 0.4, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	MESSOR_STRUCTOR("Messor structor", 100000, 0.45, 0.35, 0.35, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	PHEIDOLE_PALLIDULA("Pheidole pallidula", 250000, 0.2, 0.1, 0.2, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	PLAGIOLEPIS_PYGMAEA("Plagiolepis pygmaea", 500000, 0.2, 0.025, 0.025, NestType.MEDIUM_YTONG_NEST, FormicariumType.MEDIUM_INSTALLATION),

	ACROMYRMEX_SPEC("Acromyrmex spec.", 100000, 1.2, 0.55, 0.45, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	ACROMYRMEX_VERSICOLOR("Acromyrmex versicolor", 100000, 1.5, 0.55, 0.45, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	CAMPONOTUS_SERICEUS("Camponotus sericeus", 50000, 5.0, 0.6, 0.6, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	CAMPONOTUS_SUBSTITUTUS("Camponotus substitutus", 80000, 1.0, 0.45, 0.45, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	MESSOR_ALEXANDRI("Messor alexandri", 100000, 1.5, 0.4, 0.4, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	MYRMECIA_CHRYSOGASTER("Myrmecia chrysogaster", 1000, 70.0, 0.3, 0.4, NestType.MEDIUM_NEST, FormicariumType.MEDIUM_FORMICARY),
	MYRMECIA_PAVIDA("Myrmecia pavida", 1000, 50.0, 0.4, 0.5, NestType.MEDIUM_NEST, FormicariumType.MEDIUM_FORMICARY),
	MYRMECOCYSTUS_MEXICANUS("Myrmecocystus mexicanus", 120000, 1.4, 0.3, 0.5, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	PHEIDOLOGETON_DIVERSUS("Pheidologeton diversus", 100000, 1.3, 0.3, 0.4, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	POLYRHACHIS_DIVES("Polyrhachis dives", 100000, 0.9, 0.3, 0.5, NestType.BIG_YTONG_NEST, FormicariumType.BIG_INSTALLATION),
	MARSAMEISE("Marsameise", 500, 100.0, 0.4, 0.4, NestType.MEDIUM_NEST, FormicariumType.MEDIUM_FORMICARY);


	private String name;
	private Integer maxPopulation;
	private Double punkte;

	private Double platzbedarfNest;
	private Double platzbedarfFormicarium;

	private NestType maxNest;
	private FormicariumType maxFormicarium;

	AmeisenType(String name, Integer maxPopulation, Double punkte, Double platzbedarfNest, Double platzbedarfFormicarium,
				NestType maxNest, FormicariumType maxFormicarium)
	{
		this.name = name;
		this.maxPopulation = maxPopulation;
		this.punkte = punkte;
		this.platzbedarfNest = platzbedarfNest;
		this.platzbedarfFormicarium = platzbedarfFormicarium;
		this.maxNest = maxNest;
		this.maxFormicarium = maxFormicarium;
	}

	public static AmeisenType fromString(String text)
	{
		AmeisenType output = null;

		for(AmeisenType ameisenType : AmeisenType.values())
		{
			if(ameisenType.name.equalsIgnoreCase(text))
			{
				output = ameisenType;
			}
		}

		return output;
	}

	public Integer getMaxPopulation()
	{
		return maxPopulation;
	}

	public Double getPunkte()
	{
		return punkte;
	}

	public String getName()
	{
		return name;
	}

	public String getLogName()
	{
		return name + makeSpaces();
	}

	private String makeSpaces()
	{
		Integer spaceFaktor = 30 - name.length();
		StringBuilder spaces = new StringBuilder();
		for(int i = 0; i < spaceFaktor; i++)
		{
			spaces.append(' ');
		}

		return spaces.toString();
	}

	public Double getPlatzbedarfNest()
	{
		return platzbedarfNest;
	}

	public Double getPlatzbedarfFormicarium()
	{
		return platzbedarfFormicarium;
	}

	public NestType getMaxNest()
	{
		return maxNest;
	}

	public FormicariumType getMaxFormicarium()
	{
		return maxFormicarium;
	}

	@Override
	public String toString()
	{
		return "AmeisenType{" + "name='" + name + '\'' + ", maxPopulation=" + maxPopulation + ", punkte=" + punkte +
				", platzbedarfNest=" + platzbedarfNest + ", platzbedarfFormicarium=" + platzbedarfFormicarium +
				", maxNest=" + maxNest + ", maxFormicarium=" + maxFormicarium + '}';
	}
}

