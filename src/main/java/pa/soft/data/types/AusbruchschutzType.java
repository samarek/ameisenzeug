package pa.soft.data.types;

public enum AusbruchschutzType
{
	PARAFFINÖL(590, "Paraffinöl", 90),
	TALKUM(591, "Talkum", 199),
	PTFE(592, "PTFE", 299);

	private Integer id;
	private String name;
	private Integer preis;

	AusbruchschutzType(Integer id, String name, Integer preis)
	{
		this.id = id;
		this.name = name;
		this.preis = preis;
	}

	public static AusbruchschutzType fromString(String text)
	{
		AusbruchschutzType output = null;

		for(AusbruchschutzType nestType : AusbruchschutzType.values())
		{
			if(nestType.name.equalsIgnoreCase(text))
			{
				output = nestType;
			}
		}

		return output;
	}

	@Override
	public String toString()
	{
		return name;
	}

	public String getIdString()
	{
		return id + "";
	}
}
