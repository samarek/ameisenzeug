package pa.soft.data.types;

public enum Nest
{
	TEST_TUBE("Reagenzglas"), SMALL_NEST("kleines Nest"), MEDIUM_NEST("mittleres Nest"), BIG_NEST(
		"großes Nest"), MEDIUM_YTONG_NEST("mittleres YTong Nest"), BIG_YTONG_NEST("großes YTong Nest");

	private String value;

	Nest(String value)
	{
		this.value = value;
	}

	public static Nest fromString(String text)
	{
		Nest output = null;

		for(Nest nest : Nest.values())
		{
			if(nest.value.equalsIgnoreCase(text))
			{
				output = nest;
			}
		}

		return output;
	}

	@Override
	public String toString()
	{
		return value;
	}
}
