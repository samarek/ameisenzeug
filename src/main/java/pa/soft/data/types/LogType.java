package pa.soft.data.types;

public enum LogType
{
	DEFAULT, VERSORGUNG, FUTTER_ENTFERNT, ANZUCHT, LAGER
}
