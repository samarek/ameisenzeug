package pa.soft.data.types;

public enum TemplateType
{
	PREISLISTE("Ankaufpreiskurse", 720),
	ANZUCHT("Anzucht", 1440),
	BIS50K("Bis50k", 1440),
	BIS250K("Bis250k", 1440),
	ARBEITERINNENLISTE("Alles", 1440);

	private String name;

	private Integer maxLength;

	TemplateType(String name, Integer maxLength)
	{
		this.name = name;
		this.maxLength = maxLength;
	}

	public String getName()
	{
		return name;
	}

	public Integer getMaxLength()
	{
		return maxLength;
	}
}
