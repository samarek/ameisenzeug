package pa.soft.data.types;

public enum FormicariumType
{
	MINI_ARENA(604, "Mini Arena", "MINI_ARENA", 30, .5, 0),
	SMALL_FORMICARY(601, "kleines Formicarium", "SMALL_FORMICARY", 100, 1d, 1),
	MEDIUM_FORMICARY(602, "mittleres Formicarium", "MEDIUM_FORMICARY", 500, 2d, 2),
	BIG_FORMICARY(603, "großes Formicarium", "BIG_FORMICARY", 2_500, 3d, 3),
	MEDIUM_INSTALLATION(703, "mittlere Anlage", "MEDIUM_INSTALLATION", 15_000, 4d, 4),
	BIG_INSTALLATION(704, "große Anlage", "BIG_INSTALLATION", 100_000, 6d, 5),
	GIANT_INSTALLATION(708, "riesige Anlage", "GIANT_INSTALLATION", 200_000, 0d, 6);

	private Integer id;
	private String name;
	private String title;
	private Integer platz;
	private Double regalplatz;
	private Integer comparator;

	FormicariumType(Integer id, String name, String title, Integer platz, Double regalplatz, Integer comparator)
	{
		this.id = id;
		this.name = name;
		this.title = title;
		this.platz = platz;
		this.regalplatz = regalplatz;
		this.comparator = comparator;
	}

	public static FormicariumType fromString(String text)
	{
		FormicariumType output = null;

		for(FormicariumType formicariumType : FormicariumType.values())
		{
			if(formicariumType.name.equalsIgnoreCase(text))
			{
				output = formicariumType;
			}
		}

		return output;
	}

	public static FormicariumType fetchBiggestPossible(Double platzbedarf)
	{
		FormicariumType output = MINI_ARENA;
		if(platzbedarf >= BIG_INSTALLATION.regalplatz)
		{
			output = BIG_INSTALLATION;
		}
		else if(platzbedarf >= MEDIUM_INSTALLATION.regalplatz)
		{
			output = MEDIUM_INSTALLATION;
		}
		else if(platzbedarf >= BIG_FORMICARY.regalplatz)
		{
			output = BIG_FORMICARY;
		}
		else if(platzbedarf >= MEDIUM_FORMICARY.regalplatz)
		{
			output = MEDIUM_FORMICARY;
		}
		else if(platzbedarf >= SMALL_FORMICARY.regalplatz)
		{
			output = SMALL_FORMICARY;
		}

		return output;
	}

	public String getNameForLog()
	{
		return name + makeSpaces();
	}

	public String getName()
	{
		return name;
	}

	private String makeSpaces()
	{
		Integer spaceFaktor = 30 - name.length();
		StringBuilder spaces = new StringBuilder();
		for(int i = 0; i < spaceFaktor; i++)
		{
			spaces.append(' ');
		}

		return spaces.toString();
	}

	public String getTitle()
	{
		return title;
	}

	public Integer getPlatz()
	{
		return platz;
	}

	public Double getRegalplatz()
	{
		return regalplatz;
	}

	public String getIdString()
	{
		return id + "";
	}

	@Override
	public String toString()
	{
		return name;
	}

	public boolean smallerOrEqual(FormicariumType biggestFormicarium)
	{
		return comparator <= biggestFormicarium.comparator;
	}
}
