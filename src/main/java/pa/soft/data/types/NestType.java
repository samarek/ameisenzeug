package pa.soft.data.types;

public enum NestType
{
	TEST_TUBE(501, "Reagenzglas", "TEST_TUBE", 10),
	SMALL_NEST(611, "kleines Nest", "SMALL_NEST", 100),
	MEDIUM_NEST(612, "mittleres Nest", "MEDIUM_NEST", 500),
	BIG_NEST(613, "großes Nest", "BIG_NEST", 2_500),
	MEDIUM_YTONG_NEST(705, "mittleres YTong Nest", "MEDIUM_YTONG_NEST", 15_000),
	BIG_YTONG_NEST(706, "großes YTong Nest", "BIG_YTONG_NEST", 100_000),
	GIANT_YTONG_NEST(709, "riesiges YTong Nest", "GIANT_YTONG_NEST", 200_000),
	HAZELNUT(0, "Haselnuss", "HAZELNUT", 0);

	private Integer id = 0;
	private String name;
	private String title;
	private Integer platz;

	NestType(Integer id, String name, String title, Integer platz)
	{
		this.id = id;
		this.name = name;
		this.title = title;
		this.platz = platz;
	}

	public static NestType fromString(String text)
	{
		NestType output = null;

		for(NestType nestType : NestType.values())
		{
			if(nestType.name.equalsIgnoreCase(text))
			{
				output = nestType;
			}
		}

		return output;
	}

	public String getName()
	{
		return name + makeSpaces();
	}

	private String makeSpaces()
	{
		Integer spaceFaktor = 20 - name.length();
		StringBuilder spaces = new StringBuilder();
		for(int i = 0; i < spaceFaktor; i++)
		{
			spaces.append(' ');
		}

		return spaces.toString();
	}

	public Integer getPlatz()
	{
		return platz;
	}

	public String getTitle()
	{
		return title;
	}

	@Override
	public String toString()
	{
		return name;
	}

	public Integer getId()
	{
		return id;
	}
}
