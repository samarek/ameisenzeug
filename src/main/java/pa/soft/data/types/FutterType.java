package pa.soft.data.types;

import java.util.Arrays;
import java.util.List;

public enum FutterType
{
	PROTEIN("Protein"), ZUCKER("Zucker"), SAMEN("Samen"), BLÄTTER("Blätter");

	private String value;
	private static List<String> BLÄTTER_LIST = Arrays.asList("Brombeerblätter");
	private static List<String> ZUCKER_LIST = Arrays.asList("Zuckerwasser", "Honig", "gemixtes Zuckerwasser");
	private static List<String> SAMEN_LIST = Arrays.asList("Grassamen", "Löwenzahnsamen");
	private static List<String> PROTEINE_LIST = Arrays.asList("Mehlwürmer", "Maden", "Grashüpfer", "Regenwürmer", "Baumwanzen",
												  "Käferlarven", "Heimchen", "Waldschaben");

	FutterType(String value)
	{
		this.value = value;
	}

	public static FutterType fromString(String text)
	{
		for(FutterType nest : FutterType.values())
		{
			if(nest.value.equalsIgnoreCase(text))
			{
				return nest;
			}
		}

		return PROTEIN;
	}

	public static FutterType fromFutterString(String futter)
	{
		if (BLÄTTER_LIST.contains(futter))
		{
			return BLÄTTER;
		}
		if (ZUCKER_LIST.contains(futter))
		{
			return ZUCKER;
		}
		if (SAMEN_LIST.contains(futter))
		{
			return SAMEN;
		}

		return PROTEIN;
	}

	@Override
	public String toString()
	{
		return value;
	}
}
