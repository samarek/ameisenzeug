package pa.soft.data.types;

public enum Formicary
{
	MINI_ARENA("Mini Arena"), SMALL_FORMICARY("kleines Formicarium"), MEDIUM_FORMICARY("mittleres Formicarium"), BIG_FORMICARY(
		"großes Formicarium"), MEDIUM_INSTALLATION("mittlere Anlage"), BIG_INSTALLATION("große Anlage");

	private String value;

	Formicary(String value)
	{
		this.value = value;
	}

	public static Formicary fromString(String text)
	{
		Formicary output = null;

		for(Formicary formicary : Formicary.values())
		{
			if(formicary.value.equalsIgnoreCase(text))
			{
				output = formicary;
			}
		}

		return output;
	}

	@Override
	public String toString()
	{
		return value;
	}
}
