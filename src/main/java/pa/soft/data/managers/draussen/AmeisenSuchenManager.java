package pa.soft.data.managers.draussen;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.Task;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.List;

public class AmeisenSuchenManager extends DraussenTaskManager
{
	public static List<Task> fetchAmeisenSuchen(Spieler spieler)
	{
		Elements ameisenSuchenElements = Collector.fetchAmeisenSuchenElements(spieler.getCookies());

		List<Task> ameisenSuchenList = new ArrayList<>();
		for(Element ameisenSuchenElement : ameisenSuchenElements)
		{
			ameisenSuchenList.add(makeDraussenTask(ameisenSuchenElement.select("form")));
		}

		return ameisenSuchenList;
	}
}
