package pa.soft.data.managers.draussen;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.Task;
import pa.soft.data.entities.draussen.AmeisenSchutzWarte;
import pa.soft.data.entities.draussen.details.AmeisenSchutzWarteTask;
import pa.soft.data.entities.draussen.details.EhemaligeKolonie;
import pa.soft.data.entities.draussen.details.Kolonie;
import pa.soft.data.managers.TaskManager;
import pa.soft.data.types.NestType;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AmeisenSchutzWarteManager extends TaskManager
{
	private static Spieler spieler;

	public static AmeisenSchutzWarte fetchAmeisenSchutzWarte(Spieler spieler)
	{
		AmeisenSchutzWarteManager.spieler = spieler;

		AmeisenSchutzWarte ameisenSchutzWarte = new AmeisenSchutzWarte();

//		ameisenSchutzWarte.setAuswilderKolonien(makeAuswilderKolonien());
//		ameisenSchutzWarte.setEhemaligeKolonien(makeEhemaligeKolonien());
		ameisenSchutzWarte.setAktionen(makeAktionen());

		return ameisenSchutzWarte;
	}

	private static Map<Integer, Kolonie> makeAuswilderKolonien()
	{
		Elements auswilderKolonienElements = Collector.fetchAmeisenSchutzWarteAuswilderKolonien(spieler.getCookies());

		Map<Integer, Kolonie> kolonien = new HashMap<>();
		for(Element auswilderKolonienElement : auswilderKolonienElements)
		{
			Integer id = Integer.parseInt(auswilderKolonienElement.val());
			kolonien.put(id, extractKolonie(auswilderKolonienElement));
		}

		return kolonien;
	}

	private static Kolonie extractKolonie(Element auswilderKolonienElement)
	{
		Kolonie kolonie = new Kolonie();

		String kolonieString = auswilderKolonienElement.text().replace(",", "");
		Integer klammerAuf = kolonieString.indexOf("(");
		Integer klammerZu = kolonieString.indexOf(")");
		String queens = kolonieString.substring(klammerAuf + 1, kolonieString.indexOf("K")).trim();
		String drones = kolonieString.substring(kolonieString.indexOf("K") + 2, kolonieString.indexOf("A")).trim();

		kolonie.setSpezies(kolonieString.substring(0, klammerAuf).trim());
		kolonie.setQueens(Integer.parseInt(queens));
		kolonie.setDrones(Integer.parseInt(drones));
		kolonie.setNestType(NestType.fromString(kolonieString.substring(kolonieString.indexOf("in") + 2, klammerZu).trim()));

		return kolonie;
	}

	private static List<EhemaligeKolonie> makeEhemaligeKolonien()
	{
		Elements ehemaligeKolonienElements = Collector.fetchAmeisenSchutzWarteEhemaligeKolonien(spieler.getCookies());

		List<EhemaligeKolonie> ehemaligeKolonien = new ArrayList<>();
		for(Element ehemaligeKolonienElement : ehemaligeKolonienElements)
		{
			ehemaligeKolonien.add(extractEhemaligeKolonie(ehemaligeKolonienElement));
		}

		return ehemaligeKolonien;
	}

	private static EhemaligeKolonie extractEhemaligeKolonie(Element ehemaligeKolonienElement)
	{
		EhemaligeKolonie ehemaligeKolonie = new EhemaligeKolonie();

		// ToDo herausfinden warum beim Datum das parsen nicht funktioniert
		String zeitpunktString = ehemaligeKolonienElement.select("td:nth-child(1)").text().trim();
//		ehemaligeKolonie.setZeitpunkt(LocalDateTime.parse(zeitpunktString, formatter));

		ehemaligeKolonie.setKolonie(extractKolonie(ehemaligeKolonienElement.select("td:nth-child(2)").first()));
		String punkteString = ehemaligeKolonienElement.select("td:nth-child(3)").text().trim();
		ehemaligeKolonie.setPunkte(Integer.parseInt(punkteString));
		String aswPunkteString = ehemaligeKolonienElement.select("td:nth-child(4)").text().trim();
		ehemaligeKolonie.setAswPunkte(Integer.parseInt(aswPunkteString));

		return ehemaligeKolonie;
	}

	private static List<Task> makeAktionen()
	{
		Elements aktionenElements = Collector.fetchAmeisenSchutzWarteAktionen(spieler.getCookies());

		List<Task> aktionen = new ArrayList<>();
		for(Element aktionenElement : aktionenElements)
		{
			aktionen.add(extractAktion(aktionenElement.select("form")));
		}

		return aktionen;
	}

	private static Task extractAktion(Elements aktionenElement)
	{
		String content = aktionenElement.html();

		AmeisenSchutzWarteTask ameisenSchutzWarteTask = new AmeisenSchutzWarteTask();

		ameisenSchutzWarteTask.setId(extractId(aktionenElement));
		ameisenSchutzWarteTask.setTitel(aktionenElement.select("b").text());
		ameisenSchutzWarteTask.setBeschreibung(extractBeschreibung(content));
		ameisenSchutzWarteTask.setDauer(extractDauer(content));
		ameisenSchutzWarteTask.setMoney(extractMoney(content));

		return ameisenSchutzWarteTask;
	}

	protected static Integer extractId(Elements form)
	{
		String idString = form.select("input:nth-of-type(3)").val();

		return Integer.parseInt(idString);
	}
}
