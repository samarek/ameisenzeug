package pa.soft.data.managers.draussen;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.draussen.Ergebnis;
import pa.soft.data.managers.AbstractManager;
import pa.soft.http.Collector;
import pa.soft.processors.Util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ErgebnisManager extends AbstractManager
{
	public static List<Ergebnis> fetchDraussenErgebnisseAmeisen(Spieler spieler)
	{
		Elements ergebnisElements = Collector.fetchDraussenErgebnisseAmeisen(spieler.getCookies());

		return processErgebnisElements(ergebnisElements);
	}

	public static List<Ergebnis> fetchDraussenErgebnisseFuttertiere(Spieler spieler)
	{
		Elements ergebnisElements = Collector.fetchDraussenErgebnisseFuttertiere(spieler.getCookies());

		return processErgebnisElements(ergebnisElements);
	}

	private static List<Ergebnis> processErgebnisElements(Elements ergebnisElements)
	{
		List<Ergebnis> ergebnisse = new ArrayList<>();
		for(Element ergebnisElement : ergebnisElements)
		{
			ergebnisse.add(makeDraussenErgebnisse(ergebnisElement));
		}

		return ergebnisse;
	}

	private static Ergebnis makeDraussenErgebnisse(Element ergebnisElement)
	{
		Ergebnis ergebnis = new Ergebnis();
		String datumString = ergebnisElement.select("td:nth-child(1)").text();
		ergebnis.setZeitpunkt(LocalDateTime.parse(datumString, Util.FORMATTER_SHORT_YEAR));
		ergebnis.setErgebnis(ergebnisElement.select("td:nth-child(2)").text());

		return ergebnis;
	}
}
