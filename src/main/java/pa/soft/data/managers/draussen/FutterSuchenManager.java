package pa.soft.data.managers.draussen;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.Task;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.List;

public class FutterSuchenManager extends DraussenTaskManager
{
	public static List<Task> fetchFutterSuchen(Spieler spieler)
	{
		Elements fetchFutterSuchenElements = Collector.fetchFutterSuchenElements(spieler.getCookies());

		List<Task> futterSuchenList = new ArrayList<>();
		for(Element futterSuchenElement : fetchFutterSuchenElements)
		{
			futterSuchenList.add(makeDraussenTask(futterSuchenElement.select("form")));
		}

		return futterSuchenList;
	}
}
