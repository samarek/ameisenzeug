package pa.soft.data.managers;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.draussen.DraussenTask;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

public class DraussenTaskManager extends TaskManager
{
	protected static DraussenTask makeDraussenTask(Elements form)
	{
		String content = form.html();

		DraussenTask task = new DraussenTask();
		task.setId(extractId(form));
		task.setTitel(form.select("b").text());
		task.setBeschreibung(extractBeschreibung(content));
		task.setDauer(extractDauerList(form));
		task.setMoney(extractMoney(content));

		return task;
	}

	private static List<Duration> extractDauerList(Elements content)
	{
		List<Duration> duration = new ArrayList<>();

		for(Element dauer : content.select("select option"))
		{
			String[] dauerArray = dauer.text().split(":");
			duration.add(Duration.of(Long.parseLong(dauerArray[0]), ChronoUnit.HOURS)
							   .plusMinutes(Long.parseLong(dauerArray[1]))
							   .plusSeconds(Long.parseLong(dauerArray[2])));
		}

		return duration;
	}
}
