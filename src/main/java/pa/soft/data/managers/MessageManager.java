package pa.soft.data.managers;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Message;
import pa.soft.data.entities.Spieler;
import pa.soft.http.Collector;
import pa.soft.processors.Util;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MessageManager
{
	private static Spieler spieler;

	public static final List<String> KNOWN_SENDERS = Arrays.asList(
		"Kleinanzeigen Markt"
	);

	public static List<Message> fetchMessages(Spieler spieler)
	{
		MessageManager.spieler = spieler;

		Elements messageElements = Collector.fetchMessages(spieler.getCookies());

		List<Message> messages = new ArrayList<>();
		for(Element messageElement : messageElements)
		{
			messages.add(makeMessage(messageElement));
		}

		return messages;
	}

	private static Message makeMessage(Element messageElement)
	{
		Integer id = extractId(messageElement);
		String sender = extractSender(messageElement);
		String subject = extractSubject(messageElement);

		String content = "";
		if(!KNOWN_SENDERS.contains(sender))
		{
			Elements contentElements = Collector.fetchMessageContent(spieler.getCookies(), id);
			content = contentElements.get(1).text();
		}

		LocalDateTime localDateTime = extractLocalDateTime(messageElement);

		return new Message(id, sender, subject, content, localDateTime);
	}

	private static Integer extractId(Element messageElement)
	{
		String idString = messageElement.select("td:nth-child(3) a").attr("href");
		idString = idString.substring(idString.lastIndexOf("=") + 1);

		return Integer.valueOf(idString);
	}

	private static String extractSender(Element messageElement)
	{
		return messageElement.select("td:nth-child(2)").text();
	}

	private static String extractSubject(Element messageElement)
	{
		return messageElement.select("td:nth-child(3)").text();
	}

	private static LocalDateTime extractLocalDateTime(Element messageElement)
	{
		String localDateTimeString = messageElement.select("td:nth-child(4)").text();

		return LocalDateTime.parse(localDateTimeString, Util.FORMATTER_LONG_YEAR);
	}
}
