package pa.soft.data.managers;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.time.format.DateTimeFormatter;

public class AbstractManager
{
	protected static Integer extractId(Element immobilienElement)
	{
		String idString = immobilienElement.select("input[name=\"id\"]").val();

		return idString.length() > 0 ? Integer.parseInt(idString) : 0;
	}

	protected static Integer extractPreis(Element element)
	{
		String preisString = element.select("p").text();
		preisString = preisString.length() > 0 ? preisString.substring(preisString.indexOf(":") + 1,
																	   preisString.indexOf("€")) : "0";
		preisString = preisString.replace(",", "");
		preisString = preisString.replace(".", "");

		return Integer.parseInt(preisString.trim());
	}

	protected static Integer extractIntegerValue(Element attributeElement)
	{
		return Integer.parseInt(attributeElement.select("td").get(1).text());
	}

	protected static String extractKey(Element attributeElement)
	{
		return attributeElement.select("td").get(0).text().trim();
	}

	protected static String extractStringValue(Element attributeElement)
	{
		return attributeElement.select("td").get(1).text().replace("\u00A0", "").trim();
	}

	protected static Elements removeEinrichtenShopitems(Elements allElements)
	{
		Elements output = new Elements();
		for(Element element : allElements)
		{
			if (!element.text().contains("einrichten"))
			{
				output.add(element);
			}
		}

		return output;
	}
}
