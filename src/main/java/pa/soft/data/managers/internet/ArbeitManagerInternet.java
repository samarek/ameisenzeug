package pa.soft.data.managers.internet;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.Task;
import pa.soft.data.managers.InternetTaskManager;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.List;

public class ArbeitManagerInternet extends InternetTaskManager
{
	public static List<Task> fetchArbeiten(Spieler spieler)
	{
		Elements arbeitenElements = Collector.fetchArbeitenElements(spieler.getCookies());

		List<Task> arbeitList = new ArrayList<>();
		for(Element arbeitElement : arbeitenElements)
		{
			arbeitList.add(makeInternetTask(arbeitElement.select("form")));
		}

		return arbeitList;
	}
}
