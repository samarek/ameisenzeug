package pa.soft.data.managers.internet;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.internet.Immobilie;
import pa.soft.data.managers.AbstractManager;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.List;

public class ImmobilienManager extends AbstractManager
{
	public static List<Immobilie> fetchImmobilien(Spieler spieler)
	{
		Elements immobilienElements = Collector.fetchImmobilienElements(spieler.getCookies());

		List<Immobilie> immobilienList = new ArrayList<>();
		for(Element immobilienElement : immobilienElements)
		{
			immobilienList.add(makeImmobilie(immobilienElement));
		}

		return immobilienList;
	}

	private static Immobilie makeImmobilie(Element immobilienElement)
	{
		Immobilie immobilie = new Immobilie();

		immobilie.setId(extractId(immobilienElement));
		immobilie.setName(immobilienElement.select("b").text());
		immobilie.setBeschreibung(extractBeschreibung(immobilienElement));
		immobilie.setRegalplätze(extractRegalplätze(immobilienElement));
		immobilie.setPreis(extractPreis(immobilienElement));

		return immobilie;
	}

	private static String extractBeschreibung(Element element)
	{
		String elementHtml = element.select("td:nth-child(2)").html();
		Integer firstBr = elementHtml.indexOf("<br>") + 4;
		elementHtml = elementHtml.substring(firstBr, elementHtml.indexOf("<br>", firstBr));

		return elementHtml;
	}

	private static Integer extractRegalplätze(Element element)
	{
		String elementHtml = element.select("td:nth-child(2)").html();
		Integer doppelpunkt = elementHtml.indexOf(":") + 1;
		Integer paragraph = elementHtml.indexOf("<p>");

		return Integer.parseInt(elementHtml.substring(doppelpunkt, paragraph).trim());
	}
}
