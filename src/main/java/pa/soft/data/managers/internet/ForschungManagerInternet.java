package pa.soft.data.managers.internet;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.Task;
import pa.soft.data.managers.InternetTaskManager;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.List;

public class ForschungManagerInternet extends InternetTaskManager
{
	public static List<Task> fetchForschungen(Spieler spieler)
	{
		Elements forschungElements = Collector.fetchForschungenElements(spieler.getCookies());

		List<Task> forschungList = new ArrayList<>();
		for(Element forschungElement : forschungElements)
		{
			forschungList.add(makeInternetTask(forschungElement.select("form")));
		}

		return forschungList;
	}
}
