package pa.soft.data.managers.internet;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.details.Ameisen;
import pa.soft.data.entities.internet.details.SellableAmeisen;
import pa.soft.data.entities.draussen.details.Kolonie;
import pa.soft.data.entities.internet.Kleinanzeige;
import pa.soft.data.entities.internet.details.KleinanzeigeAmeisen;
import pa.soft.data.entities.internet.details.KleinanzeigeSonstiges;
import pa.soft.data.entities.internet.details.SellableSonstiges;
import pa.soft.data.managers.AbstractManager;
import pa.soft.data.types.NestType;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class KleinanzeigenManager extends AbstractManager
{
	public static List<KleinanzeigeAmeisen> fetchKleinanzeigenAmeisen(Spieler spieler)
	{
		Elements kleinanzeigenElements = Collector.fetchKleinanzeigenAmeisen(spieler.getCookies());

		List<KleinanzeigeAmeisen> kleinanzeigen = new ArrayList<>();
		for(Element kleinanzeigenElement : kleinanzeigenElements)
		{
			kleinanzeigen.add(makeKleinanzeigeAmeise(kleinanzeigenElement));
		}

		return kleinanzeigen;
	}

	private static KleinanzeigeAmeisen makeKleinanzeigeAmeise(Element kleinanzeigenElement)
	{
		KleinanzeigeAmeisen kleinanzeigeAmeisen = new KleinanzeigeAmeisen();
		kleinanzeigeAmeisen.setPreis(extractMoney(kleinanzeigenElement.select("td:nth-child(5)").text()));
		kleinanzeigeAmeisen.setVerkäufer(kleinanzeigenElement.select("td:nth-child(6)").text());
		kleinanzeigeAmeisen.setKolonie(makeKolonie(kleinanzeigenElement));

		return kleinanzeigeAmeisen;
	}

	private static Kolonie makeKolonie(Element kleinanzeigenElement)
	{
		Kolonie kolonie = new Kolonie();
		kolonie.setSpezies(kleinanzeigenElement.select("td:nth-child(1)").text());
		String queensString = kleinanzeigenElement.select("td:nth-child(2)").text();
		kolonie.setQueens(Integer.parseInt(queensString));
		String dronesString = kleinanzeigenElement.select("td:nth-child(3)").text();
		kolonie.setDrones(Integer.parseInt(dronesString));
		String nestTypeString = kleinanzeigenElement.select("td:nth-child(4)").text();
		kolonie.setNestType(NestType.fromString(nestTypeString));

		return kolonie;
	}

	public static List<Kleinanzeige> fetchKleinanzeigenSonstiges(Spieler spieler)
	{
		Elements kleinanzeigenElements = Collector.fetchKleinanzeigenSonstiges(spieler.getCookies());

		List<Kleinanzeige> kleinanzeigen = new ArrayList<>();
		for(Element kleinanzeigenElement : kleinanzeigenElements)
		{
			kleinanzeigen.add(makeKleinanzeigeSonstiges(kleinanzeigenElement));
		}

		return kleinanzeigen;
	}

	private static KleinanzeigeSonstiges makeKleinanzeigeSonstiges(Element kleinanzeigenElement)
	{
		KleinanzeigeSonstiges kleinanzeigeSonstiges = new KleinanzeigeSonstiges();
		kleinanzeigeSonstiges.setName(kleinanzeigenElement.select("td:nth-child(1)").text());
		String anzahlString = kleinanzeigenElement.select("td:nth-child(2)").text();
		kleinanzeigeSonstiges.setAnzahl(Integer.parseInt(anzahlString));
		kleinanzeigeSonstiges.setPreis(extractMoney(kleinanzeigenElement.select("td:nth-child(3)").text()));
		kleinanzeigeSonstiges.setProStück(extractMoney(kleinanzeigenElement.select("td:nth-child(4)").text()));
		kleinanzeigeSonstiges.setVerkäufer(kleinanzeigenElement.select("td:nth-child(5)").text());

		return kleinanzeigeSonstiges;
	}

	private static Integer extractMoney(String moneyString)
	{
		moneyString = moneyString.replace(",", "");
		moneyString = moneyString.replace(".", "");
		moneyString = moneyString.replace("€", "");
		moneyString = moneyString.replace("c", "");

		return Integer.parseInt(moneyString.trim());
	}

	public static List<SellableAmeisen> fetchSellableAmeisen(Spieler spieler)
	{
		Elements sellableAmeisenElements = Collector.fetchKleinanzeigenSellableAmeisen(spieler.getCookies());

		List<SellableAmeisen> sellableAmeisen = new ArrayList<>();
		for(Element sellableAmeisenElement : sellableAmeisenElements)
		{
			sellableAmeisen.add(makeSellableAmeisen(sellableAmeisenElement));
		}
		sellableAmeisen.sort(Comparator.comparing(Ameisen::getName));


		return sellableAmeisen;
	}

	private static SellableAmeisen makeSellableAmeisen(Element sellableAmeisenElement)
	{
		SellableAmeisen sellableAmeisen = new SellableAmeisen();
		String sellableAmeisenElementText = sellableAmeisenElement.text();
		sellableAmeisen.setId(sellableAmeisenElement.val());
		Integer indexOpenBracket = sellableAmeisenElementText.indexOf("(");
		Integer indexKPoint = sellableAmeisenElementText.indexOf("K.");
		Integer indexAPoint = sellableAmeisenElementText.indexOf("A.");
		Integer indexIn = sellableAmeisenElementText.indexOf("in");
		sellableAmeisen.setName(sellableAmeisenElementText.substring(0, indexOpenBracket).trim());
		Integer queens = Integer.valueOf(sellableAmeisenElementText.substring(indexOpenBracket + 1, indexKPoint ).trim());
		sellableAmeisen.setQueens(queens);
		Integer drones = Integer.valueOf(sellableAmeisenElementText.substring(indexKPoint + 2, indexAPoint ).trim());
		sellableAmeisen.setDrones(drones);
		String nestString = sellableAmeisenElementText.substring(indexIn + 2).trim();
		sellableAmeisen.setNestType(NestType.fromString(nestString));

		return sellableAmeisen;
	}

	public static List<SellableSonstiges> fetchSellableSonstiges(Spieler spieler)
	{
		Elements sellableSonstigesElements = Collector.fetchKleinanzeigenSellableSonstiges(spieler.getCookies());

		List<SellableSonstiges> sellableSonstiges = new ArrayList<>();
		for(Element sellableAmeisenElement : sellableSonstigesElements)
		{
			sellableSonstiges.add(makeSellableSonstiges(sellableAmeisenElement));
		}
		sellableSonstiges.sort(Comparator.comparing(SellableSonstiges::getAmount));

		return sellableSonstiges;
	}

	private static SellableSonstiges makeSellableSonstiges(Element sellableSonstigesElement)
	{
		SellableSonstiges sellableSonstiges = new SellableSonstiges();
		String sellableSonstigesElementText = sellableSonstigesElement.text();
		sellableSonstiges.setId(sellableSonstigesElement.val());
		Integer indexOpenBracket = sellableSonstigesElementText.indexOf("(");
		Integer indexVorhanden = sellableSonstigesElementText.indexOf("vorhanden");
		sellableSonstiges.setName(sellableSonstigesElementText.substring(0, indexOpenBracket).trim());
		sellableSonstiges.setAmount(Integer.valueOf(sellableSonstigesElementText.substring(indexOpenBracket + 1, indexVorhanden).trim()));

		return sellableSonstiges;
	}
}
