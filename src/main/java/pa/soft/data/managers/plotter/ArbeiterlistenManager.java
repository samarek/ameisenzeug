package pa.soft.data.managers.plotter;

import pa.soft.data.entities.plotter.ArbeiterListe;
import pa.soft.data.entities.plotter.KursList;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ArbeiterlistenManager
{
	public static Map<String, KursList> fetchArbeiterlisten(File file)
	{
		Map<String, KursList> arbeiterListe = new HashMap<>();
		if(file.exists())
		{
			try
			{
				Scanner fileScanner = new Scanner(file);
				String line;
				while(fileScanner.hasNextLine())
				{
					line = fileScanner.nextLine();
					arbeiterListe.put(line.split("=")[0] , new ArbeiterListe(line));
				}
			}
			catch(FileNotFoundException e)
			{
				e.printStackTrace();
			}
		}

		return arbeiterListe;
	}
}
