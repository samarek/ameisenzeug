package pa.soft.data.managers.plotter;

import pa.soft.data.entities.plotter.KursList;
import pa.soft.data.entities.plotter.PreisListe;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class PreislistenManager
{
	public static Map<String, KursList> fetchPreislisten(File file)
	{
		Map<String, KursList> preisListen = new HashMap<>();
		if(file.exists())
		{
			try
			{
				Scanner fileScanner = new Scanner(file);
				String line;
				while(fileScanner.hasNextLine())
				{
					line = fileScanner.nextLine();
					preisListen.put(line.split("=")[0] , new PreisListe(line));
				}
			}
			catch(FileNotFoundException e)
			{
				e.printStackTrace();
			}
		}

		return preisListen;
	}
}
