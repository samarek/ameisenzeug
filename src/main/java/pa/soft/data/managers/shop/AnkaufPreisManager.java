package pa.soft.data.managers.shop;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenshop.AnkaufPreis;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.List;

public class AnkaufPreisManager
{
	public static List<AnkaufPreis> fetchAnkaufPreise(Spieler spieler)
	{
		Elements ankaufPreisElemente = Collector.fetchAmeisenAnkaufPreise(spieler.getCookies());

		List<AnkaufPreis> ankaufPreise = new ArrayList<>();

		for(Element ankaufPreisElement : ankaufPreisElemente)
		{
			String name = ankaufPreisElement.select("td").get(0).text();
			String preisString = ankaufPreisElement.select("td").get(1).text();
			preisString = preisString.substring(0, preisString.indexOf("€") - 1)
					.replace(",", "")
					.replace("", "");
			Integer preis = Integer.valueOf(preisString);

			ankaufPreise.add(new AnkaufPreis(name, preis));
		}

		return ankaufPreise;
	}
}
