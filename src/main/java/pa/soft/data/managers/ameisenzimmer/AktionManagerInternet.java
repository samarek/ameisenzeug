package pa.soft.data.managers.ameisenzimmer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.Task;
import pa.soft.data.managers.InternetTaskManager;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.List;

public class AktionManagerInternet extends InternetTaskManager
{
	public static List<Task> fetchAktionen(Spieler spieler)
	{
		Elements aktionenElements = Collector.fetchAktionenElements(spieler.getCookies());

		List<Task> aktionenList = new ArrayList<>();
		for(Element aktionenElement : aktionenElements)
		{
			aktionenList.add(makeInternetTask(aktionenElement.select("form")));
		}

		return aktionenList;
	}
}
