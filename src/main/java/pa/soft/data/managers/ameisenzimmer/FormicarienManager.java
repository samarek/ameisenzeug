package pa.soft.data.managers.ameisenzimmer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Formicarium;
import pa.soft.data.managers.AbstractManager;
import pa.soft.data.managers.ameisenzimmer.details.BrutManager;
import pa.soft.data.managers.ameisenzimmer.details.FormicariumDetailsManager;
import pa.soft.data.types.FormicariumType;
import pa.soft.data.types.NestType;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FormicarienManager extends AbstractManager
{
	private static Map<String, String> cookies = new HashMap<>();

	private static List<Formicarium> formicarien;

	public static List<Formicarium> fetchFormicarien(Spieler spieler)
	{
		cookies = spieler.getCookies();

		if(formicarien == null || formicarien.size() == 0)
		{
			Elements formicarienElements = Collector.fetchFormicariumElements(cookies);
			formicarienElements = removeEinrichtenShopitems(formicarienElements);
			formicarien = new ArrayList<>();

			for(Element element : formicarienElements)
			{
				formicarien.add(makeFormicarium(element));
			}
		}

		return formicarien;
	}

	public static Formicarium fetchFormicarienById(Spieler spieler, Integer id)
	{
		cookies = spieler.getCookies();

		if(formicarien == null || formicarien.size() == 0)
		{
			fetchFormicarien(spieler);
		}

		Formicarium outputFormicarium = null;
		for(Formicarium formicarium : formicarien)
		{
			if(formicarium.getId().equals(id))
			{
				outputFormicarium = formicarium;
				break;
			}
		}

		return outputFormicarium;
	}

	private static Formicarium makeFormicarium(Element element)
	{
		Formicarium formicarium = extractAttributes(element);

		formicarium.setBrut(BrutManager.extractBrood(element));
		formicarium.setFormicariumDetails(FormicariumDetailsManager.fetchDetails(formicarium, cookies));

		return formicarium;
	}

	private static Formicarium extractAttributes(Element element)
	{
		Formicarium formicarium = new Formicarium();
		formicarium.setId(extractId(element));
		Elements attributeElements = element.getElementsByAttributeValue("id", "group").select("tbody tr");

		for(Element attributeElement : attributeElements)
		{
			String key = extractKey(attributeElement);
			String value = extractStringValue(attributeElement);

			if(key.equals("Art"))
			{
				formicarium.setAmeisenType(value);
			}
			if(key.contains("Formicarium"))
			{
				formicarium.setFormicariumType(FormicariumType.fromString(value));
			}
			if(key.contains("Nest"))
			{
				formicarium.setNestType(NestType.fromString(value));
			}
			if(key.contains("Punkte:"))
			{
				formicarium.setPunkte(extractIntegerValue(attributeElement));
			}
			if(key.contains("(Zucker)"))
			{
				formicarium.setZuckerhunger(extractDoubleValue(attributeElement));
			}
			if(key.contains("Körnervorrat"))
			{
				formicarium.setSamenvorrat(extractDoubleValue(attributeElement));
			}
			if(key.contains("Pilzstatus"))
			{
				formicarium.setPilzstatus(extractDoubleValue(attributeElement));
			}
			if(key.contains("(Proteine)"))
			{
				formicarium.setProteinhunger(extractDoubleValue(attributeElement));
			}
		}

		return formicarium;
	}

	protected static Integer extractId(Element element)
	{
		String idString = element.select("tbody tr td").get(0).select("a").attr("href");

		return Integer.parseInt(idString.substring(idString.lastIndexOf("=") + 1));
	}

	private static Double extractDoubleValue(Element attributeElement)
	{
		String content = attributeElement.select("td").get(1).toString();

		Integer start = content.indexOf("'),") + 4;
		Integer ende =  content.indexOf(",", start);

		return Double.parseDouble(content.substring(start, ende));
	}
}
