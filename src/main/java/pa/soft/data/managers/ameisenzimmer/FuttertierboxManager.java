package pa.soft.data.managers.ameisenzimmer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Futtertierbox;
import pa.soft.data.managers.AbstractManager;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.List;

public class FuttertierboxManager extends AbstractManager
{
	public static List<Futtertierbox> fetchFuttertierboxen(Spieler spieler)
	{
		Elements futtertierboxElements = Collector.fetchFuttertierboxElements(spieler.getCookies());
		futtertierboxElements = removeEinrichtenShopitems(futtertierboxElements);
		List<Futtertierbox> futtertierboxen = new ArrayList<>();

		for(Element futtertierboxElement : futtertierboxElements)
		{
			futtertierboxen.add(makeFuttertierbox(futtertierboxElement));
		}

		return futtertierboxen;
	}

	private static Futtertierbox makeFuttertierbox(Element futtertierboxElement)
	{
		Futtertierbox futtertierbox = extractAttributes(futtertierboxElement);
		futtertierbox.setId(extractId(futtertierboxElement));

		return futtertierbox;
	}

	private static Futtertierbox extractAttributes(Element futtertierboxElement)
	{
		Futtertierbox futtertierbox = new Futtertierbox();

		Elements attributeElements = futtertierboxElement.select("tbody tr");

		for(Element attributeElement : attributeElements)
		{
			String key = extractKey(attributeElement);
			String value = extractStringValue(attributeElement);
			if(key.equals("Art"))
			{
				futtertierbox.setSpecies(extractSpecies(value));
				futtertierbox.setSpeciesStorage(extractSpeciesStorage(value));
			}
			if(key.equals("Behälter:"))
			{
				futtertierbox.setBig(isBigContainer(value));
			}
			if(key.equals("Anzahl:"))
			{
				futtertierbox.setCount(Integer.parseInt(value));
			}
			if(key.contains("Platzverbrauch:"))
			{
				futtertierbox.setRoom(extractPercentValue(value));
			}
			if(key.contains("Status:"))
			{
				futtertierbox.setState(extractPercentValue(value));
			}
		}

		return futtertierbox;
	}

	protected static Integer extractId(Element futtertierboxElement)
	{
		Element element = futtertierboxElement.select("form input[name=\"boxid\"]").get(0);

		return Integer.parseInt(element.attr("value"));
	}

	private static String extractSpecies(String value)
	{
		return value.contains("auf Lager") ? value.substring(0, value.indexOf("(")).trim() : value;
	}

	private static Integer extractSpeciesStorage(String value)
	{
		return value.contains("auf Lager") ? Integer.parseInt(value.substring(value.indexOf("Lager:")+6, value.indexOf(")")).trim()) : 0;
	}

	private static Boolean isBigContainer(String value)
	{
		return value.equals("großer Zuchtbehälter");
	}

	private static Integer extractPercentValue(String value)
	{
		return Integer.parseInt(value.replace("%", ""));
	}
}
