package pa.soft.data.managers.ameisenzimmer.details;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.ameisenzimmer.Formicarium;
import pa.soft.data.entities.ameisenzimmer.details.Ausbruchschutz;
import pa.soft.data.entities.ameisenzimmer.details.FormicariumDetails;
import pa.soft.data.entities.ameisenzimmer.details.FutterItem;
import pa.soft.data.types.AusbruchschutzType;
import pa.soft.data.types.FutterType;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class FormicariumDetailsManager
{
	public static FormicariumDetails fetchDetails(Formicarium formicarium, Map<String, String> cookies)
	{
		Elements detailElements = Collector.fetchFormicariumDetails(formicarium.getId(), cookies);

		FormicariumDetails formicariumDetails = new FormicariumDetails();
		formicariumDetails.setFutterItems(extractFutterItems(detailElements.get(1)));
		formicariumDetails.setAusbruchschutz(extractAusbruchschutz(detailElements.get(2)));

		Elements futterElements = detailElements.get(1).select("select option");
		formicariumDetails.setAvailableFutterItems(extractAvailableFutterItems(futterElements));

		for(Element element : detailElements)
		{
			String elementContent = element.select("tbody script").toString();
			if(elementContent.contains("'spaceformi'"))
			{
				formicariumDetails.setFormicariumZustand(extractFormicariumZustand(elementContent));
			}
			if(elementContent.contains("'spacenest'"))
			{
				formicariumDetails.setNestZustand(extractNestZustand(elementContent));
			}
		}

		return formicariumDetails;
	}

	private static List<FutterItem> extractAvailableFutterItems(Elements futterElements)
	{
		String idString;
		String futtername;
		String grammString;

		List<FutterItem> futterItems = new ArrayList<>();
		FutterItem futterItem;

		for(Element futterElement : futterElements)
		{
			idString = futterElement.val();
			futtername = futterElement.text().substring(0, futterElement.text().indexOf("(")).trim();
			grammString = futterElement.text().substring(futterElement.text().indexOf("(max") + 4, futterElement.text().indexOf("g)")).trim();

			futterItem = new FutterItem();
			futterItem.setId(Integer.valueOf(idString));
			futterItem.setName(futtername);
			futterItem.setGramm(Double.valueOf(grammString));
			futterItem.setFutterType(FutterType.fromFutterString(futtername));

			futterItems.add(futterItem);
		}

		return futterItems;
	}

	private static Ausbruchschutz extractAusbruchschutz(Element element)
	{
		Ausbruchschutz ausbruchschutz = new Ausbruchschutz();

		String nameString = element.select("b").text();
		nameString = nameString.split(" ")[1];
		ausbruchschutz.setType(AusbruchschutzType.fromString(nameString));

		Double zustand = extractZustand(element.select("tbody script").toString(), "escape'),");
		ausbruchschutz.setZustand(zustand);

		return ausbruchschutz;
	}

	private static Double extractFormicariumZustand(String elementContent)
	{
		String identifier = "spaceformi'),";

		return extractZustand(elementContent, identifier);
	}

	private static Double extractNestZustand(String elementContent)
	{
		String identifier = "spacenest'),";

		return extractZustand(elementContent, identifier);
	}

	private static Double extractZustand(String elementContent, String identifier)
	{
		Integer identifierStart = elementContent.indexOf(identifier) + identifier.length();
		String zustandString = elementContent.substring(identifierStart, elementContent.indexOf(",", identifierStart));

		return Double.parseDouble(zustandString.trim());
	}

	private static List<FutterItem> extractFutterItems(Element detailElement)
	{
		Elements futterElements = detailElement.select("tbody tr[class^=\"color\"]");

		List<FutterItem> futterItems = new ArrayList<>();

		for(Element futterElement : futterElements)
		{
			Elements elements = futterElement.select("td");
			if(elements.size() > 0)
			{
				futterItems.add(makeFutterItem(elements));
			}
		}

		return futterItems;
	}

	private static FutterItem makeFutterItem(Elements elements)
	{
		FutterItem futterItem = new FutterItem();

		String idString = elements.select("a").attr("href");
		idString = idString.substring(idString.indexOf("foodid=") + 7);

		futterItem.setId(Integer.valueOf(idString));
		futterItem.setName(elements.get(0).text());
		futterItem.setGramm(Double.parseDouble(elements.get(1).text()));
		futterItem.setZustand(Integer.parseInt(elements.get(3).text().replace("%","")));
		futterItem.setFutterType(FutterType.fromFutterString(futterItem.getName()));

		return futterItem;
	}
}
