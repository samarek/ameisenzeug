package pa.soft.data.managers.ameisenzimmer.details;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.ameisenzimmer.details.Brut;
import pa.soft.data.managers.AbstractManager;

public class BrutManager extends AbstractManager
{
	public static Brut extractBrood(Element element)
	{
		Elements broodElements = element.getElementsByAttributeValue("class", "liste").select("tbody").select("tr");
		Brut brood = new Brut();
		for(Element listeElement : broodElements)
		{
			switch(extractKey(listeElement))
			{
				case "Königinnen:":
					brood.setQueens(extractIntegerValue(listeElement));
					break;
				case "Arbeiterinnen:":
					brood.setDrones(extractIntegerValue(listeElement));
					break;
				case "Eier:":
					brood.setEggs(extractIntegerValue(listeElement));
					break;
				case "Larven:":
					brood.setLarvae(extractIntegerValue(listeElement));
					break;
				case "Puppen:":
					brood.setCocoons(extractIntegerValue(listeElement));
					break;
			}
		}

		return brood;
	}
}
