package pa.soft.data.managers.ameisenzimmer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Lager;
import pa.soft.data.entities.ameisenzimmer.details.Ameisen;
import pa.soft.data.entities.ameisenzimmer.details.LagerItem;
import pa.soft.data.managers.AbstractManager;
import pa.soft.data.types.NestType;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.List;

public class LagerManager extends AbstractManager
{
	public static Lager fetchLager(Spieler spieler)
	{
		Elements lagerElements = Collector.fetchLager(spieler.getCookies());

		return makeLager(lagerElements);
	}

	private static Lager makeLager(Elements lagerElements)
	{
		Lager lager = new Lager();
		lager.setAmeisen(extractAmeisen(lagerElements));
		lager.setFutter(extractLagerItems(lagerElements, 2));
		lager.setZubehör(extractLagerItems(lagerElements, 3));
		lager.setFormicarienNester(extractLagerItems(lagerElements, 4));
		lager.setMöbel(extractLagerItems(lagerElements, 5));

		return lager;
	}

	private static List<Ameisen> extractAmeisen(Elements lagerElements)
	{
		Elements ameisenElemente = lagerElements.select("#text > table:nth-of-type(1) tr:not(.liste)");

		List<Ameisen> ameisenList = new ArrayList<>();
		for (Element ameisenElement : ameisenElemente)
		{
			ameisenList.add(makeAmeisen(ameisenElement));
		}

		return ameisenList;
	}

	private static Ameisen makeAmeisen(Element ameisenElement)
	{
		Ameisen ameisen = new Ameisen();
		ameisen.setName(ameisenElement.select("td").get(0).text().trim());
		String queenString = ameisenElement.select("td").get(1).text().trim();
		ameisen.setQueens(Integer.parseInt(queenString));
		String dronesString = ameisenElement.select("td").get(2).text().trim();
		ameisen.setDrones(Integer.parseInt(dronesString));
		String nestTypeString = ameisenElement.select("td").get(3).text().trim();
		ameisen.setNestType(NestType.fromString(nestTypeString));

		return ameisen;
	}

	private static List<LagerItem> extractLagerItems(Elements lagerElements, Integer count)
	{
		Elements lagerItemElemente = lagerElements.select("#text > table:nth-of-type(" + count + ") tr:not(.liste)");

		List<LagerItem> lagerItemList = new ArrayList<>();
		for(Element lagerItemElement : lagerItemElemente)
		{
			lagerItemList.add(makeLagerItem(lagerItemElement));
		}

		return lagerItemList;
	}

	private static LagerItem makeLagerItem(Element lagerItemElement)
	{
		LagerItem lagerItem = new LagerItem();
		lagerItem.setName(lagerItemElement.select("td").get(0).text().trim());
		String mengeString = lagerItemElement.select("td").get(1).text().trim();
		lagerItem.setMenge(Integer.parseInt(mengeString));

		return lagerItem;
	}
}

