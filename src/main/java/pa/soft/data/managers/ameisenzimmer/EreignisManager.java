package pa.soft.data.managers.ameisenzimmer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Ereignis;
import pa.soft.data.managers.AbstractManager;
import pa.soft.http.Collector;
import pa.soft.processors.Util;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class EreignisManager extends AbstractManager
{
	public static List<Ereignis> fetchEreignisse(Spieler spieler)
	{
		Elements ereignisElements = Collector.fetchEreignisse(spieler.getCookies());

		List<Ereignis> ereignisList = new ArrayList<>();
		for(Element ereigniselement : ereignisElements)
		{
			ereignisList.add(makeEreignis(ereigniselement));
		}

		return ereignisList;
	}

	private static Ereignis makeEreignis(Element ereigniselement)
	{
		Ereignis ereignis = new Ereignis();
		String datumString = ereigniselement.select("td:nth-child(1)").text();
		ereignis.setZeitpunkt(LocalDateTime.parse(datumString, Util.FORMATTER_LONG_YEAR));
		ereignis.setEreignis(ereigniselement.select("td:nth-child(2)").text());

		return ereignis;
	}
}
