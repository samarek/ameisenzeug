package pa.soft.data.managers.ameisenzimmer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Möbel;
import pa.soft.data.entities.internet.MöbelAngebot;
import pa.soft.data.managers.AbstractManager;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.List;

public class MöbelManager extends AbstractManager
{
	public static List<Möbel> fetchMöbel(Spieler spieler)
	{
		Elements möbelElements = Collector.fetchMöbel(spieler.getCookies());

		List<Möbel> möbel = new ArrayList<>();
		for(Element möbelElement : möbelElements)
		{
			möbel.add(makeMöbel(möbelElement));
		}

		return möbel;
	}

	private static Möbel makeMöbel(Element möbelElement)
	{
		Möbel möbelStück = new Möbel();
		möbelStück.setName(möbelElement.select("table td:nth-child(2) b").text());
		String plätzeInsgesamt = möbelElement.select("table table tr:nth-child(1) td:nth-child(2)").text();
		möbelStück.setPlätzeInsgesamt(Integer.valueOf(plätzeInsgesamt));
		String plätzeFrei = möbelElement.select("table table tr:nth-child(2) td:nth-child(2)").text();
		möbelStück.setPlätzeFrei(Double.valueOf(plätzeFrei));

		return möbelStück;
	}

	public static List<MöbelAngebot> fetchMöbelAngebote(Spieler spieler)
	{
		Elements möbelAngebotElements = Collector.fetchMöbelAngebote(spieler.getCookies());

		List<MöbelAngebot> möbelAngebote = new ArrayList<>();
		for(Element möbelAngebotElement : möbelAngebotElements)
		{
			möbelAngebote.add(makeMöbelAngebot(möbelAngebotElement));
		}

		return möbelAngebote;
	}

	private static MöbelAngebot makeMöbelAngebot(Element möbelElement)
	{
		MöbelAngebot möbelAngebot = new MöbelAngebot();

		möbelAngebot.setId(extractId(möbelElement));
		möbelAngebot.setName(möbelElement.select("b").text());
		möbelAngebot.setBeschreibung(extractBeschreibung(möbelElement));
		möbelAngebot.setPreis(extractPreis(möbelElement));

		return möbelAngebot;
	}

	private static String extractBeschreibung(Element element)
	{
		String elementHtml = element.select("td:nth-child(3)").html();
		Integer firstBr = elementHtml.indexOf("<br>") + 4;
		elementHtml = elementHtml.substring(firstBr, elementHtml.indexOf("<p>", firstBr));

		return elementHtml;
	}
}
