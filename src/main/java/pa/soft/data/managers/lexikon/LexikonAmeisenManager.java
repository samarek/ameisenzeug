package pa.soft.data.managers.lexikon;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.lexikon.LexikonAmeise;
import pa.soft.http.Collector;

import java.util.ArrayList;
import java.util.List;

public class LexikonAmeisenManager
{
	public static List<LexikonAmeise> fetchLexikonAmeisen(Spieler spieler)
	{
		Elements lexikonAmeisenElements = Collector.fetchLexikonAmeisen(spieler.getCookies());

		List<LexikonAmeise> lexikonAmeiseList = new ArrayList<>();
		for(Element lexikonAmeisenElement : lexikonAmeisenElements)
		{
			lexikonAmeiseList.add(extractLexikonAmeisen(lexikonAmeisenElement));
		}

		return lexikonAmeiseList;
	}

	private static LexikonAmeise extractLexikonAmeisen(Element einheimischeAmeisenElement)
	{
		LexikonAmeise lexikonAmeise = new LexikonAmeise();

		lexikonAmeise.setName(extractName(einheimischeAmeisenElement));
		lexikonAmeise.setBeschreibung(extractBeschreibung(einheimischeAmeisenElement));
		lexikonAmeise.setVerfügbarInShop(extractVerfügbarInShop(einheimischeAmeisenElement));
		lexikonAmeise.setMonogyn(extractMonogyn(einheimischeAmeisenElement));
		lexikonAmeise.setMaxKoloniegröße(extractMaxKoloniegröße(einheimischeAmeisenElement));
		lexikonAmeise.setPunkteProArbeiterin(extractPunkteProArbeiterin(einheimischeAmeisenElement));
		lexikonAmeise.setHaltungMöglich(extractHaltungMöglich(einheimischeAmeisenElement));
		lexikonAmeise.setSchonFindbar(extractSchonFindbar(einheimischeAmeisenElement));
		lexikonAmeise.setEiEntwicklung(extractEiEntwicklung(einheimischeAmeisenElement));
		lexikonAmeise.setLarvenEntwicklung(extractLarvenEntwicklung(einheimischeAmeisenElement));
		lexikonAmeise.setPuppenEntwicklung(extractPuppenEntwicklung(einheimischeAmeisenElement));
		lexikonAmeise.setPlatzbedarfNest(extractPlatzbedarfNest(einheimischeAmeisenElement));
		lexikonAmeise.setPlatzbedarfFormicarium(extractPlatzbedarfFormicarium(einheimischeAmeisenElement));

		return lexikonAmeise;
	}

	private static String extractName(Element einheimischeAmeisenElement)
	{
		return einheimischeAmeisenElement.select("td > center > b").text();
	}

	private static String extractBeschreibung(Element einheimischeAmeisenElement)
	{
		String ameisenBeschreibung = einheimischeAmeisenElement.select("tbody td:nth-child(1)").get(0).text();

		return ameisenBeschreibung;
	}

	private static Boolean extractVerfügbarInShop(Element einheimischeAmeisenElement)
	{
		String verfügbarInShop = einheimischeAmeisenElement.select("tr[class^=\"color\"]:nth-child(1) td:nth-child(2)").text();

		return verfügbarInShop.equals("Ja");
	}

	private static Boolean extractMonogyn(Element einheimischeAmeisenElement)
	{
		String monogyn = einheimischeAmeisenElement.select("tr[class^=\"color\"]:nth-child(2) td:nth-child(2)").text();

		return monogyn.equals("monogyn");
	}

	private static Integer extractMaxKoloniegröße(Element einheimischeAmeisenElement)
	{
		String maxKoloniegröße = einheimischeAmeisenElement.select("tr[class^=\"color\"]:nth-child(3) td:nth-child(2)").text();

		return Integer.valueOf(maxKoloniegröße);
	}

	private static Double extractPunkteProArbeiterin(Element einheimischeAmeisenElement)
	{
		String punkteProArbeiterin = einheimischeAmeisenElement.select("tr[class^=\"color\"]:nth-child(4) td:nth-child(2)").text();

		return Double.valueOf(punkteProArbeiterin);
	}

	private static Boolean extractHaltungMöglich(Element einheimischeAmeisenElement)
	{
		String haltungMöglich = einheimischeAmeisenElement.select("tr[class^=\"color\"]:nth-child(5) td:nth-child(2)").text();

		return haltungMöglich.equals("Ja");
	}

	private static Boolean extractSchonFindbar(Element einheimischeAmeisenElement)
	{
		String schonFindbar = einheimischeAmeisenElement.select("tr[class^=\"color\"]:nth-child(6) td:nth-child(2)").text();

		return schonFindbar.equals("Ja");
	}

	private static Integer extractEiEntwicklung(Element einheimischeAmeisenElement)
	{
		String eiEntwicklung = einheimischeAmeisenElement.select("tr[class^=\"color\"]:nth-child(7) td:nth-child(2)").text();

		return Integer.valueOf(eiEntwicklung.replace("h", "").trim());
	}

	private static Integer extractLarvenEntwicklung(Element einheimischeAmeisenElement)
	{
		String larvenEntwicklung = einheimischeAmeisenElement.select("tr[class^=\"color\"]:nth-child(8) td:nth-child(2)").text();

		return Integer.valueOf(larvenEntwicklung.replace("h", "").trim());
	}

	private static Integer extractPuppenEntwicklung(Element einheimischeAmeisenElement)
	{
		String puppenEntwicklung = einheimischeAmeisenElement.select("tr[class^=\"color\"]:nth-child(9) td:nth-child(2)").text();

		return Integer.valueOf(puppenEntwicklung.replace("h", "").trim());
	}

	private static Double extractPlatzbedarfNest(Element einheimischeAmeisenElement)
	{
		String puppenEntwicklung = einheimischeAmeisenElement.select("tr[class^=\"color\"]:nth-child(10) td:nth-child(2)").text();

		return Double.valueOf(puppenEntwicklung);
	}

	private static Double extractPlatzbedarfFormicarium(Element einheimischeAmeisenElement)
	{
		String puppenEntwicklung = einheimischeAmeisenElement.select("tr[class^=\"color\"]:nth-child(11) td:nth-child(2)").text();

		return Double.valueOf(puppenEntwicklung);
	}
}
