package pa.soft.data.managers;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import pa.soft.data.entities.internet.InternetTask;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class InternetTaskManager extends AbstractManager
{
	protected static InternetTask makeInternetTask(Elements form)
	{
		String content = form.html();

		InternetTask internetTask = new InternetTask();
		internetTask.setId(extractId(form));
		internetTask.setTitel(form.select("b").text());
		internetTask.setBeschreibung(extractBeschreibung(content));
		internetTask.setDauer(extractDauer(content));
		internetTask.setMoney(extractMoney(content));

		return internetTask;
	}

	protected static Integer extractId(Elements form)
	{
		String idString = form.select("input:nth-of-type(2)").val();

		return Integer.parseInt(idString);
	}

	protected static String extractBeschreibung(String content)
	{
		content = content.substring(content.indexOf("<br>") + 4);
		Integer startIndex = content.indexOf("<br>") + 4;
		String beschreibung = content.substring(startIndex, content.indexOf("<br>", startIndex)).trim();

		return Jsoup.parse(beschreibung).text();
	}

	protected static Duration extractDauer(String content)
	{
		Duration duration = null;

		if(!content.contains("taskcounter"))
		{
			Integer dauerStart = content.indexOf("Dauer:") + 6;
			String dauerString = content.substring(dauerStart, content.indexOf("<br>", dauerStart)).trim();
			String[] dauerArray = dauerString.split(":");
			duration = Duration.of(Long.parseLong(dauerArray[0]), ChronoUnit.HOURS)
						   .plusMinutes(Long.parseLong(dauerArray[1]))
						   .plusSeconds(Long.parseLong(dauerArray[2]));
		}

		return duration;
	}

	protected static Integer extractMoney(String content)
	{
		Integer moneyStart = findStart(content);
		String moneyString = content.substring(moneyStart, content.indexOf("&#x20ac;"))
								   .replace(",", "")
								   .trim();

		return Integer.parseInt(moneyString);
	}

	private static Integer findStart(String content)
	{
		Integer moneyStart;
		if(content.contains("Lohn:"))
		{
			moneyStart = content.indexOf("Lohn:") + 5;
		}
		else
		{
			moneyStart = content.indexOf("Kosten:") + 7;
		}

		return moneyStart;
	}
}
