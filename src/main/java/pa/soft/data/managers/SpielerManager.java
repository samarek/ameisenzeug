package pa.soft.data.managers;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import pa.soft.data.entities.Spieler;
import pa.soft.http.Collector;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class SpielerManager
{
	private static final String COOKIES_FILE_PATH = "out/cookies";
	private static final List<Spieler> spielerList = Arrays.asList(
			new Spieler("cumeartand", "AmeisenPasswort"),
			new Spieler("panu", "test"),
			new Spieler("iad_uroboros", "test"),
			new Spieler("Railer_x3", "test"),
			new Spieler("witchita", "test")
	);

	public static Spieler fetchSpieler(String name)
	{
		Optional<Spieler> spielerOptional = spielerList.stream()
											   .filter(spieler -> spieler.getName().equalsIgnoreCase(name))
											   .findAny();
		if(spielerOptional.isPresent())
		{
			return spielerEinloggen(spielerOptional.get());
		}
		else
		{
			throw new RuntimeException("Spieler unbekannt");
		}
	}

	public static List<Spieler> fetchSpieler()
	{
		List<String> spielerStrings = Arrays.asList("cumeartand;AmeisenPasswort");
 		List<Spieler> spieler = new ArrayList<>();

		try
		{
//			Scanner scanner = new Scanner(new File("spieler"));
			String[] spielerArray;
//			while (scanner.hasNextLine())
			for (String spielerString : spielerStrings)
			{
//				spielerArray = scanner.nextLine().split(";");
				spielerArray = spielerString.split(";");
				spieler.add(new Spieler(spielerArray[0].trim(), spielerArray[1].trim()));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return spieler;
	}

	public static Spieler spielerEinloggen(Spieler spieler)
	{
		Connection.Response response = Collector.login(spieler);

		spieler.setCookies(response.cookies());

		Document responseDocument = Jsoup.parse(response.body());
		String spielerString = responseDocument.select("#fix #header #status tr:first-of-type td:first-of-type").text();
		String messageString = responseDocument.select("#fix #header #status tr:nth-of-type(3) td:first-of-type").text();

		spieler.setGeld(extractGeld(spielerString));
		spieler.setPunkte(extractPunkte(spielerString));
		spieler.setBusy(extractBusy(spielerString));
		spieler.setMessageCount(extractMessageCount(messageString));
		if(spieler.getMessageCount() > 0)
		{
			spieler.setMessages(MessageManager.fetchMessages(spieler));
		}

		return spieler;
	}

	private static Connection.Response fetchCookies(Spieler spieler)
	{
		File cookieFile = new File(COOKIES_FILE_PATH);
		Map<String, String> cookies = new HashMap<>();
		if(cookieFile.exists())
		{
			cookies = readCookies(cookieFile);
			spieler.setCookies(cookies);
		}

		Connection.Response response = Collector.cookieValidation(spieler);
		if(cookies.size() == 0 || !response.body().contains(spieler.getName()))
		{
			response = Collector.login(spieler);
			cookies = response.cookies();
			writeCookies(cookieFile, cookies);
		}

		return response;
	}

	private static HashMap<String, String> readCookies(File cookieFile)
	{
		HashMap<String, String> cookies = new HashMap<>();
		try
		{
			FileInputStream f = new FileInputStream(cookieFile);
			ObjectInputStream s = new ObjectInputStream(f);
			cookies = (HashMap<String, String>)s.readObject();
			s.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		return cookies;
	}

	private static void writeCookies(File cookieFile, Map<String, String> cookies)
	{
		try
		{
			FileOutputStream f = new FileOutputStream(cookieFile);
			ObjectOutputStream s = new ObjectOutputStream(f);
			s.writeObject(cookies);
			s.flush();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void updateMoney(Spieler spieler, Document responseDocument)
	{
		String playerString = extractPlayerString(responseDocument);
		spieler.setGeld(extractGeld(playerString));
	}

	private static String extractPlayerString(Document responseDocument)
	{
		Element playerElement = responseDocument.select("#fix #header #status tr:first-of-type td:first-of-type").first();

		return playerElement.text();
	}

	private static Integer extractGeld(String spielerString)
	{
		String geldString = spielerString.substring(spielerString.indexOf("Geld:") + 5, spielerString.indexOf("€"));
		geldString = geldString
				.replace(",", "")
				.replace(".", "")
				.trim();

		return Integer.parseInt(geldString);
	}

	private static Integer extractPunkte(String spielerString)
	{
		Integer ende = spielerString.contains("Aktion") ? spielerString.indexOf("Aktion") : spielerString.length();
		String punktString = spielerString.substring(spielerString.indexOf("Punkte:") + 7, ende);
		punktString = punktString.replace(".", "").trim();

		return Integer.parseInt(punktString);
	}

	private static Integer extractMessageCount(String messageString)
	{
		String count = messageString.contains("ungelesen") ? messageString.substring(messageString.indexOf("(") + 1, messageString.indexOf("ungelesen")).trim() : "0";

		return Integer.valueOf(count);
	}

	private static Boolean extractBusy(String spielerString)
	{
		return !spielerString.contains("Aktion: keine");
	}
}
