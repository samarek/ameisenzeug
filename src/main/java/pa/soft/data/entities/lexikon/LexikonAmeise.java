package pa.soft.data.entities.lexikon;

import pa.soft.data.types.FormicariumType;
import pa.soft.data.types.NestType;

public class LexikonAmeise
{
	private String name;
	private String beschreibung;
	private Boolean verfügbarInShop;
	private Boolean monogyn;
	private Integer maxKoloniegröße;
	private Double punkteProArbeiterin;
	private Boolean haltungMöglich;
	private Boolean schonFindbar;
	private Integer eiEntwicklung;
	private Integer larvenEntwicklung;
	private Integer puppenEntwicklung;
	private Double platzbedarfNest;
	private Double platzbedarfFormicarium;

	private NestType maxNest;
	private FormicariumType maxFormicarium;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getBeschreibung()
	{
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung)
	{
		this.beschreibung = beschreibung;
	}

	public Boolean getVerfügbarInShop()
	{
		return verfügbarInShop;
	}

	public void setVerfügbarInShop(Boolean verfügbarInShop)
	{
		this.verfügbarInShop = verfügbarInShop;
	}

	public Boolean getMonogyn()
	{
		return monogyn;
	}

	public void setMonogyn(Boolean monogyn)
	{
		this.monogyn = monogyn;
	}

	public Integer getMaxKoloniegröße()
	{
		return maxKoloniegröße;
	}

	public void setMaxKoloniegröße(Integer maxKoloniegröße)
	{
		this.maxKoloniegröße = maxKoloniegröße;
	}

	public Double getPunkteProArbeiterin()
	{
		return punkteProArbeiterin;
	}

	public void setPunkteProArbeiterin(Double punkteProArbeiterin)
	{
		this.punkteProArbeiterin = punkteProArbeiterin;
	}

	public Boolean getHaltungMöglich()
	{
		return haltungMöglich;
	}

	public void setHaltungMöglich(Boolean haltungMöglich)
	{
		this.haltungMöglich = haltungMöglich;
	}

	public Boolean getSchonFindbar()
	{
		return schonFindbar;
	}

	public void setSchonFindbar(Boolean schonFindbar)
	{
		this.schonFindbar = schonFindbar;
	}

	public Integer getEiEntwicklung()
	{
		return eiEntwicklung;
	}

	public void setEiEntwicklung(Integer eiEntwicklung)
	{
		this.eiEntwicklung = eiEntwicklung;
	}

	public Integer getLarvenEntwicklung()
	{
		return larvenEntwicklung;
	}

	public void setLarvenEntwicklung(Integer larvenEntwicklung)
	{
		this.larvenEntwicklung = larvenEntwicklung;
	}

	public Integer getPuppenEntwicklung()
	{
		return puppenEntwicklung;
	}

	public void setPuppenEntwicklung(Integer puppenEntwicklung)
	{
		this.puppenEntwicklung = puppenEntwicklung;
	}

	public Double getPlatzbedarfNest()
	{
		return platzbedarfNest;
	}

	public void setPlatzbedarfNest(Double platzbedarfNest)
	{
		this.platzbedarfNest = platzbedarfNest;
	}

	public Double getPlatzbedarfFormicarium()
	{
		return platzbedarfFormicarium;
	}

	public void setPlatzbedarfFormicarium(Double platzbedarfFormicarium)
	{
		this.platzbedarfFormicarium = platzbedarfFormicarium;
	}

	public NestType getMaxNest()
	{
		return maxNest;
	}

	public void setMaxNest(NestType maxNest)
	{
		this.maxNest = maxNest;
	}

	public FormicariumType getMaxFormicarium()
	{
		return maxFormicarium;
	}

	public void setMaxFormicarium(FormicariumType maxFormicarium)
	{
		this.maxFormicarium = maxFormicarium;
	}

	@Override
	public String toString()
	{
		return "LexikonAmeise{" + "name=" + name + ", verfügbarInShop=" + verfügbarInShop + ", monogyn=" +
				monogyn + ", maxKoloniegröße=" + maxKoloniegröße + ", punkteProArbeiterin=" + punkteProArbeiterin +
				", haltungMöglich=" + haltungMöglich + ", schonFindbar=" + schonFindbar + ", eiEntwicklung=" +
				eiEntwicklung + ", larvenEntwicklung=" + larvenEntwicklung + ", puppenEntwicklung=" +
				puppenEntwicklung + ", platzbedarfNest=" + platzbedarfNest + ", platzbedarfFormicarium=" +
				platzbedarfFormicarium + '}';
	}

	public String toAmeisenType()
	{
		String typeName = name.toUpperCase()
							  .replace(" ", "_")
							  .replace(".", "");

		return  typeName + "(\"" + name + "\", " + maxKoloniegröße + ", " + punkteProArbeiterin + ", " + platzbedarfNest
				+ ", " + platzbedarfFormicarium + ", NestType." + maxNest.getTitle() + ", FormicariumType."
				+ maxFormicarium.getTitle() + ")";
	}
}
