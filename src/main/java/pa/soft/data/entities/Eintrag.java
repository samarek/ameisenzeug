package pa.soft.data.entities;

import pa.soft.data.types.LogType;
import pa.soft.processors.Util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Eintrag
{
	private final LocalDateTime localDateTime;

	private final String inhalt;

	private final LogType logType;

	public Eintrag(String eintrag)
	{
		String[] eintragsArray = eintrag.split("->");
		localDateTime = LocalDateTime.parse(eintragsArray[0].trim(), Util.FORMATTER_ISO8601);
		inhalt = eintragsArray[1].trim();
		logType = findLogType(eintragsArray[1].trim());
	}

	private LogType findLogType(String inhalt)
	{
		LogType outputLogType = LogType.DEFAULT;

		if(inhalt.contains("Versorgung"))
		{
			outputLogType = LogType.VERSORGUNG;
		}
		if(inhalt.contains("Anzucht"))
		{
			outputLogType = LogType.ANZUCHT;
		}
		if(inhalt.contains("Futter entfernt"))
		{
			outputLogType = LogType.FUTTER_ENTFERNT;
		}
		if(inhalt.contains("Lager"))
		{
			outputLogType = LogType.LAGER;
		}

		return outputLogType;
	}

	public LogType getLogType()
	{
		return logType;
	}

	@Override
	public String toString()
	{
		return System.lineSeparator() + "Eintrag{" + "localDateTime=" + localDateTime + ", inhalt='" + inhalt + '\'' + '}';
	}
}
