package pa.soft.data.entities.ameisenzimmer.details;

public class LagerItem
{
	private String name;
	private Integer menge;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getMenge()
	{
		return menge;
	}

	public void setMenge(Integer menge)
	{
		this.menge = menge;
	}

	@Override
	public String toString()
	{
		return "LagerItem{" + "name='" + name + '\'' + ", menge=" + menge + '}';
	}
}
