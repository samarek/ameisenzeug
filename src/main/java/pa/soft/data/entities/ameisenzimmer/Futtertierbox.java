package pa.soft.data.entities.ameisenzimmer;

public class Futtertierbox
{
	private Integer id;

	private String species;
	private Integer speciesStorage;
	private Boolean big;
	private Integer count;
	private Integer room;
	private Integer state;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getSpecies()
	{
		return species;
	}

	public void setSpecies(String species)
	{
		this.species = species;
	}

	public Integer getSpeciesStorage()
	{
		return speciesStorage;
	}

	public void setSpeciesStorage(Integer speciesStorage)
	{
		this.speciesStorage = speciesStorage;
	}

	public Boolean getBig()
	{
		return big;
	}

	public void setBig(Boolean big)
	{
		this.big = big;
	}

	public Integer getCount()
	{
		return count;
	}

	public void setCount(Integer count)
	{
		this.count = count;
	}

	public Integer getRoom()
	{
		return room;
	}

	public void setRoom(Integer room)
	{
		this.room = room;
	}

	public Integer getState()
	{
		return state;
	}

	public void setState(Integer state)
	{
		this.state = state;
	}

	@Override
	public String toString()
	{
		return "Futtertierbox{" + "id=" + id + ", species='" + species + '\'' + ", speciesStorage=" + speciesStorage +
				", big=" + big + ", count=" + count + ", room=" + room + ", state=" + state + '}';
	}
}
