package pa.soft.data.entities.ameisenzimmer;

import java.time.LocalDateTime;

public class Ereignis
{
	private LocalDateTime zeitpunkt;
	private String ereignis;

	public LocalDateTime getZeitpunkt()
	{
		return zeitpunkt;
	}

	public void setZeitpunkt(LocalDateTime zeitpunkt)
	{
		this.zeitpunkt = zeitpunkt;
	}

	public String getEreignis()
	{
		return ereignis;
	}

	public void setEreignis(String ereignis)
	{
		this.ereignis = ereignis;
	}

	@Override
	public String toString()
	{
		return "Ereignis{" + "zeitpunkt=" + zeitpunkt + ", ereignis='" + ereignis + '\'' + '}';
	}
}
