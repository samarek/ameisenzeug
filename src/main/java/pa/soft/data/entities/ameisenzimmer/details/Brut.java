package pa.soft.data.entities.ameisenzimmer.details;

public class Brut
{
	private Integer queens;
	private Integer drones;
	private Integer eggs;
	private Integer larvae;
	private Integer cocoons;

	public Integer getQueens()
	{
		return queens;
	}

	public void setQueens(Integer queens)
	{
		this.queens = queens;
	}

	public Integer getDrones()
	{
		return drones;
	}

	public void setDrones(Integer drones)
	{
		this.drones = drones;
	}

	public Integer getEggs()
	{
		return eggs;
	}

	public void setEggs(Integer eggs)
	{
		this.eggs = eggs;
	}

	public Integer getLarvae()
	{
		return larvae;
	}

	public void setLarvae(Integer larvae)
	{
		this.larvae = larvae;
	}

	public Integer getCocoons()
	{
		return cocoons;
	}

	public void setCocoons(Integer cocoons)
	{
		this.cocoons = cocoons;
	}

	@Override
	public String toString()
	{
		return "Brut{" + "queens=" + queens + ", drones=" + drones + ", eggs=" + eggs + ", larvae=" + larvae +
				", cocoons=" + cocoons + '}';
	}
}
