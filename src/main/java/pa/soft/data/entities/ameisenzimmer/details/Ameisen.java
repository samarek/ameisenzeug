package pa.soft.data.entities.ameisenzimmer.details;

import pa.soft.data.types.AmeisenType;
import pa.soft.data.types.NestType;

public class Ameisen
{
	private AmeisenType ameisenType;
	private Integer queens;
	private Integer drones;
	private NestType nestType;

	public String getName()
	{
		return ameisenType.getName();
	}

	public AmeisenType getAmeisenType()
	{
		return ameisenType;
	}

	public void setName(String name)
	{
		this.ameisenType = AmeisenType.fromString(name);
	}

	public Integer getQueens()
	{
		return queens;
	}

	public void setQueens(Integer queens)
	{
		this.queens = queens;
	}

	public Integer getDrones()
	{
		return drones;
	}

	public void setDrones(Integer drones)
	{
		this.drones = drones;
	}

	public NestType getNestType()
	{
		return nestType;
	}

	public void setNestType(NestType nestType)
	{
		this.nestType = nestType;
	}

	@Override
	public String toString()
	{
		return "Ameisen{" + "name='" + getName() + '\'' + ", queens=" + queens + ", drones=" + drones + ", nestType=" +
				nestType + '}';
	}
}
