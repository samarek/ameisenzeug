package pa.soft.data.entities.ameisenzimmer;

import pa.soft.data.entities.ameisenzimmer.details.Brut;
import pa.soft.data.entities.ameisenzimmer.details.FormicariumDetails;
import pa.soft.data.types.AmeisenType;
import pa.soft.data.types.FormicariumType;
import pa.soft.data.types.NestType;

public class Formicarium
{
	private Integer id;

	private AmeisenType ameisenType;
	private Integer punkte;
	private NestType nestType;
	private FormicariumType formicariumType;
	private Double zuckerhunger;
	private Double samenvorrat;
	private Double pilzstatus;
	private Double proteinhunger;

	private Brut brut;

	private FormicariumDetails formicariumDetails;

	public String idString()
	{
		return id + " " + getName();
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return ameisenType.getName();
	}

	public AmeisenType getAmeisenType()
	{
		return ameisenType;
	}

	public void setAmeisenType(String ameisenType)
	{
		this.ameisenType = AmeisenType.fromString(ameisenType);
	}

	public Integer getPunkte()
	{
		return punkte;
	}

	public void setPunkte(Integer punkte)
	{
		this.punkte = punkte;
	}

	public NestType getNestType()
	{
		return nestType;
	}

	public Boolean hasMaxNest()
	{
		return nestType != null && ameisenType.getMaxNest().getPlatz() <= nestType.getPlatz();
	}

	public void setNestType(NestType nestType)
	{
		this.nestType = nestType;
	}

	public FormicariumType getFormicariumType()
	{
		return formicariumType;
	}

	public Boolean hasMaxFormicarium()
	{
		return ameisenType.getMaxFormicarium().getPlatz() <= formicariumType.getPlatz();
	}

	public void setFormicariumType(FormicariumType formicariumType)
	{
		this.formicariumType = formicariumType;
	}

	public Double getZuckerhunger()
	{
		return zuckerhunger;
	}

	public void setZuckerhunger(Double zuckerhunger)
	{
		this.zuckerhunger = zuckerhunger;
	}

	public Double getSamenvorrat()
	{
		return samenvorrat;
	}

	public void setSamenvorrat(Double samenvorrat)
	{
		this.samenvorrat = samenvorrat;
	}

	public Double getPilzstatus()
	{
		return pilzstatus;
	}

	public void setPilzstatus(Double pilzstatus)
	{
		this.pilzstatus = pilzstatus;
	}

	public Double getProteinhunger()
	{
		return proteinhunger;
	}

	public void setProteinhunger(Double proteinhunger)
	{
		this.proteinhunger = proteinhunger;
	}

	public Brut getBrut()
	{
		return brut;
	}

	public void setBrut(Brut brut)
	{
		this.brut = brut;
	}

	public FormicariumDetails getFormicariumDetails()
	{
		return formicariumDetails;
	}

	public void setFormicariumDetails(FormicariumDetails formicariumDetails)
	{
		this.formicariumDetails = formicariumDetails;
	}

	public String toStatusString()
	{
		return formicariumDetails.toStatusString();
	}

	public String toLogString()
	{
		return getFormicariumType().getNameForLog() + getId() + " " + getName() + makeSpaces();
	}

	private String makeSpaces()
	{
		Integer spaceFaktor = 30 - getName().length();
		StringBuilder spaces = new StringBuilder();
		for(int i = 0; i < spaceFaktor; i++)
		{
			spaces.append(' ');
		}

		return spaces.toString();
	}

	@Override
	public String toString()
	{
		return "Formicarium{" + "id=" + id + ", spezies='" + ameisenType + '\'' + ", punkte=" + punkte + ", nestType=" +
				nestType + ", formicariumType=" + formicariumType + ", zuckerhunger=" + zuckerhunger +
				", samenvorrat=" + samenvorrat + ", proteinhunger=" + proteinhunger + ", brut=" + brut +
				", formicariumDetails=" + formicariumDetails + '}';
	}
}
