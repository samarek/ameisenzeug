package pa.soft.data.entities.ameisenzimmer.details;

import pa.soft.data.types.FutterType;

public class FutterItem
{
	private Integer id;

	private String name;
	private Double gramm;
	private Integer zustand;
	private FutterType futterType;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Double getGramm()
	{
		return gramm;
	}

	public void setGramm(Double gramm)
	{
		this.gramm = gramm;
	}

	public Integer getZustand()
	{
		return zustand;
	}

	public void setZustand(Integer zustand)
	{
		this.zustand = zustand;
	}

	public FutterType getFutterType()
	{
		return futterType;
	}

	public void setFutterType(FutterType futterType)
	{
		this.futterType = futterType;
	}

	@Override
	public String toString()
	{
		return "FutterItem{" + "name='" + name + '\'' + ", gramm=" + gramm + ", zustand=" + zustand + ", futterType=" +
				futterType + '}';
	}
}
