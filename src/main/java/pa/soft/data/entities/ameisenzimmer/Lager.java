package pa.soft.data.entities.ameisenzimmer;

import pa.soft.data.entities.ameisenzimmer.details.Ameisen;
import pa.soft.data.entities.ameisenzimmer.details.LagerItem;

import java.util.ArrayList;
import java.util.List;

public class Lager
{
	private List<Ameisen> ameisen;
	private List<LagerItem> futter;
	private List<LagerItem> zubehör;
	private List<LagerItem> formicarienNester;
	private List<LagerItem> möbel;

	public List<Ameisen> getAmeisen()
	{
		return ameisen;
	}

	public void setAmeisen(List<Ameisen> ameisen)
	{
		this.ameisen = ameisen;
	}

	public List<LagerItem> getFutter()
	{
		return futter;
	}

	public void setFutter(List<LagerItem> futter)
	{
		this.futter = futter;
	}

	public List<LagerItem> getZubehör()
	{
		return zubehör;
	}

	public void setZubehör(List<LagerItem> zubehör)
	{
		this.zubehör = zubehör;
	}

	public List<LagerItem> getFormicarienNester()
	{
		return formicarienNester;
	}

	public void setFormicarienNester(List<LagerItem> formicarienNester)
	{
		this.formicarienNester = formicarienNester;
	}

	public List<LagerItem> getMöbel()
	{
		return möbel;
	}

	public void setMöbel(List<LagerItem> möbel)
	{
		this.möbel = möbel;
	}

	public List<LagerItem> getAllItems()
	{
		List<LagerItem> lagerItems = new ArrayList<>();
		lagerItems.addAll(futter);
		lagerItems.addAll(zubehör);
		lagerItems.addAll(formicarienNester);
		lagerItems.addAll(möbel);

		return lagerItems;
	}

	@Override
	public String toString()
	{
		return "Lager{" + "ameisen=" + ameisen + ", futter=" + futter + ", zubehör=" + zubehör +
				", formicarienNester=" + formicarienNester + ", möbel=" + möbel + '}';
	}
}
