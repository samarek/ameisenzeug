package pa.soft.data.entities.ameisenzimmer.details;

import java.util.ArrayList;
import java.util.List;

public class FormicariumDetails
{
	private List<FutterItem> futterItems = new ArrayList<>();

	private List<FutterItem> availableFutterItems = new ArrayList<>();

	private Ausbruchschutz ausbruchschutz;
	private Double formicariumZustand;
	private Double nestZustand;

	public List<FutterItem> getFutterItems()
	{
		return futterItems;
	}

	public void setFutterItems(List<FutterItem> futterItems)
	{
		this.futterItems = futterItems;
	}

	public List<FutterItem> getAvailableFutterItems()
	{
		return availableFutterItems;
	}

	public void setAvailableFutterItems(List<FutterItem> availableFutterItems)
	{
		this.availableFutterItems = availableFutterItems;
	}

	public Ausbruchschutz getAusbruchschutz()
	{
		return ausbruchschutz;
	}

	public void setAusbruchschutz(Ausbruchschutz ausbruchschutz)
	{
		this.ausbruchschutz = ausbruchschutz;
	}

	public Double getFormicariumZustand()
	{
		return formicariumZustand;
	}

	public void setFormicariumZustand(Double formicariumZustand)
	{
		this.formicariumZustand = formicariumZustand;
	}

	public Double getNestZustand()
	{
		return nestZustand;
	}

	public void setNestZustand(Double nestZustand)
	{
		this.nestZustand = nestZustand;
	}

	public String toStatusString()
	{
		StringBuilder out = new StringBuilder("<span style=\"color: red; font-weight: bold;\">");

		if(getFormicariumZustand() >= 75)
		{
			out.append("F ").append(getFormicariumZustand()).append("%&nbsp;");
		}
		if(getNestZustand() >= 75)
		{
			out.append("N ").append(getNestZustand()).append("%&nbsp;");
		}

		return out.toString() + "</span>";
	}

	@Override
	public String toString()
	{
		return "FormicariumDetails{" + "futterItems=" + futterItems + ", ausbruchschutz='" + ausbruchschutz + '\'' +
				", formicariumZustand=" + formicariumZustand + ", nestZustand=" + nestZustand + '}';
	}
}
