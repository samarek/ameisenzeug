package pa.soft.data.entities.ameisenzimmer;

public class Möbel
{
	private String name;
	private Integer plätzeInsgesamt;
	private Double plätzeFrei;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getPlätzeInsgesamt()
	{
		return plätzeInsgesamt;
	}

	public void setPlätzeInsgesamt(Integer plätzeInsgesamt)
	{
		this.plätzeInsgesamt = plätzeInsgesamt;
	}

	public Double getPlätzeFrei()
	{
		return plätzeFrei;
	}

	public void setPlätzeFrei(Double plätzeFrei)
	{
		this.plätzeFrei = plätzeFrei;
	}

	@Override
	public String toString()
	{
		return "Möbel{" + "name='" + name + '\'' + ", plätzeInsgesamt=" + plätzeInsgesamt + ", plätzeFrei=" +
				plätzeFrei + '}';
	}
}
