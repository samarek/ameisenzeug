package pa.soft.data.entities.ameisenzimmer.details;

import pa.soft.data.types.AusbruchschutzType;

public class Ausbruchschutz
{
	private AusbruchschutzType type;
	private Double zustand;

	public AusbruchschutzType getType()
	{
		return type;
	}

	public void setType(AusbruchschutzType type)
	{
		this.type = type;
	}

	public Double getZustand()
	{
		return zustand;
	}

	public void setZustand(Double zustand)
	{
		this.zustand = zustand;
	}

	@Override
	public String toString()
	{
		return "Ausbruchschutz{" + "type=" + type + ", zustand=" + zustand + '}';
	}
}
