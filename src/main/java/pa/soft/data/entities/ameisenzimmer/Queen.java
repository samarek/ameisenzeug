package pa.soft.data.entities.ameisenzimmer;

import pa.soft.data.types.AmeisenType;
import pa.soft.data.types.NestType;

public class Queen
{
	private final String id;
	private final AmeisenType ameisenType;
	private final Integer queens;
	private final Integer workers;
	private final NestType nestType;

	public Queen(String id, AmeisenType ameisenType, Integer queens, Integer workers, NestType nestType)
	{
		this.id = id;
		this.ameisenType = ameisenType;
		this.queens = queens;
		this.workers = workers;
		this.nestType = nestType;
	}

	public AmeisenType getAmeisenType()
	{
		return ameisenType;
	}

	public Integer getQueens()
	{
		return queens;
	}

	public Integer getWorkers()
	{
		return workers;
	}

	public NestType getNestType()
	{
		return nestType;
	}

	public String getIdString()
	{
		return id + " " + ameisenType.getName();
	}
}
