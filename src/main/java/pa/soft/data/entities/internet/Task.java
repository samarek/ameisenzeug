package pa.soft.data.entities.internet;

import java.time.Duration;

public class Task
{
	private Integer id;
	private String titel;
	private String beschreibung;
	private Duration dauer;
	private Integer money;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getTitel()
	{
		return titel;
	}

	public void setTitel(String titel)
	{
		this.titel = titel;
	}

	public String getBeschreibung()
	{
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung)
	{
		this.beschreibung = beschreibung;
	}

	public Duration getDauer()
	{
		return dauer;
	}

	public void setDauer(Duration dauer)
	{
		this.dauer = dauer;
	}

	public Integer getMoney()
	{
		return money;
	}

	public void setMoney(Integer money)
	{
		this.money = money;
	}

	@Override
	public String toString()
	{
		return "Task{" + "id=" + id + ", titel='" + titel + '\'' + ", beschreibung='" + beschreibung + '\'' +
				", dauer=" + dauer + ", money=" + money + '}';
	}
}
