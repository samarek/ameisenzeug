package pa.soft.data.entities.internet.details;

import pa.soft.data.entities.draussen.details.Kolonie;
import pa.soft.data.entities.internet.Kleinanzeige;

public class KleinanzeigeAmeisen extends Kleinanzeige
{
	private String id;

	private Kolonie kolonie;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public Kolonie getKolonie()
	{
		return kolonie;
	}

	public void setKolonie(Kolonie kolonie)
	{
		this.kolonie = kolonie;
	}

	@Override
	public String toString()
	{
		return "KleinanzeigeAmeisen{" + "kolonie=" + kolonie + ", preis=" + preis + ", verkäufer='" + verkäufer + '\'' +
				'}';
	}
}
