package pa.soft.data.entities.internet;

import java.time.Duration;

public class Arbeit
{
	private Integer id;
	private String titel;
	private String beschreibung;
	private Duration dauer;
	private Integer lohn;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getTitel()
	{
		return titel;
	}

	public void setTitel(String titel)
	{
		this.titel = titel;
	}

	public String getBeschreibung()
	{
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung)
	{
		this.beschreibung = beschreibung;
	}

	public Duration getDauer()
	{
		return dauer;
	}

	public void setDauer(Duration dauer)
	{
		this.dauer = dauer;
	}

	public Integer getLohn()
	{
		return lohn;
	}

	public void setLohn(Integer lohn)
	{
		this.lohn = lohn;
	}

	@Override
	public String toString()
	{
		return "Arbeit{" + "id=" + id + ", titel='" + titel + '\'' + ", beschreibung='" + beschreibung + '\'' +
				", dauer=" + dauer + ", lohn=" + lohn + '}';
	}
}
