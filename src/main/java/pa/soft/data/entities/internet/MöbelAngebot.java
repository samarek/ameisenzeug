package pa.soft.data.entities.internet;

public class MöbelAngebot
{
	private Integer id;
	private String name;
	private String beschreibung;
	private Integer preis;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getBeschreibung()
	{
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung)
	{
		this.beschreibung = beschreibung;
	}

	public Integer getPreis()
	{
		return preis;
	}

	public void setPreis(Integer preis)
	{
		this.preis = preis;
	}

	@Override
	public String toString()
	{
		return "MöbelAngebot{" + "id=" + id + ", name='" + name + '\'' + ", beschreibung='" + beschreibung + '\'' +
				", preis=" + preis + '}';
	}
}
