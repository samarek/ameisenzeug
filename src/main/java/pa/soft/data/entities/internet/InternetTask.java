package pa.soft.data.entities.internet;

import pa.soft.data.entities.Task;

import java.time.Duration;

public class InternetTask extends Task
{
	private Duration dauer;

	public Duration getDauer()
	{
		return dauer;
	}

	public void setDauer(Duration dauer)
	{
		this.dauer = dauer;
	}

	@Override
	public String toString()
	{
		return "InternetTask{" + "dauer=" + dauer + ", id=" + id + ", titel='" + titel + '\'' + ", beschreibung='" +
				beschreibung + '\'' + ", money=" + money + '}';
	}
}
