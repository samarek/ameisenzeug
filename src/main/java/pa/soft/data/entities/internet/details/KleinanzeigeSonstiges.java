package pa.soft.data.entities.internet.details;

import pa.soft.data.entities.internet.Kleinanzeige;

public class KleinanzeigeSonstiges extends Kleinanzeige
{
	private Integer anzahl;
	private Integer proStück;

	public Integer getAnzahl()
	{
		return anzahl;
	}

	public void setAnzahl(Integer anzahl)
	{
		this.anzahl = anzahl;
	}

	public Integer getProStück()
	{
		return proStück;
	}

	public void setProStück(Integer proStück)
	{
		this.proStück = proStück;
	}

	@Override
	public String toString()
	{
		return "KleinanzeigeSonstiges{" + "anzahl=" + anzahl + ", proStück=" + proStück + ", name='" + name + '\'' +
				", preis=" + preis + ", verkäufer='" + verkäufer + '\'' + '}';
	}
}
