package pa.soft.data.entities.internet;

public class Immobilie
{
	private Integer id;
	private String name;
	private String beschreibung;
	private Integer regalplätze;
	private Integer preis;

	private Boolean available;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getBeschreibung()
	{
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung)
	{
		this.beschreibung = beschreibung;
	}

	public Integer getRegalplätze()
	{
		return regalplätze;
	}

	public void setRegalplätze(Integer regalplätze)
	{
		this.regalplätze = regalplätze;
	}

	public Integer getPreis()
	{
		return preis;
	}

	public void setPreis(Integer preis)
	{
		this.preis = preis;
	}

	public Boolean isAvailable()
	{
		return available;
	}

	public void setAvailable(Boolean available)
	{
		this.available = available;
	}

	@Override
	public String toString()
	{
		return "Immobilie{" + "id=" + id + ", name='" + name + '\'' + ", beschreibung='" + beschreibung + '\'' +
				", regalplätze=" + regalplätze + ", preis=" + preis + '}';
	}
}
