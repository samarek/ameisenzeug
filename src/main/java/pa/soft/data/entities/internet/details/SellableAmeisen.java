package pa.soft.data.entities.internet.details;

import pa.soft.data.entities.ameisenzimmer.details.Ameisen;

public class SellableAmeisen extends Ameisen
{
	private String id;

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String printDetails()
	{
		return getId() + "\t" + getDrones() + "\t" + getName();
	}

	@Override
	public String toString()
	{
		return "SellableAmeisen{" + "id='" + id + '\'' + "} " + super.toString();
	}
}
