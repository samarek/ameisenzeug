package pa.soft.data.entities.internet;

public class Kleinanzeige
{
	protected String name;
	protected Integer preis;
	protected String verkäufer;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Integer getPreis()
	{
		return preis;
	}

	public void setPreis(Integer preis)
	{
		this.preis = preis;
	}

	public String getVerkäufer()
	{
		return verkäufer;
	}

	public void setVerkäufer(String verkäufer)
	{
		this.verkäufer = verkäufer;
	}

	@Override
	public String toString()
	{
		return "Kleinanzeige{" + "name='" + name + '\'' + ", preis=" + preis + ", verkäufer='" + verkäufer + '\'' + '}';
	}
}
