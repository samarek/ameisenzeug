package pa.soft.data.entities.ameisenshop;

import pa.soft.data.types.AmeisenType;

public class AnkaufPreis
{
	AmeisenType ameisenType;
	Integer preis;

	public AnkaufPreis(String ameisenType, Integer preis)
	{
		this.ameisenType = AmeisenType.fromString(ameisenType);
		this.preis = preis;
	}

	public String getName()
	{
		return ameisenType.getName();
	}

	public void setAmeisenType(String ameisenType)
	{
		this.ameisenType = AmeisenType.fromString(ameisenType);
	}

	public Integer getPreis()
	{
		return preis;
	}

	public void setPreis(Integer preis)
	{
		this.preis = preis;
	}
}
