package pa.soft.data.entities.ameisenshop;

public class ShopItem
{
	private String titel;
	private String beschreibung;
	private Integer preis;
	private Integer lagerbestand;

	public String getTitel()
	{
		return titel;
	}

	public void setTitel(String titel)
	{
		this.titel = titel;
	}

	public String getBeschreibung()
	{
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung)
	{
		this.beschreibung = beschreibung;
	}

	public Integer getPreis()
	{
		return preis;
	}

	public void setPreis(Integer preis)
	{
		this.preis = preis;
	}

	public Integer getLagerbestand()
	{
		return lagerbestand;
	}

	public void setLagerbestand(Integer lagerbestand)
	{
		this.lagerbestand = lagerbestand;
	}

	@Override
	public String toString()
	{
		return "ShopItem{" + "titel='" + titel + '\'' + ", beschreibung='" + beschreibung + '\'' + ", preis=" + preis +
				(lagerbestand != null ? "lagerbestand=" + lagerbestand : "") + '}';
	}
}
