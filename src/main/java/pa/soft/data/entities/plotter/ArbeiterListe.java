package pa.soft.data.entities.plotter;

import pa.soft.data.entities.ameisenzimmer.Formicarium;
import pa.soft.data.managers.ameisenzimmer.FormicarienManager;
import pa.soft.data.types.AmeisenType;
import pa.soft.data.types.FormicariumType;
import pa.soft.data.types.TemplateType;
import pa.soft.processors.AmeisenKurser;
import pa.soft.processors.ameisenkurser.TemplateProcessor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ArbeiterListe implements KursList
{
	private String name;

	private List<Integer> arbeiterinnen = new ArrayList<>();

	private Formicarium formicarium;

	private AmeisenType ameisenType;

	private final TemplateType templateType = TemplateType.ARBEITERINNENLISTE;

	private Random random = new Random();

	private String color =
			String.format("#%02x%02x%02x", random.nextInt(200), random.nextInt(200), random.nextInt(200));

	public ArbeiterListe(String arbeiterliste)
	{
		String[] listElements = arbeiterliste.split("=");
		name = listElements[0].trim();
		ameisenType = AmeisenType.fromString(name.substring(name.indexOf(" ")).trim());
		Integer id = Integer.valueOf(name.substring(0, name.indexOf(" ")).trim());
		formicarium = FormicarienManager.fetchFormicarienById(AmeisenKurser.spieler, id);

		for(String listElement : listElements[1].split(";"))
		{
			arbeiterinnen.add(Integer.valueOf(listElement.trim()));
		}
	}

	public ArbeiterListe(Formicarium formicarium)
	{
		name = formicarium.idString();
		arbeiterinnen.add(formicarium.getBrut().getDrones());
	}

	public void addElement(Integer newPreis)
	{
		Integer overflow = arbeiterinnen.size() - templateType.getMaxLength();
		for(int i = 0; i < overflow; i++)
		{
			arbeiterinnen.remove(0);
		}

		arbeiterinnen.add(newPreis);
	}

	@Override
	public AmeisenType getAmeisenType()
	{
		return ameisenType;
	}

	public String toFileString()
	{
		String output = name + "=";
		for(Integer arbeiterin : arbeiterinnen)
		{
			output += arbeiterin + ";";
		}

		return output + System.lineSeparator();
	}

	public Formicarium getFormicarium()
	{
		return formicarium;
	}

	public String generateButton(Integer id)
	{
		String cssClasses = "class=\"antbutton note\"";
		String cssStyle = "style=\"border-color: " + color + "; background: radial-gradient(ellipse at center, #ffffff 35%," + color + " 100%);\"";
		String jsEvents = "onclick=\"toggleGraph(" + id + ")\" "
				+ "onmouseover=\"toggleGraphBackground(" + id + ", '1')\" "
				+ "onmouseout=\"toggleGraphBackground(" + id + ", '0')\"";
		String buttonCaption = name + "</br>" + TemplateProcessor.makeNumber(arbeiterinnen.get(arbeiterinnen.size() - 1)) + " / "
				+ TemplateProcessor.makeNumber(formicarium.getAmeisenType().getMaxPopulation()) + "</br>"
				+ formicarium.getAmeisenType().getPunkte() + "\t\t"
				+ TemplateProcessor.makeNumber(formicarium.getPunkte());

		String out = "\t\t<div id=\"button" + id + "\" "
				+ cssClasses + " " + cssStyle + " " + jsEvents
				+ ">" +  buttonCaption
				+ generateIcons()
				+ generateToolTip()
				+ "</div>";

		return out;
	}

	private String generateIcons()
	{
		String outputValue = "";

		if(formicarium.getFormicariumType().equals(FormicariumType.MINI_ARENA))
		{
			outputValue += "<span style=\"float: right; font-family: wingdings; color: black;\">&#9645;</span>";
		}

		if(formicarium.hasMaxNest() && formicarium.hasMaxFormicarium())
		{
			outputValue += "<span style=\"float: right; font-family: wingdings; color: green;\">&#10004;</span>";
		}

		if(formicarium.getFormicariumDetails().getFormicariumZustand() >= 50 ||
				formicarium.getFormicariumDetails().getNestZustand() >= 50)
		{
			String size = "";
			if (formicarium.getFormicariumDetails().getFormicariumZustand() >= 75 ||
					formicarium.getFormicariumDetails().getNestZustand() >= 75)
			{
				size = "font-size: 1.5em; font-weight: bold; color: red;";
			}
			outputValue += "<span style=\"float: right; margin-right: 5px; " + size +"\">!</span>";
		}

		return outputValue;
	}

	private String generateToolTip()
	{
		String tickNest = "<td style=\"font-family: wingdings;" + (formicarium.hasMaxNest() ? " color: green;\">&#10004;" : " color: red;\">&#10008;") + "</td>";
		String tickFormicarium = "<td style=\"font-family: wingdings;" + (formicarium.hasMaxFormicarium() ? " color: green;\">&#10004;" : " color: red;\">&#10008;") + "</td>";

		String note = "<table>";
		note += "<tr><td colspan=\"2\">" + formicarium.getAmeisenType().getName() + "</td></tr>";
		if(formicarium.getFormicariumType().equals(FormicariumType.MINI_ARENA))
		{
			note += generateAnzuchtContent();
		}
		else
		{
			note += generateContent(tickNest, tickFormicarium);
		}
		note += "<tr><td colspan=\"2\">" + formicarium.toStatusString() + "</td></tr>";
		note += "</table>";

		return "<span class=\"notetext\">" + note + "</span>";
	}

	private String generateAnzuchtContent()
	{
		String ticks = "<tr><td>" + "Formicarium" + "</td><td>" + formicarium.getFormicariumDetails().getFormicariumZustand() + "</td></tr>";
		ticks += "<tr><td>" + "Nest" + "</td><td>" + formicarium.getFormicariumDetails().getNestZustand() + "</td></tr>";

		return ticks;
	}

	private String generateContent(String tickNest, String tickFormicarium)
	{
		String ticks = "<tr><td>" + formicarium.getAmeisenType().getMaxNest().getName() + "</td>" + tickNest + "</tr>";
		ticks += "<tr><td>" + formicarium.getAmeisenType().getMaxFormicarium().getNameForLog() + "</td>" + tickFormicarium + "</tr>";

		return ticks;
	}

	@Override
	public String getColor()
	{
		return color;
	}

	@Override
	public List<Integer> getListe()
	{
		return arbeiterinnen;
	}

	@Override
	public Integer getMaxFromList()
	{
		return Collections.max(arbeiterinnen);
	}
}
