package pa.soft.data.entities.plotter;

import pa.soft.data.types.AmeisenType;

import java.util.List;

public interface KursList
{
	void addElement(Integer newValue);
	AmeisenType getAmeisenType();
	String toFileString();
	String generateButton(Integer id);
	String getColor();
	List<Integer> getListe();
	Integer getMaxFromList();
}
