package pa.soft.data.entities.plotter;

import pa.soft.data.entities.ameisenshop.AnkaufPreis;
import pa.soft.data.types.AmeisenType;
import pa.soft.data.types.TemplateType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class PreisListe implements KursList
{
	private AmeisenType ameisenType;
	private List<Integer> preise = new ArrayList<>();

	private final TemplateType templateType = TemplateType.PREISLISTE;

	Random random = new Random();
	private String color = String.format("#%02x%02x%02x", random.nextInt(200), random.nextInt(200), random.nextInt(200));


	public PreisListe(String preisliste)
	{
		String[] listElements = preisliste.split("=");
		ameisenType = AmeisenType.fromString(listElements[0].trim());

		for(String listElement : listElements[1].split(";"))
		{
			preise.add(Integer.valueOf(listElement.trim()));
		}
	}

	public PreisListe(AnkaufPreis ankaufPreis)
	{
		ameisenType = AmeisenType.fromString(ankaufPreis.getName());
		preise.add(ankaufPreis.getPreis());
	}

	public void addElement(Integer newPreis)
	{
		Integer overflow = preise.size() - templateType.getMaxLength();
		for (int i = 0; i < overflow; i++)
		{
			preise.remove(0);
		}

		preise.add(newPreis);
	}

	@Override
	public AmeisenType getAmeisenType()
	{
		return ameisenType;
	}

	public String toFileString()
	{
		String output = ameisenType.getName() + "=";
		for(Integer preis : preise)
		{
			output += preis + ";";
		}

		return output + System.lineSeparator();
	}

	public String generateButton(Integer id)
	{
		String cssClasses = "class=\"antbutton note\"";
		String cssStyle = "style=\"border-color: " + color + "; background: radial-gradient(ellipse at center, #ffffff 35%," + color + " 100%);\"";
		String jsEvents = "onclick=\"toggleGraph(" + id + ")\" "
						+ "onmouseover=\"toggleGraphBackground(" + id + ", '1')\" "
						+ "onmouseout=\"toggleGraphBackground(" + id + ", '0')\"";
		String buttonCaption = ameisenType.getName() + " - " + makeMoneyString(preise.get(preise.size() - 1));

		String out = "\t\t<div id=\"button" + id + "\" "
				+ cssClasses + " " + cssStyle + " " + jsEvents
				+ ">" +  buttonCaption
				+ generateToolTip()
				+ "</div>";

		return out;
	}

	private String generateToolTip()
	{
		Integer maxValue = Collections.max(preise);
		Integer minValue = Collections.min(preise);

		String note = "<table>";
		note += "<tr><td colspan=\"2\">" + ameisenType.getName() + "</td></tr>";
		note += "<tr><td>Tiefstwert</td><td>" + makeMoneyString(minValue) + "</td></tr>";
		note += "<tr><td>Höchstwert</td><td>" + makeMoneyString(maxValue) + "</td></tr>";
		note += "</table>";

		return "<span class=\"notetext\">" + note + "</span>";
	}

	private String makeMoneyString(Integer preis)
	{
		return preis / 100 + "," + (preis % 100 < 10 ? "0" + preis % 100: preis % 100) + " &euro;";
	}

	public String getColor()
	{
		return color;
	}

	public List<Integer> getListe()
	{
		return preise;
	}

	@Override
	public Integer getMaxFromList()
	{
		return Collections.max(preise);
	}
}
