package pa.soft.data.entities.draussen.details;

import pa.soft.data.types.NestType;

public class Kolonie
{
	private String spezies;
	private Integer queens;
	private Integer drones;
	private NestType nestType;

	public String getSpezies()
	{
		return spezies;
	}

	public void setSpezies(String spezies)
	{
		this.spezies = spezies;
	}

	public Integer getQueens()
	{
		return queens;
	}

	public void setQueens(Integer queens)
	{
		this.queens = queens;
	}

	public Integer getDrones()
	{
		return drones;
	}

	public void setDrones(Integer drones)
	{
		this.drones = drones;
	}

	public NestType getNestType()
	{
		return nestType;
	}

	public void setNestType(NestType nestType)
	{
		this.nestType = nestType;
	}

	@Override
	public String toString()
	{
		return "Kolonie{" + "spezies='" + spezies + '\'' + ", queens=" + queens + ", drones=" + drones + ", nestType=" +
				nestType + '}';
	}
}
