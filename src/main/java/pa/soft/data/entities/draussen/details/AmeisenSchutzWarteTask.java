package pa.soft.data.entities.draussen.details;

import pa.soft.data.entities.Task;

import java.time.Duration;

public class AmeisenSchutzWarteTask extends Task
{
	private Duration dauer;

	public Duration getDauer()
	{
		return dauer;
	}

	public void setDauer(Duration dauer)
	{
		this.dauer = dauer;
	}

	@Override
	public String toString()
	{
		return "Task{" + "id=" + id + ", titel='" + titel + '\'' + ", beschreibung='" + beschreibung + '\'' +
				", dauer=" + dauer + ", money=" + money + '}';
	}
}
