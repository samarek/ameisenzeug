package pa.soft.data.entities.draussen;

import pa.soft.processors.Util;

import java.time.LocalDateTime;

public class Ergebnis
{
	private LocalDateTime zeitpunkt;
	private String ergebnis;

	public LocalDateTime getZeitpunkt()
	{
		return zeitpunkt;
	}

	public void setZeitpunkt(LocalDateTime zeitpunkt)
	{
		this.zeitpunkt = zeitpunkt;
	}

	public String getErgebnis()
	{
		return ergebnis;
	}

	public void setErgebnis(String ergebnis)
	{
		this.ergebnis = ergebnis;
	}

	@Override
	public String toString()
	{
		return "Ergebnis{" + "zeitpunkt=" + zeitpunkt + ", ergebnis='" + ergebnis + '\'' + '}';
	}

	public String getZeitpunktString()
	{
		return zeitpunkt.format(Util.FORMATTER_ISO8601);
	}
}
