package pa.soft.data.entities.draussen;

import pa.soft.data.entities.Task;
import pa.soft.data.entities.draussen.details.EhemaligeKolonie;
import pa.soft.data.entities.draussen.details.Kolonie;

import java.util.List;
import java.util.Map;

public class AmeisenSchutzWarte
{
	private Map<Integer, Kolonie> auswilderKolonien;
	private List<EhemaligeKolonie> ehemaligeKolonien;
	private List<Task> aktionen;

	public Map<Integer, Kolonie> getAuswilderKolonien()
	{
		return auswilderKolonien;
	}

	public void setAuswilderKolonien(Map<Integer, Kolonie> auswilderKolonien)
	{
		this.auswilderKolonien = auswilderKolonien;
	}

	public List<EhemaligeKolonie> getEhemaligeKolonien()
	{
		return ehemaligeKolonien;
	}

	public void setEhemaligeKolonien(List<EhemaligeKolonie> ehemaligeKolonien)
	{
		this.ehemaligeKolonien = ehemaligeKolonien;
	}

	public List<Task> getAktionen()
	{
		return aktionen;
	}

	public void setAktionen(List<Task> aktionen)
	{
		this.aktionen = aktionen;
	}

	@Override
	public String toString()
	{
		return "AmeisenSchutzWarte{" + "auswilderKolonien=" + auswilderKolonien + ", ehemaligeKolonien=" +
				ehemaligeKolonien + ", aktionen=" + aktionen + '}';
	}
}
