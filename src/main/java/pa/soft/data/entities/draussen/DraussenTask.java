package pa.soft.data.entities.draussen;

import pa.soft.data.entities.Task;
import pa.soft.http.Links;

import java.time.Duration;
import java.util.List;

public class DraussenTask extends Task
{
	private List<Duration> dauer;

	public List<Duration> getDauer()
	{
		return dauer;
	}

	public void setDauer(List<Duration> dauer)
	{
		this.dauer = dauer;
	}

	@Override
	public String toString()
	{
		return "Task{" + "id=" + id + ", titel='" + titel + '\'' + ", beschreibung='" + beschreibung + '\'' +
				", dauer=" + dauer + ", money=" + money + '}';
	}

	public String[] toStringArray()
	{
		return new String[]{id + "", titel, Links.DRAUSSEN};
	}
}
