package pa.soft.data.entities.draussen.details;

import java.time.LocalDateTime;

public class EhemaligeKolonie
{
	private LocalDateTime zeitpunkt;
	private Kolonie kolonie;
	private Integer punkte;
	private Integer aswPunkte;

	public LocalDateTime getZeitpunkt()
	{
		return zeitpunkt;
	}

	public void setZeitpunkt(LocalDateTime zeitpunkt)
	{
		this.zeitpunkt = zeitpunkt;
	}

	public Kolonie getKolonie()
	{
		return kolonie;
	}

	public void setKolonie(Kolonie kolonie)
	{
		this.kolonie = kolonie;
	}

	public Integer getPunkte()
	{
		return punkte;
	}

	public void setPunkte(Integer punkte)
	{
		this.punkte = punkte;
	}

	public Integer getAswPunkte()
	{
		return aswPunkte;
	}

	public void setAswPunkte(Integer aswPunkte)
	{
		this.aswPunkte = aswPunkte;
	}

	@Override
	public String toString()
	{
		return "EhemaligeKolonie{" + "zeitpunkt=" + zeitpunkt + ", kolonie=" + kolonie + ", punkte=" + punkte +
				", aswPunkte=" + aswPunkte + '}';
	}
}
