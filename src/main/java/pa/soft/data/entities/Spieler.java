package pa.soft.data.entities;

import pa.soft.data.entities.ameisenshop.AnkaufPreis;
import pa.soft.data.entities.ameisenshop.ShopItem;
import pa.soft.data.entities.ameisenzimmer.*;
import pa.soft.data.entities.draussen.AmeisenSchutzWarte;
import pa.soft.data.entities.draussen.Ergebnis;
import pa.soft.data.entities.internet.Immobilie;
import pa.soft.data.entities.internet.Kleinanzeige;
import pa.soft.data.entities.internet.MöbelAngebot;
import pa.soft.data.entities.internet.details.KleinanzeigeAmeisen;
import pa.soft.http.Links;

import java.util.*;

public class Spieler
{
	private String name;
	private String passwort;
	private Map<String, String> cookies = new HashMap<>();

	private Integer geld;
	private Integer punkte;
	private Integer messageCount;
	private List<Message> messages = new ArrayList<>();

	// Ameisenzimmer
	private List<Formicarium> formicarien;
	private Lager lager;
	private List<Ereignis> ereignisse;
	private List<Möbel> möbel;
	private List<Task> aktionen;
	private List<Futtertierbox> futtertierboxen;

	// Internet
	private List<Task> forschungen;
	private List<KleinanzeigeAmeisen> kleinanzeigenAmeisen;
	private List<Kleinanzeige> kleinanzeigenSonstiges;
	private List<Task> arbeiten;
	private List<Immobilie> immobilien;
	private List<MöbelAngebot> möbelAngebote;

	// Ameisenshop
	private List<ShopItem> einheimischeKöniginnen;
	private List<ShopItem> südeuropäischeKöniginnen;
	private List<ShopItem> exotischeKöniginnen;
	private List<ShopItem> futter;
	private List<ShopItem> zubehör;
	private List<ShopItem> formicarienAngebote;
	private List<ShopItem> nester;
	private List<AnkaufPreis> ankaufPreise;

	// Draussen
	private List<Task> ameisenSuchen;
	private List<Task> futterSuchen;
	private List<Ergebnis> ameisenSuchenErgebnisse;
	private List<Ergebnis> futterSuchenErgebnisse;
	private AmeisenSchutzWarte ameisenSchutzWarte;

	public static final String[] KLOPUTZER = {"301", "Kloputzer", Links.INTERNET};
	public static final String[] REGALE = {"302", "Regale", Links.INTERNET};
	public static final String[] PIZZA = {"303", "Pizzabote", Links.INTERNET};
	public static final String[] BETREUER = {"304", "Betreuer", Links.INTERNET};
	public static final String[] MYRMEKOLOGE = {"305", "Myrmekologe", Links.INTERNET};

	private String[] job = KLOPUTZER;

	private String forschung;
	private Boolean antGatheringWanted = false;

	private boolean busy;

	public Spieler(String name, String passwort)
	{
		this.name = name;
		this.passwort = passwort;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getPasswort()
	{
		return passwort;
	}

	public void setPasswort(String passwort)
	{
		this.passwort = passwort;
	}

	public Map<String, String> getCookies()
	{
		return cookies;
	}

	public void setCookies(Map<String, String> cookies)
	{
		this.cookies = cookies;
	}

	public Integer getGeld()
	{
		return geld;
	}

	public String getGeldString()
	{
		String geldString = geld + "";
		String out = geldString.substring(0, geldString.length() - 2);
		out += "," + geldString.substring(geldString.length() - 2);
		out += " €";

		return out;
	}

	public void setGeld(Integer geld)
	{
		this.geld = geld;
	}

	public Integer getPunkte()
	{
		return punkte;
	}

	public void setPunkte(Integer punkte)
	{
		this.punkte = punkte;
	}

	public Integer getMessageCount()
	{
		return messageCount;
	}

	public void setMessageCount(Integer messageCount)
	{
		this.messageCount = messageCount;
	}

	public void setMessages(List<Message> messages)
	{
		this.messages = messages;
	}

	public List<Message> getMessages()
	{
		return messages;
	}

	public List<Formicarium> getFormicarien()
	{
		return formicarien;
	}

	public void setFormicarien(List<Formicarium> formicarien)
	{
		this.formicarien = formicarien;
	}

	public Lager getLager()
	{
		return lager;
	}

	public void setLager(Lager lager)
	{
		this.lager = lager;
	}

	public List<Ereignis> getEreignisse()
	{
		return ereignisse;
	}

	public void setEreignisse(List<Ereignis> ereignisse)
	{
		this.ereignisse = ereignisse;
	}

	public List<Möbel> getMöbel()
	{
		return möbel;
	}

	public void setMöbel(List<Möbel> möbel)
	{
		this.möbel = möbel;
	}

	public List<Task> getAktionen()
	{
		return aktionen;
	}

	public void setAktionen(List<Task> aktionen)
	{
		this.aktionen = aktionen;
	}

	public List<Futtertierbox> getFuttertierboxen()
	{
		return futtertierboxen;
	}

	public void setFuttertierboxen(List<Futtertierbox> futtertierboxen)
	{
		this.futtertierboxen = futtertierboxen;
	}

	public List<Task> getForschungen()
	{
		return forschungen;
	}

	public void setForschungen(List<Task> forschungen)
	{
		this.forschungen = forschungen;
	}

	public List<KleinanzeigeAmeisen> getKleinanzeigenAmeisen()
	{
		return kleinanzeigenAmeisen;
	}

	public void setKleinanzeigenAmeisen(List<KleinanzeigeAmeisen> kleinanzeigenAmeisen)
	{
		this.kleinanzeigenAmeisen = kleinanzeigenAmeisen;
	}

	public List<Kleinanzeige> getKleinanzeigenSonstiges()
	{
		return kleinanzeigenSonstiges;
	}

	public void setKleinanzeigenSonstiges(List<Kleinanzeige> kleinanzeigenSonstiges)
	{
		this.kleinanzeigenSonstiges = kleinanzeigenSonstiges;
	}

	public List<Task> getArbeiten()
	{
		return arbeiten;
	}

	public void setArbeiten(List<Task> arbeiten)
	{
		this.arbeiten = arbeiten;
	}

	public List<Immobilie> getImmobilien()
	{
		return immobilien;
	}

	public void setImmobilien(List<Immobilie> immobilien)
	{
		this.immobilien = immobilien;
	}

	public List<MöbelAngebot> getMöbelAngebote()
	{
		return möbelAngebote;
	}

	public void setMöbelAngebote(List<MöbelAngebot> möbelAngebote)
	{
		this.möbelAngebote = möbelAngebote;
	}

	public List<ShopItem> getEinheimischeKöniginnen()
	{
		return einheimischeKöniginnen;
	}

	public void setEinheimischeKöniginnen(List<ShopItem> einheimischeKöniginnen)
	{
		this.einheimischeKöniginnen = einheimischeKöniginnen;
	}

	public List<ShopItem> getSüdeuropäischeKöniginnen()
	{
		return südeuropäischeKöniginnen;
	}

	public void setSüdeuropäischeKöniginnen(List<ShopItem> südeuropäischeKöniginnen)
	{
		this.südeuropäischeKöniginnen = südeuropäischeKöniginnen;
	}

	public List<ShopItem> getExotischeKöniginnen()
	{
		return exotischeKöniginnen;
	}

	public void setExotischeKöniginnen(List<ShopItem> exotischeKöniginnen)
	{
		this.exotischeKöniginnen = exotischeKöniginnen;
	}

	public List<ShopItem> getFutter()
	{
		return futter;
	}

	public void setFutter(List<ShopItem> futter)
	{
		this.futter = futter;
	}

	public List<ShopItem> getZubehör()
	{
		return zubehör;
	}

	public void setZubehör(List<ShopItem> zubehör)
	{
		this.zubehör = zubehör;
	}

	public List<ShopItem> getFormicarienAngebote()
	{
		return formicarienAngebote;
	}

	public void setFormicarienAngebote(List<ShopItem> formicarienAngebote)
	{
		this.formicarienAngebote = formicarienAngebote;
	}

	public List<ShopItem> getNester()
	{
		return nester;
	}

	public void setNester(List<ShopItem> nester)
	{
		this.nester = nester;
	}

	public List<Task> getAmeisenSuchen()
	{
		return ameisenSuchen;
	}

	public void setAmeisenSuchen(List<Task> ameisenSuchen)
	{
		this.ameisenSuchen = ameisenSuchen;
	}

	public List<Task> getFutterSuchen()
	{
		return futterSuchen;
	}

	public void setFutterSuchen(List<Task> futterSuchen)
	{
		this.futterSuchen = futterSuchen;
	}

	public List<Ergebnis> getAmeisenSuchenErgebnisse()
	{
		return ameisenSuchenErgebnisse;
	}

	public void setAmeisenSuchenErgebnisse(List<Ergebnis> ameisenSuchenErgebnisse)
	{
		this.ameisenSuchenErgebnisse = ameisenSuchenErgebnisse;
	}

	public List<Ergebnis> getFutterSuchenErgebnisse()
	{
		return futterSuchenErgebnisse;
	}

	public void setFutterSuchenErgebnisse(List<Ergebnis> futterSuchenErgebnisse)
	{
		this.futterSuchenErgebnisse = futterSuchenErgebnisse;
	}

	public AmeisenSchutzWarte getAmeisenSchutzWarte()
	{
		return ameisenSchutzWarte;
	}

	public void setAmeisenSchutzWarte(AmeisenSchutzWarte ameisenSchutzWarte)
	{
		this.ameisenSchutzWarte = ameisenSchutzWarte;
	}

	public List<AnkaufPreis> getAnkaufPreise()
	{
		return ankaufPreise;
	}

	public void setAnkaufPreise(List<AnkaufPreis> ankaufPreise)
	{
		this.ankaufPreise = ankaufPreise;
	}

	@Override
	public String toString()
	{
		return "Spieler{" + "name='" + name + '\'' + ", passwort='" + passwort + '\'' + ", cookies=" + cookies +
				", geld=" + geld + ", punkte=" + punkte + ", messageCount=" + messageCount + ", messages=" + messages +
				", formicarien=" + formicarien + ", lager=" + lager + ", ereignisse=" + ereignisse + ", möbel=" +
				möbel + ", aktionen=" + aktionen + ", futtertierboxen=" + futtertierboxen + ", forschungen=" +
				forschungen + ", kleinanzeigenAmeisen=" + kleinanzeigenAmeisen + ", kleinanzeigenSonstiges=" +
				kleinanzeigenSonstiges + ", arbeiten=" + arbeiten + ", immobilien=" + immobilien + ", möbelAngebote=" +
				möbelAngebote + ", einheimischeKöniginnen=" + einheimischeKöniginnen + ", südeuropäischeKöniginnen=" +
				südeuropäischeKöniginnen + ", exotischeKöniginnen=" + exotischeKöniginnen + ", futter=" + futter +
				", zubehör=" + zubehör + ", formicarienAngebote=" + formicarienAngebote + ", nester=" + nester +
				", ankaufPreise=" + ankaufPreise + ", ameisenSuchen=" + ameisenSuchen + ", futterSuchen=" +
				futterSuchen + ", ameisenSuchenErgebnisse=" + ameisenSuchenErgebnisse + ", futterSuchenErgebnisse=" +
				futterSuchenErgebnisse + ", ameisenSchutzWarte=" + ameisenSchutzWarte + '}';
	}

	public boolean isBusy()
	{
		return busy;
	}

	public void setBusy(Boolean busy)
	{
		this.busy = busy;
	}

	public String[] getJob()
	{
		return job;
	}

	public void setJob(String[] job)
	{
		this.job = job;
	}

	public String getForschung()
	{
		return forschung;
	}

	public boolean isAntGatheringWanted()
	{
		return antGatheringWanted;
	}
}
