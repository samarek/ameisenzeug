package pa.soft.data.entities;

import java.time.LocalDateTime;

public class Message
{
	private Integer id;

	private String sender;
	private String subject;
	private String content = "";
	private LocalDateTime localDateTime;

	public Message(Integer id, String sender, String subject, String content, LocalDateTime localDateTime)
	{

		this.id = id;
		this.sender = sender;
		this.subject = subject;
		this.content = content;
		this.localDateTime = localDateTime;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getSender()
	{
		return sender;
	}

	public void setSender(String sender)
	{
		this.sender = sender;
	}

	public String getSubject()
	{
		return subject;
	}

	public void setSubject(String subject)
	{
		this.subject = subject;
	}

	public String getContent()
	{
		return content;
	}

	public void setContent(String content)
	{
		this.content = content;
	}

	public LocalDateTime getLocalDateTime()
	{
		return localDateTime;
	}

	public void setLocalDateTime(LocalDateTime localDateTime)
	{
		this.localDateTime = localDateTime;
	}

	@Override
	public String toString()
	{
		return "Message{" + "id=" + id + ", sender='" + sender + '\'' + ", subject='" + subject + '\'' + ", content='" +
				content + '\'' + ", localDateTime=" + localDateTime + '}';
	}
}
