package pa.soft;

import pa.soft.processors.AmeisenKurser;
import pa.soft.processors.EatenByAnts;
import pa.soft.processors.Employer;
import pa.soft.processors.Farmer;
import pa.soft.processors.Feeder;
import pa.soft.processors.FormicarienNestFinder;
import pa.soft.processors.Futtertierboxer;
import pa.soft.processors.GameLogger;
import pa.soft.processors.LagerPrinter;
import pa.soft.processors.LexikonPrinter;
import pa.soft.processors.LogDisplayer;
import pa.soft.processors.Restocker;
import pa.soft.processors.Seller;
import pa.soft.processors.Util;

public class Starter
{
	private static final String AMEISENKURSER_STRING = "ameisenkurser";
	private static final AmeisenKurser AMEISEN_KURSER = new AmeisenKurser();

	private static final String LAGERPRINTER_STRING = "lagerprinter";
	private static final LagerPrinter LAGER_PRINTER = new LagerPrinter();

	private static final String GAMELOGGER_STRING = "gamelogger";
	private static final GameLogger GAME_LOGGER = new GameLogger();

	private static final String EMPLOYER_STRING = "employer";
	private static final Employer EMPLOYER = new Employer();

	private static final String FARMER_STRING = "farmer";
	private static final Farmer FARMER = new Farmer();


	private static final String FORMICARIUMNESTER_STRING = "formicariumnester";
	private static final String FEEDER_STRING = "feeder";
	private static final String LEXIKONPRINTER_STRING = "lexikonprinter";
	private static final String FUTTERTIERBOXER_STRING = "futtertierboxer";
	private static final String STOCKPROCESSOR_STRING = "stockprocessor";
	private static final String LOG_DISPLAYER_STRING = "logdisplayer";
	private static final String REGULAR_STRING = "regular";
	private static final String PRINT_SELLABLES_STRING = "printsellables";
	private static final String SELL_STUFF_STRING = "sellstuff";
	private static final String TRADE_LASIUS_NIGER_STRING = "tradelasiusniger";

	public static void run(String runmode)
	{
		String playername = Main.ARGS.containsKey("playername") ? " - Player " + Main.ARGS.get("playername") : "";
		Util.log("===== Running " + runmode + playername + " =====");
		switch(runmode.toLowerCase())
		{
			case AMEISENKURSER_STRING:
				AMEISEN_KURSER.run();
				break;
			case LAGERPRINTER_STRING:
				LAGER_PRINTER.run();
				break;
			case EMPLOYER_STRING:
				EMPLOYER.run();
				break;
			case GAMELOGGER_STRING:
				GAME_LOGGER.run();
				break;
			case FARMER_STRING:
				FARMER.run();
				break;

			case LEXIKONPRINTER_STRING:
				LexikonPrinter.run();
				break;
			case FEEDER_STRING:
				Feeder.run();
				break;
			case FUTTERTIERBOXER_STRING:
				Futtertierboxer.run();
				break;
			case STOCKPROCESSOR_STRING:
				Restocker.run();
				break;
			case FORMICARIUMNESTER_STRING:
				FormicarienNestFinder.printFormicarienNestForAmeisenType();
				break;
			case LOG_DISPLAYER_STRING:
				LogDisplayer.run();
				break;
			case PRINT_SELLABLES_STRING:
				Seller.printSellables();
				break;
			case SELL_STUFF_STRING:
				Seller.sellSonstiges();
				break;
			case TRADE_LASIUS_NIGER_STRING:
				Seller.tradeLasiusNiger();
				break;
			case REGULAR_STRING:
				EatenByAnts.run();
				break;
			default:
				Util.log("===== Runmode " + runmode + " unknown =====");
				break;
		}
	}
}
