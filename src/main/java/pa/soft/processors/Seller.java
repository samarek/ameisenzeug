package pa.soft.processors;

import org.jsoup.nodes.Document;
import pa.soft.Main;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.internet.details.KleinanzeigeAmeisen;
import pa.soft.data.entities.internet.details.SellableAmeisen;
import pa.soft.data.managers.SpielerManager;
import pa.soft.data.managers.internet.KleinanzeigenManager;
import pa.soft.data.types.AmeisenType;
import pa.soft.data.types.NestType;
import pa.soft.http.Dispatcher;
import pa.soft.http.Links;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

public class Seller
{
	private static String spielername = "cumeartand";
	private static Spieler spieler;
	private static Predicate<KleinanzeigeAmeisen> kleinanzeigenFilter
			= kleinanzeigeAmeisen -> kleinanzeigeAmeisen.getVerkäufer().equalsIgnoreCase("cumeartand");

	public static void sellSonstiges()
	{
		loginPlayer();
		actuallySellStuff();
	}

	private static void actuallySellStuff()
	{
		Map<String, String> httpData = new HashMap<>();
		List<String> allowedFields = Arrays.asList("action", "itemid", "amount", "antid", "price_cent");

		for(String allowedField : allowedFields)
		{
			if(Main.ARGS.containsKey(allowedField))
			{
				httpData.put(allowedField, Main.ARGS.get(allowedField));
			}
		}
		httpData.put("cat", "2");

		Integer sellCount = fetchSellCount();

		for (int i = 0; i < sellCount; i++)
		{
			sellStuff(httpData);
		}
	}

	private static Integer fetchSellCount()
	{
		return Integer.valueOf(Main.ARGS.getOrDefault("sellcount", "1"));
	}

	public static void printSellables()
	{
		loginPlayer();

		StringBuilder sellablesString = new StringBuilder(System.lineSeparator());
		KleinanzeigenManager.fetchSellableSonstiges(spieler).stream()
							.forEach(sellableSonstiges ->
											 sellablesString.append(sellableSonstiges.printDetails() + System.lineSeparator()));
//		KleinanzeigenManager.fetchSellableAmeisen(spieler).stream()
//							.forEach(sellableAmeisen ->
//											 sellablesString.append(sellableAmeisen.printDetails() + System.lineSeparator()));
		Util.log(sellablesString.toString());
	}

	private static void loginPlayer()
	{
		login(Main.ARGS.getOrDefault("playername", spielername));
	}

	public static void tradeLasiusNiger()
	{
		loginPlayer();
		final Optional<SellableAmeisen> sellableAntOptional = KleinanzeigenManager.fetchSellableAmeisen(spieler)
																				  .stream()
																				  .filter(sellableAmeisen -> sellableAmeisen.getAmeisenType().equals(AmeisenType.LASIUS_NIGER))
																				  .filter(sellableAmeisen -> sellableAmeisen.getNestType().equals(NestType.TEST_TUBE))
																				  .filter(sellableAmeisen -> sellableAmeisen.getDrones() > 0)
																				  .findAny();
		sellableAntOptional.ifPresent(sellableAmeisen -> {
			sellAnt(sellableAmeisen);
			buyAnt();
		});
	}

	private static void sellAnt(final SellableAmeisen sellableAmeisen)
	{
		Map<String, String> httpData = new HashMap<>();
		httpData.put("action", "sellant");
		httpData.put("cat", "2");
		httpData.put("antid", sellableAmeisen.getId());
		httpData.put("price_cent", Main.ARGS.getOrDefault("price_cent", "1500"));

		sellStuff(httpData);
	}

	private static void buyAnt()
	{
		loginBuyer();
		final List<KleinanzeigeAmeisen> kleinanzeigenAmeisen = KleinanzeigenManager.fetchKleinanzeigenAmeisen(spieler);
		KleinanzeigenProcessor.setKleinanzeigenFilter(kleinanzeigenFilter);
		KleinanzeigenProcessor.setKleinanzeigenAmeisen(kleinanzeigenAmeisen);
		KleinanzeigenProcessor.process(spieler);
	}

	private static void sellStuff(final Map<String, String> httpData)
	{
		Document response = Dispatcher.post(spieler, Links.INTERNET, httpData);
		Util.log(response.select("#message").text());
	}

	private static void loginBuyer()
	{
		if(Main.ARGS.containsKey("buyername"))
		{
			login(Main.ARGS.get("buyername"));
		}
		else
		{
			Util.logException(new IllegalArgumentException("No buyername provided"));
		}
	}

	private static void login(String name)
	{
		spieler = SpielerManager.fetchSpieler(name);
	}
}

/*

action:sellant
cat:2
antid:2751000
price_cent:846.50

407	 Waldschaben
412	 Löwenzahnsamen
421	 Brombeerblätter
453	 gemixtes Zuckerwasser
501	 Reagenzglas
581	 großer Zuchtbehälter
592	 Ausbruchschutz: PTFE
601	 kleines Formicarium
602	 mittleres Formicarium
604	 Mini Arena
605	 mittlere Anlage
606	 große Anlage
612	 mittleres Nest
613	 großes Nest
615	 mittleres YTong Nest
616	 großes YTong Nest
611	 kleines Nest
901	 Regal

 */