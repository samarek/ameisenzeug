package pa.soft.processors;

import pa.soft.data.entities.lexikon.LexikonAmeise;
import pa.soft.data.types.AmeisenType;
import pa.soft.data.types.FormicariumType;
import pa.soft.data.types.NestType;

import java.util.List;

public class FormicarienNestFinder
{
	public static void printFormicarienNestForAmeisenType()
	{
		Integer platzNest;
		Integer platzFormicarium;
		for(AmeisenType ameisenType : AmeisenType.values())
		{
			platzNest = (int) (ameisenType.getMaxPopulation() * ameisenType.getPlatzbedarfNest());
			platzFormicarium = (int) (ameisenType.getMaxPopulation() * ameisenType.getPlatzbedarfFormicarium());

			NestType nestType = null;
			for(NestType currentNestType : NestType.values())
			{
				if(currentNestType.getPlatz() >= platzNest)
				{
					nestType = currentNestType;
					break;
				}
			}

			FormicariumType formicariumType = null;
			for(FormicariumType currentFormicariumType : FormicariumType.values())
			{
				if(currentFormicariumType.getPlatz() >= platzFormicarium)
				{
					formicariumType = currentFormicariumType;
					break;
				}
			}

			System.out.println(ameisenType.getLogName() + platzNest + "\t\t" + nestType.getName() + "\t" + platzFormicarium + "\t" + formicariumType.getNameForLog());
		}
	}

	public static void findFormicarienNestForLexikonAmeisen(List<LexikonAmeise> lexikonAmeisen)
	{
		Integer platzNest;
		Integer platzFormicarium;
		for(LexikonAmeise lexikonAmeise : lexikonAmeisen)
		{
			platzNest = (int) (lexikonAmeise.getMaxKoloniegröße() * lexikonAmeise.getPlatzbedarfNest());
			platzFormicarium = (int) (lexikonAmeise.getMaxKoloniegröße() * lexikonAmeise.getPlatzbedarfFormicarium());

			NestType nestType = null;
			for(NestType currentNestType : NestType.values())
			{
				if(currentNestType.getPlatz() >= platzNest)
				{
					nestType = currentNestType;
					break;
				}
			}
			lexikonAmeise.setMaxNest(nestType);

			FormicariumType formicariumType = null;
			for(FormicariumType currentFormicariumType : FormicariumType.values())
			{
				if(currentFormicariumType.getPlatz() >= platzFormicarium)
				{
					formicariumType = currentFormicariumType;
					break;
				}
			}
			lexikonAmeise.setMaxFormicarium(formicariumType);
		}
	}
}
