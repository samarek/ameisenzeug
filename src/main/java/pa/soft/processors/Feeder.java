package pa.soft.processors;

import pa.soft.data.entities.Spieler;
import pa.soft.data.managers.SpielerManager;
import pa.soft.processors.workerant.FormicariumProcessor;

import java.util.List;

public class Feeder
{
	public static Spieler spieler;

	public static void run()
	{
		List<Spieler> spielerList = SpielerManager.fetchSpieler();

		for(Spieler spieler : spielerList)
		{
			Feeder.spieler = spieler;
			Feeder.spieler = SpielerManager.spielerEinloggen(spieler);
			FormicariumProcessor.process(Feeder.spieler);
		}
	}
}
