package pa.soft.processors;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class Util
{
	public static final DateTimeFormatter FORMATTER_LONG_YEAR = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
	public static final DateTimeFormatter FORMATTER_LONG_YEAR_SECONDS = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
	public static final DateTimeFormatter FORMATTER_SHORT_YEAR = DateTimeFormatter.ofPattern("dd.MM.yy HH:mm");
	public static final DateTimeFormatter FORMATTER_ISO8601 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");


	private static List<String> log = new ArrayList<>();
	public static String logFilePath = "EatenByAnts_default.log";

	public static File createFile(String filepath, String filename)
	{
		File file = new File(filepath + "/" + filename);
		createDirectory(file.getParent());
		if(!file.exists())
		{
			try
			{
				file.createNewFile();
			}
			catch(IOException e)
			{
				e.printStackTrace();
				logException(e);
			}
		}

		return file;
	}

	public static File createDirectory(String folderpath)
	{
		File folder = new File(folderpath);
		if(!folder.exists())
		{
			folder.mkdirs();
		}

		return folder;
	}

	public static void log(String message)
	{
		if(!alreadyLogged(message))
		{
			log.add(makeTimestamp() + " -> " + message);
		}
	}

	public static void logException(Exception e)
	{
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String sStackTrace = sw.toString();

		Util.log("### Exception ###" + System.lineSeparator() + sStackTrace);
	}

	public static void logMap(Map<String, String> map)
	{
		Set<Map.Entry<String, String>> entries = map.entrySet();

		StringBuilder mapString = new StringBuilder("### Map ###" + System.lineSeparator());
		for(Map.Entry<String, String> entry : entries)
		{
			mapString.append(entry.getKey())
					 .append(";")
					 .append(entry.getValue())
					 .append(System.lineSeparator());
		}
		Util.log(mapString.toString());
	}

	public static Map<String, String> readCsv(String filename)
	{
		List<String> lines = readFileLines(filename).stream()
													.filter(s -> !s.startsWith("#"))
													.filter(s -> !s.isEmpty())
													.filter(s -> s.contains(";"))
													.collect(Collectors.toList());
		Map<String, String> outputMap = new HashMap<>();

		for(String line : lines)
		{
			String[] fields = line.split(";");
			outputMap.put(fields[0].trim(), fields[1].trim());
		}

		return outputMap;
	}

	public static LocalDateTime fetchLatestTimestamp(String filename)
	{
		File file = new File(filename);
		String entryDate = "1970-01-01 00:00:00";
		if(file.exists())
		{
			List<String> lines = readFileLines(filename);
			entryDate = lines.get(lines.size() - 1).split(";")[0];

		}

		return LocalDateTime.parse(entryDate, Util.FORMATTER_ISO8601);
	}

	public static List<String> readFileLines(String filename)
	{
		List<String> fileLines = new ArrayList<>();

		try
		{
			FileReader fileReader = new FileReader(filename);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String line;
			while((line = bufferedReader.readLine()) != null)
			{
				fileLines.add(line);
			}
		}
		catch(Exception e)
		{
			Util.logException(e);
		}

		return fileLines;
	}

	private static boolean alreadyLogged(String message)
	{
		Boolean alreadyLogged;

		try
		{
			Scanner scanner = new Scanner(new File(logFilePath));
			List<String> logLines = new ArrayList<>();
			String currentLine;
			while (scanner.hasNextLine())
			{
				currentLine = scanner.nextLine();
				if(!currentLine.contains("========")
						&& currentLine.contains("->")
						&& (
							currentLine.contains("beschäftigt")
							|| currentLine.contains("Unbekannte Nachrichten")
							|| currentLine.contains("CallCount")
						))
				{
					logLines.add(currentLine.substring(currentLine.indexOf("->") + 2).trim());
				}
			}

			alreadyLogged = logLines.contains(message.trim());
		}
		catch(Exception e)
		{
			alreadyLogged = false;
		}

		return alreadyLogged;
	}

	private static String makeTimestamp()
	{
		return LocalDateTime.now().format(Util.FORMATTER_ISO8601);
	}

	public static void logFlush()
	{
		if(log.size() > 1)
		{
			try
			{
				FileWriter fw = new FileWriter(new File(logFilePath), true);
				for(String logEntry : log)
				{
					fw.write(logEntry + System.lineSeparator());
				}
				fw.flush();
				log = new ArrayList<>();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
	}
}
