package pa.soft.processors;

import pa.soft.Main;
import pa.soft.data.entities.Eintrag;
import pa.soft.data.entities.Spieler;
import pa.soft.data.managers.SpielerManager;
import pa.soft.processors.logdisplayer.LogParser;

import java.util.List;
import java.util.Map;

public class LogDisplayer
{
	private static String LOG_FOLDER = "/root/EatenByAnts/BAK";
	private static String OUT_FOLDER = "/var/www/html/ameisen";

	public static void run()
	{
		List<Spieler> spielerList = SpielerManager.fetchSpieler();
		for(Spieler spieler : spielerList)
		{
			spieler = SpielerManager.spielerEinloggen(spieler);
			if(Main.ARGS.containsKey("outfolder"))
			{
				LOG_FOLDER = Main.ARGS.get("outfolder");
				OUT_FOLDER = Main.ARGS.get("outfolder");
			}

			Map<String, List<Eintrag>> logMap = LogParser.parseLogFiles(LOG_FOLDER);

			System.out.println(logMap);
		}
	}
}

