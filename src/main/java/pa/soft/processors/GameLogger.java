package pa.soft.processors;

import pa.soft.Main;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Lager;
import pa.soft.data.entities.ameisenzimmer.details.LagerItem;
import pa.soft.data.entities.draussen.Ergebnis;
import pa.soft.data.managers.ameisenzimmer.LagerManager;
import pa.soft.data.managers.draussen.ErgebnisManager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class GameLogger extends AbstractProcessor
{
	private static String LOG_FOLDER = "/root/EatenByAnts/[SPIELER]/log";

	@Override
	void process(final Spieler spieler)
	{
		LOG_FOLDER = LOG_FOLDER.replace("[SPIELER]", spieler.getName());
		if(Main.ARGS.containsKey("outfolder"))
		{
			LOG_FOLDER = LOG_FOLDER.replace("/root/EatenByAnts", Main.ARGS.get("outfolder"));
		}

		logSpielerdaten(spieler);
		logLager(spieler);
		logDraussenErgebnisse(spieler);
	}

	private void logDraussenErgebnisse(final Spieler spieler)
	{
		logAmeisensucheErgebnisse(spieler);
		logFuttersucheErgebnisse(spieler);
	}

	private static void logSpielerdaten(Spieler spieler)
	{
		writeToFile("geld", spieler.getGeld() + ";");
		writeToFile("punkte", spieler.getPunkte() + ";");
	}

	private static void logLager(final Spieler spieler)
	{
		Lager lager = LagerManager.fetchLager(spieler);
		String lagerFolder = "lager/";

		for(LagerItem futteritem : lager.getFutter())
		{
			writeToFile(lagerFolder + futteritem.getName(), futteritem.getMenge() + ";");
		}

		for(LagerItem formicarienNester : lager.getFormicarienNester())
		{
			writeToFile(lagerFolder + formicarienNester.getName(), formicarienNester.getMenge() + ";");
		}

		for(LagerItem zubehör : lager.getZubehör())
		{
			writeToFile(lagerFolder + zubehör.getName(), zubehör.getMenge() + ";");
		}
	}

	private void logAmeisensucheErgebnisse(final Spieler spieler)
	{
		String ameisenFileName = "/draussen/ameisenErgebnisse";
		final LocalDateTime latestAmeisenTimestamp = Util.fetchLatestTimestamp(LOG_FOLDER + ameisenFileName);
		List<Ergebnis> ameisenErgebnisse = ErgebnisManager.fetchDraussenErgebnisseAmeisen(spieler)
														  .stream()
														  .filter(ergebnis ->
																		  ergebnis.getZeitpunkt().isAfter(latestAmeisenTimestamp))
														  .sorted(Comparator.comparing(Ergebnis::getZeitpunkt))
														  .collect(Collectors.toList());
		if(!ameisenErgebnisse.isEmpty())
		{
			StringBuilder logMessage = new StringBuilder("Neue Ameisenergebnisse" + System.lineSeparator());
			ameisenErgebnisse.forEach(ergebnis -> logMessage.append(ergebnis.getErgebnis()).append(System.lineSeparator()));
			Util.log(logMessage.toString().trim());
			logErgebnisse(ameisenFileName, ameisenErgebnisse);
		}
	}

	private void logFuttersucheErgebnisse(final Spieler spieler)
	{
		String futterFileName = "/draussen/futtersucheErgebnisse";
		final LocalDateTime latestFuttersucheTimestamp = Util.fetchLatestTimestamp(LOG_FOLDER + futterFileName);
		List<Ergebnis> futterErgebnisse = ErgebnisManager.fetchDraussenErgebnisseFuttertiere(spieler)
														 .stream()
														 .filter(ergebnis ->
																		  ergebnis.getZeitpunkt().isAfter(latestFuttersucheTimestamp))
														 .sorted(Comparator.comparing(Ergebnis::getZeitpunkt))
														 .collect(Collectors.toList());
		if(!futterErgebnisse.isEmpty())
		{
			StringBuilder logMessage = new StringBuilder("Neue Futtersuchenergebnisse" + System.lineSeparator());
			futterErgebnisse.forEach(ergebnis -> logMessage.append(ergebnis.getErgebnis()).append(System.lineSeparator()));
			Util.log(logMessage.toString().trim());
			logErgebnisse(futterFileName, futterErgebnisse);
		}
	}

	private void logErgebnisse(final String futterFileName, final List<Ergebnis> futterErgebnisse)
	{
		StringBuilder stringBuilderFuttertiere = new StringBuilder();
		futterErgebnisse.forEach(ergebnis -> stringBuilderFuttertiere.append(ergebnis.getZeitpunktString())
														   .append(";")
														   .append(ergebnis.getErgebnis())
														   .append(System.lineSeparator()));
		writeToFile(futterFileName, stringBuilderFuttertiere.toString());
	}

	private static void writeToFile(String filename, String content)
	{
		File punkteLogFile = Util.createFile(LOG_FOLDER, filename);

		try(FileWriter fw = new FileWriter(punkteLogFile, true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw))
		{
			out.print(content);
		}
		catch (Exception e)
		{
			Util.logException(e);
		}
	}
}
