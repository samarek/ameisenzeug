package pa.soft.processors.workerant;

import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.internet.Immobilie;
import pa.soft.data.managers.internet.ImmobilienManager;
import pa.soft.http.Dispatcher;
import pa.soft.processors.Util;

import java.util.List;

public class ImmobilienProcessor
{
	public static void process(Spieler spieler)
	{
		List<Immobilie> immobilien = ImmobilienManager.fetchImmobilien(spieler);

		for(Immobilie immobilie : immobilien)
		{
			if(immobilie.isAvailable())
			{
				Dispatcher.buyImmobilie(spieler, immobilie);
				Util.log("Immobilienkauf\t" + immobilie.getName() + "\t" + immobilie.getPreis() + "\tGeld\t" + spieler.getGeldString());
			}
		}
	}
}
