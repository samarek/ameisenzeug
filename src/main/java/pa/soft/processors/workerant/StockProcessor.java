package pa.soft.processors.workerant;

import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Lager;
import pa.soft.data.entities.ameisenzimmer.details.LagerItem;
import pa.soft.data.managers.ameisenzimmer.LagerManager;
import pa.soft.http.Dispatcher;
import pa.soft.http.Links;
import pa.soft.processors.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StockProcessor
{
	private static Spieler player;

	private static Map<String, String> shopData = new HashMap<>();

	private static Lager lager;

	public static void restock()
	{
		initialiseLager();
		shopData.put("cat", "5");
		shopData.put("submit", "Kaufen");

		List<LagerItem> lagerItems = lager.getAllItems();

		for(LagerItem lagerItem : lagerItems)
		{
			String[] item = fetchItemToRestock(lagerItem.getName());
			Integer delta = item != null ? Integer.valueOf(item[2]) - lagerItem.getMenge() : 0;

			if(delta > 0
					&& player.getGeld() > (delta * Integer.valueOf(item[3])))
			{
				shopData.put("productname", item[0]);
				shopData.put("id", item[1]);
				shopData.put("amount", delta + "");
				Dispatcher.post(player, Links.AMEISENSHOP, shopData);
				Util.log("Gekauft\t" + delta + " " + item[0] + "\tGeld\t" + player.getGeldString());
			}
		}
	}

	private static void initialiseLager()
	{
		if(lager == null)
		{
			lager = LagerManager.fetchLager(player);
		}
	}

	private static String[] fetchItemToRestock(String name)
	{
		HashMap<String, String[]> itemsToRestock = new HashMap<>();

		// {name, id, mindestmenge, kosten}
		String[] ptfe = {"Ausbruchschutz: PTFE", "592", "15", "299"};
		String[] reagenzglas = {"Reagenzglas", "501", "20", "99"};
		String[] großerZuchtbehälter = {"Großer Zuchtbehälter", "581", "5", "1999"};
		String[] kleinesNest = {"kleines Nest", "611", "10", "1999"};
		String[] mittleresNest = {"mittleres Nest", "612", "10", "2999"};
		String[] großesNest = {"großes Nest", "613", "10", "4999"};
		String[] kleinesFormicarium = {"kleines Formicarium", "601", "10", "1999"};
		String[] mittleresFormicarium = {"mittleres Formicarium", "602", "10", "3999"};
		String[] großesFormicarium = {"großes Formicarium", "603", "10", "9999"};

		itemsToRestock.put(ptfe[0].toLowerCase(), ptfe);
		itemsToRestock.put(reagenzglas[0].toLowerCase(), reagenzglas);
		itemsToRestock.put(großerZuchtbehälter[0].toLowerCase(), großerZuchtbehälter);
		itemsToRestock.put(kleinesNest[0].toLowerCase(), kleinesNest);
		itemsToRestock.put(mittleresNest[0].toLowerCase(), mittleresNest);
		itemsToRestock.put(großesNest[0].toLowerCase(), großesNest);
		itemsToRestock.put(kleinesFormicarium[0].toLowerCase(), kleinesFormicarium);
		itemsToRestock.put(mittleresFormicarium[0].toLowerCase(), mittleresFormicarium);
		itemsToRestock.put(großesFormicarium[0].toLowerCase(), großesFormicarium);

		return itemsToRestock.get(name.toLowerCase());
	}

	public static void setPlayer(Spieler player)
	{
		StockProcessor.player = player;
	}
}
