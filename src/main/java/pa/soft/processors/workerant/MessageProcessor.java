package pa.soft.processors.workerant;

import pa.soft.data.entities.Message;
import pa.soft.data.entities.Spieler;
import pa.soft.http.Dispatcher;
import pa.soft.processors.Util;

import java.util.List;

import static pa.soft.data.managers.MessageManager.KNOWN_SENDERS;

public class MessageProcessor
{
	private static Spieler spieler;

	public static void process(Spieler spieler)
	{
		MessageProcessor.spieler = spieler;
		List<Message> messages = spieler.getMessages();
		Integer unknownMails = messages.size();
		for(Message message : messages)
		{
			if(KNOWN_SENDERS.contains(message.getSender()))
			{
				unknownMails--;
				handleKnownSender(message);
			}
		}
		if(unknownMails > 0)
		{
			Util.log("######\t" + unknownMails + "\tUnbekannte Nachricht" + (unknownMails > 1 ? "en" : "") + "\t######");
		}
	}

	private static void handleKnownSender(Message message)
	{
		Util.log(message.getSender() + "\t" + message.getSubject() + "\tGeld\t" + spieler.getGeldString());
		Dispatcher.deleteMessage(spieler, message.getId());
	}
}
