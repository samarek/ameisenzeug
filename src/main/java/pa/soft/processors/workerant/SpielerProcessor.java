package pa.soft.processors.workerant;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.Task;
import pa.soft.data.entities.ameisenzimmer.Lager;
import pa.soft.data.entities.ameisenzimmer.details.Ameisen;
import pa.soft.data.entities.ameisenzimmer.details.LagerItem;
import pa.soft.data.entities.draussen.DraussenTask;
import pa.soft.data.managers.ameisenzimmer.LagerManager;
import pa.soft.data.managers.draussen.AmeisenSuchenManager;
import pa.soft.data.types.NestType;
import pa.soft.http.Collector;
import pa.soft.http.Dispatcher;
import pa.soft.http.Links;
import pa.soft.processors.Util;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SpielerProcessor
{
	static Spieler spieler;

	static Map<String, String> httpData = new HashMap<>();
	private static Map<String, String> shopData = new HashMap<>();

	private static Lager lager;

	public static void process(Spieler spieler)
	{
		SpielerProcessor.spieler = spieler;
		MessageProcessor.process(spieler);
		initialiseFields();
		initialiseLager();
		if(!spieler.isBusy())
		{
			occupy();
		}
		else
		{
			Util.log("Account " + spieler.getName() + " beschäftigt" + fetchTaskName());
		}
	}

	static void initialiseFields()
	{
		httpData.put("cat", "3");
		httpData.put("takejob", spieler.getJob()[0]);
	}

	static void initialiseLager()
	{
		if(lager == null)
		{
			lager = LagerManager.fetchLager(spieler);
		}
	}

	static void occupy()
	{
		try
		{
			switchToPizzaIfAvailable();
			switchToResearchIfAvailable();
			switchToSamenSammeln();
			switchToAmeisenSammeln();
			switchToZuckerwasserMixen();
			switchToMiniArenaHerstellen();
			switchToRandomGrinding();
			startWorking();
		}
		catch(Exception exc)
		{
			Util.logException(exc);
			exc.printStackTrace();
		}
	}

	static void switchToRandomGrinding()
	{
		Integer factor = (int) ((Math.random() * 1_000_000) % 20);
		String[] task;
		String duration = "60";
		String category = "5";
		if((factor % 3 == 0))
		{
			switch(factor)
			{
				case 0:
					task = new String[] {"997", "Körner und Samen sammeln", Links.DRAUSSEN};
					category = "2";
					break;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					task = new String[] {"998", "Blätter sammeln", Links.DRAUSSEN};
					category = "2";
					break;
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
					task = new String[] {"999", "Futtertiere sammeln", Links.DRAUSSEN};
					category = "2";
					break;
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
					task = new String[] {"707", "Zuckerwasser Massenproduktion (10kg)", Links.AMEISENZIMMER};
					break;
//				case 18:
//					task = new String[] {"705", "Mittleres YTong Nest herstellen ", Links.AMEISENZIMMER};
//					break;
//				case 19:
//					task = new String[] {"706", "Großes YTong Nest herstellen", Links.AMEISENZIMMER};
//					break;
				default:
					DraussenTask ameisenSammelTask = fetchAmeisenSammelTask();
					task = ameisenSammelTask.toStringArray();
					category = "1";
					duration = ameisenSammelTask.getDauer().get(2).toMinutes() + "";
					break;
			}

			httpData.put("cat", category);
			httpData.put("taketask", task[0]);
			httpData.put("dauer", duration);
			spieler.setJob(task);
		}
	}

	static void switchToPizzaIfAvailable()
	{
		Document document = Collector.get(spieler, Links.INTERNET_ARBEITSAMT);
		String content = document.getElementsByAttributeValue("id", "shopitem").html();
		if (content.contains("<input type=\"hidden\" name=\"takejob\" value=\"303\">"))
		{
			httpData.put("cat", "3");
			httpData.put("takejob", Spieler.PIZZA[0]);
			spieler.setJob(Spieler.PIZZA);
		}
	}

	static void switchToResearchIfAvailable()
	{
		Document document = Collector.get(spieler, Links.INTERNET_FORSCHUNG);
		Elements elements = document.getElementsByAttributeValue("id", "shopitem");
		for(Element element : elements)
		{
			String titel = element.select("b").get(0).text();
			if(!element.getElementsByTag("input").get(2).hasAttr("disabled")
					&& spieler.getForschung() != null
					&& titel.toLowerCase().contains(spieler.getForschung().toLowerCase()))
			{
				String id = element.select("input").get(1).val();
				String[] task = {id, titel, Links.INTERNET};

				httpData.put("cat", "1");
				httpData.put("taketask", task[0]);
				spieler.setJob(task);
				break;
			}
		}
	}

	private static void switchToSamenSammeln()
	{
		String samen = "Löwenzahnsamen";
		String[] task = {"997", "Körner und Samen sammeln", Links.DRAUSSEN};
		for(LagerItem lagerItem : lager.getFutter())
		{
			if (lagerItem.getName().equals(samen)
					&& lagerItem.getMenge() < 2_000)
			{
				httpData.put("cat", "2");
				httpData.put("taketask", task[0]);
				httpData.put("dauer", "60");
				spieler.setJob(task);
				break;
			}
		}
	}

	private static void switchToAmeisenSammeln()
	{
		if(spieler.isAntGatheringWanted() && gatheringNeeded())
		{
			DraussenTask sammelTask = fetchAmeisenSammelTask();
			httpData.put("cat", "1");
			httpData.put("taketask", sammelTask.getId() + "");
			httpData.put("dauer", sammelTask.getDauer().get(2).toMinutes() + "");
			spieler.setJob(sammelTask.toStringArray());
		}
	}

	private static Boolean gatheringNeeded()
	{
		Boolean gatherAnts = Boolean.TRUE;

		for(Ameisen ameisen : lager.getAmeisen())
		{
			if(ameisen.getDrones() == 0
					&& ameisen.getNestType().equals(NestType.TEST_TUBE))
			{
				gatherAnts = Boolean.FALSE;
			}
		}
		return gatherAnts;
	}

	private static DraussenTask fetchAmeisenSammelTask()
	{
		List<Task> sammelTasks = AmeisenSuchenManager.fetchAmeisenSuchen(spieler);

		DraussenTask sammelTask;

		do
		{
			Double randomDouble = (Math.random() * 100_000) % sammelTasks.size();
			Integer taskNumber = randomDouble.intValue();

			sammelTask = (DraussenTask) sammelTasks.get(taskNumber);
		} while(sammelTask.getMoney() > spieler.getGeld());

		return sammelTask;
	}

	private static void switchToZuckerwasserMixen()
	{
		String zuckerwasser = "gemixtes Zuckerwasser";
		String[] task = {"707", "Zuckerwasser Massenproduktion (10kg)", Links.AMEISENZIMMER};
		for(LagerItem lagerItem : lager.getFutter())
		{
			if (lagerItem.getName().equals(zuckerwasser)
					&& lagerItem.getMenge() < 150_000)
			{
				httpData.put("cat", "5");
				httpData.put("taketask", task[0]);
				spieler.setJob(task);
				break;
			}
		}
	}


	private static void switchToMiniArenaHerstellen()
	{
		String miniArena = "Mini Arena";
		String[] task = {"702", "Mini Arena herstellen", Links.AMEISENZIMMER};
		Boolean mussHerstellen = Boolean.TRUE;
		for(LagerItem lagerItem : lager.getFormicarienNester())
		{
			if (lagerItem.getName().equals(miniArena)
					&& lagerItem.getMenge() >= 5)
			{
				mussHerstellen = Boolean.FALSE;
				break;
			}
		}
		if(mussHerstellen)
		{
			httpData.put("cat", "5");
			httpData.put("taketask", task[0]);
			spieler.setJob(task);
		}
	}

	static void startWorking()
	{
		String link =  spieler.getJob()[2];
		Dispatcher.post(spieler, link, httpData);
		Util.log("Aktion " + spieler.getJob()[1] + " gestartet. " +
							  "Geld übrig: " + spieler.getGeldString() + " €");
		Util.log("Account " + spieler.getName() + " beschäftigt" + fetchTaskName());
	}

	static String fetchTaskName()
	{
		String activityName = "";

		Document document = Collector.get(spieler, Links.INTERNET);
		Element element = document.getElementById("shopitem");

		String header = extractHeader(document);
		if(element != null)
		{
			Elements elements = element.select("form b:first-of-type");
			activityName = " - " + elements.text();
		}

		return activityName + " " + header + "\t" ;
	}

	private static String extractHeader(Document document)
	{
		String start = "Aktion:<span>bis";
		String header = document.select("#header>#status script").html();
		Integer startIndex = header.indexOf(start) + start.length();
		Integer endIndex = header.indexOf("</span>");
		header = header.substring(startIndex, endIndex).trim();

		LocalDateTime localDateTime = LocalDateTime.parse(header, Util.FORMATTER_LONG_YEAR_SECONDS).plusHours(2);

		return "bis " + localDateTime.format(Util.FORMATTER_LONG_YEAR_SECONDS);
	}

	public static void setLager(final Lager lager)
	{
		SpielerProcessor.lager = lager;
	}
}
