package pa.soft.processors.workerant;

public class ProcessorArgs
{
	private boolean fast;

	public boolean isFast()
	{
		return fast;
	}

	public void setFast(boolean fast)
	{
		this.fast = fast;
	}
}
