package pa.soft.processors.workerant;

import pa.soft.data.entities.Spieler;
import pa.soft.http.Links;
import pa.soft.processors.Util;

import java.util.Arrays;

public class FarmingAccountProcessor extends SpielerProcessor
{
	public static void process(Spieler spieler)
	{
		SpielerProcessor.spieler = spieler;
		MessageProcessor.process(spieler);
		initialiseFields();
		initialiseLager();
		if(!spieler.isBusy())
		{
			occupy();
		}
		else
		{
			Util.log("Account " + spieler.getName() + " beschäftigt" + fetchTaskName());
		}
	}

	static void occupy()
	{
		try
		{
			switchToPizzaIfAvailable();
			switchToResearchIfAvailable();
			switchToRandomGrinding();
			startWorking();
		}
		catch(Exception exc)
		{
			Util.logException(exc);
			exc.printStackTrace();
		}
	}

	static void switchToRandomGrinding()
	{
		Integer factor = (int) ((Math.random() * 1_000_000) % 10);
		String[] task;
		String duration = "60";
		String category = "2";
		if(factor % 5 == 0)
		{
			switch(factor)
			{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
					task = new String[]{"997", "Körner und Samen sammeln", Links.DRAUSSEN};
					break;
				//
				//				case 7:
				//					task = new String[] {"998", "Blätter sammeln", Links.DRAUSSEN};
				//					break;
				default:
					task = new String[]{"999", "Futtertiere sammeln", Links.DRAUSSEN};
					break;
				//				case 8:
				//				case 9:
				//				case 10:
				//				case 11:
				//					task = new String[] {"707", "Zuckerwasser Massenproduktion (10kg)", Links.AMEISENZIMMER};
				//					String category = "5";
				//					break;
			}

			httpData.put("cat", category);
			httpData.put("taketask", task[0]);
			httpData.put("dauer", duration);
			spieler.setJob(task);
		}
	}
}
