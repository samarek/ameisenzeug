package pa.soft.processors.workerant;

import org.jsoup.nodes.Document;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Formicarium;
import pa.soft.data.entities.ameisenzimmer.details.FutterItem;
import pa.soft.data.types.FutterType;
import pa.soft.http.Dispatcher;
import pa.soft.processors.Util;

public class FoodProcessor
{
	private static final Integer INDEX_SAMEN_ZUCKER = 4;
	private static final Integer INDEX_PROTEIN_BLÄTTER = 5;

	private static Spieler spieler;

	static void cleanUp(Formicarium formicarium)
	{
		for(FutterItem futterItem : formicarium.getFormicariumDetails().getFutterItems())
		{
			Boolean proteinTooFar = futterItem.getFutterType().equals(FutterType.PROTEIN) &&
					(futterItem.getZustand() + formicarium.getProteinhunger() < 95);

			if(futterItem.getZustand() < 60 || futterItem.getGramm() == 0.0 || proteinTooFar)
			{
				Dispatcher.entferneFutteritem(spieler, formicarium, futterItem);
				if(futterItem.getGramm() > 0.0)
				{
					Util.log(formicarium.toLogString() + "Futter entfernt\t" + futterItem.getName() + "\t" + futterItem.getGramm() + " Gramm");
				}
			}
		}
	}

	static void regularSupply(Formicarium formicarium)
	{
		if(formicarium.getZuckerhunger() != null)
		{
			manageZuckerSupply(formicarium);
		}
		if(formicarium.getSamenvorrat() != null)
		{
			manageSamenSupply(formicarium);
		}
		if(formicarium.getProteinhunger() != null)
		{
			manageProteinSupply(formicarium);
		}
		if(formicarium.getPilzstatus() != null)
		{
			manageBlätterSupply(formicarium);
		}
	}

	private static void manageZuckerSupply(Formicarium formicarium)
	{
		Boolean hasNoZucker = formicarium.getZuckerhunger() >= 15 && !hasZucker(formicarium);
		Boolean isTooZuckerHungry = formicarium.getZuckerhunger() >= 30;
		if(hasNoZucker || isTooZuckerHungry)
		{
			supplyZucker(formicarium);
		}
	}

	private static void manageSamenSupply(Formicarium formicarium)
	{
		Boolean hasNoSamen = formicarium.getSamenvorrat() <= 85 && !hasSamen(formicarium);
		Boolean isTooSamenHungry = formicarium.getSamenvorrat() <= 70;
		if(hasNoSamen || isTooSamenHungry)
		{
			supplySamen(formicarium);
		}
	}

	private static void manageProteinSupply(Formicarium formicarium)
	{
		Boolean hasNoProtein = formicarium.getProteinhunger() >= 10 && !hasProtein(formicarium);
		Boolean isTooProteinHungry = formicarium.getProteinhunger() >= 25;
		if(hasNoProtein || isTooProteinHungry)
		{
			supplyProtein(formicarium);
		}
	}

	private static void manageBlätterSupply(Formicarium formicarium)
	{
		Boolean hasNoBlätter = formicarium.getPilzstatus() <= 90 && !hasBlätter(formicarium);
		Boolean isTooBlätterHungry = formicarium.getPilzstatus() <= 75;
		if(hasNoBlätter || isTooBlätterHungry)
		{
			supplyBlätter(formicarium);
		}
	}

	private static boolean hasZucker(Formicarium formicarium)
	{
		return hasType(formicarium, FutterType.ZUCKER);
	}

	private static boolean hasSamen(Formicarium formicarium)
	{
		return hasType(formicarium, FutterType.SAMEN);
	}

	private static boolean hasBlätter(Formicarium formicarium)
	{
		return hasType(formicarium, FutterType.BLÄTTER);
	}

	private static boolean hasProtein(Formicarium formicarium)
	{
		return hasType(formicarium, FutterType.PROTEIN);
	}

	private static boolean hasType(Formicarium formicarium, FutterType futterType)
	{
		Boolean hasType = false;
		for(FutterItem futterItem : formicarium.getFormicariumDetails().getFutterItems())
		{
			if(futterItem.getFutterType().equals(futterType)
					&& futterItem.getGramm() > 0)
			{
				hasType = true;
				break;
			}
		}

		return hasType;
	}

	private static void supplyZucker(Formicarium formicarium)
	{
		Document document = Dispatcher.feedZucker(spieler, formicarium);
		Integer currentHunger = extractHunger(document, 4);

		if(currentHunger > 0)
		{
			Integer menge = generateMenge(currentHunger, formicarium, FutterType.ZUCKER);
			Dispatcher.feedZuckerMenge(spieler, formicarium, menge);
			Util.log(formicarium.toLogString() + "Zucker-Versorgung\t" + menge);
		}
		else
		{
			Util.log(formicarium.toLogString() + "Zucker-Versorgung");
		}
	}

	private static void supplySamen(Formicarium formicarium)
	{
		Document document = Dispatcher.feedSamen(spieler, formicarium);
		Integer currentHunger = extractHunger(document, 4);

		if(currentHunger < 1_000)
		{
			Integer menge = generateMenge(currentHunger, formicarium, FutterType.SAMEN);
			Dispatcher.feedSamenMenge(spieler, formicarium, menge);
			Util.log(formicarium.toLogString() + "Samen-Versorgung\t" + menge);
		}
		else
		{
			Util.log(formicarium.toLogString() + "Samen-Versorgung");
		}
	}

	private static void supplyProtein(Formicarium formicarium)
	{
		Document document = Dispatcher.feedProtein(spieler, formicarium);
		Integer currentHunger = extractHunger(document, 5);

		if(currentHunger > 0)
		{
			Integer menge = generateMenge(currentHunger, formicarium, FutterType.PROTEIN);
			Dispatcher.feedProteinMenge(spieler, formicarium, menge);
			Util.log(formicarium.toLogString() + "Protein-Versorgung\t" + menge);
		}
		else
		{
			Util.log(formicarium.toLogString() + "Protein-Versorgung");
		}
	}

	private static void supplyBlätter(Formicarium formicarium)
	{
		Document document = Dispatcher.feedBlätter(spieler, formicarium);
		Integer currentHunger = extractHunger(document, 5);

		if(currentHunger < 1_000)
		{
			Integer menge = generateMenge(currentHunger, formicarium, FutterType.BLÄTTER);
			Dispatcher.feedBlätterMenge(spieler, formicarium, menge);
			Util.log(formicarium.toLogString() + "Blätter-Versorgung\t" + menge);
		}
		else
		{
			Util.log(formicarium.toLogString() + "Blätter-Versorgung");
		}
	}

	private static Integer generateMenge(Integer currentHunger, Formicarium formicarium, FutterType futterType)
	{
		Integer formerHunger = 0;
		Integer trIndex = INDEX_SAMEN_ZUCKER;
		Integer total = 1;
		Integer runs = 0;
		switch(futterType)
		{
			case ZUCKER:
				formerHunger = (int) (formicarium.getZuckerhunger() * 10);
				break;
			case SAMEN:
				formerHunger = (int) (1_000 - (formicarium.getSamenvorrat() * 10));
				currentHunger = 1_000 - currentHunger;
				break;
			case PROTEIN:
				formerHunger = (int) (formicarium.getProteinhunger() * 10);
				trIndex = INDEX_PROTEIN_BLÄTTER;
				break;
			case BLÄTTER:
				formerHunger = (int) (1_000 - (formicarium.getPilzstatus() * 10));
				currentHunger = 1_000 - currentHunger;
				trIndex = INDEX_PROTEIN_BLÄTTER;
				break;
		}
		Double divisor = (double) (formerHunger - currentHunger);

		Integer menge = 10;
		while(divisor == 0)
		{
			Document document = Dispatcher.feedTypeMenge(spieler, formicarium, menge, futterType);
			System.out.println("Fed " + menge);
			total += menge;
			runs++;
			currentHunger = extractHunger(document, trIndex);
			divisor = (formerHunger - currentHunger) / Double.valueOf(menge);
		}
		Integer baseMenge = (int)(currentHunger / divisor);
		if(runs > 0)
		{
			Util.log(formicarium.toLogString() + runs + " Runs\tfed " + total);
			baseMenge *= runs;
		}

		return baseMenge + 1;
	}

	private static Integer extractHunger(Document document, Integer trIndex)
	{
		String hungerText = document.select(".inner tr:nth-child(" + trIndex + ") td:nth-child(2)").html();
		Integer start = hungerText.indexOf("'),") + 3;
		Integer ende = hungerText.indexOf(",", start);
		String hungerValue = hungerText.substring(start, ende);

		if(!hungerValue.contains("."))
		{
			hungerValue += 0;
		}

		return Integer.valueOf(hungerValue.replace(".", "").trim());
	}

	public static void setSpieler(Spieler spieler)
	{
		FoodProcessor.spieler = spieler;
	}
}
