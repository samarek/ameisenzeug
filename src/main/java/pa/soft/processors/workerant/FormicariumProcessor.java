package pa.soft.processors.workerant;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Formicarium;
import pa.soft.data.entities.ameisenzimmer.Lager;
import pa.soft.data.entities.ameisenzimmer.Möbel;
import pa.soft.data.entities.ameisenzimmer.Queen;
import pa.soft.data.entities.ameisenzimmer.details.LagerItem;
import pa.soft.data.managers.ameisenzimmer.FormicarienManager;
import pa.soft.data.managers.ameisenzimmer.LagerManager;
import pa.soft.data.managers.ameisenzimmer.MöbelManager;
import pa.soft.data.types.AmeisenType;
import pa.soft.data.types.AusbruchschutzType;
import pa.soft.data.types.FormicariumType;
import pa.soft.data.types.NestType;
import pa.soft.http.Collector;
import pa.soft.http.Dispatcher;
import pa.soft.http.Links;
import pa.soft.processors.Util;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class FormicariumProcessor
{
	private static Spieler player;
	private static List<Formicarium> formicarien;
	private static List<Möbel> möbelList;
	private static Lager lager;

	public static void process(Spieler spieler)
	{
		FormicariumProcessor.player = spieler;
		FormicariumProcessor.formicarien = FormicarienManager.fetchFormicarien(spieler);
		FormicariumProcessor.möbelList = MöbelManager.fetchMöbel(spieler);
		FormicariumProcessor.lager = LagerManager.fetchLager(spieler);

		FoodProcessor.setSpieler(spieler);

		checkFormicarien();
		setOutNewFormicarium();
	}

	private static void checkFormicarien()
	{
		for(Formicarium formicarium : formicarien)
		{
			FoodProcessor.cleanUp(formicarium);
			FoodProcessor.regularSupply(formicarium);
			storeIfFull(formicarium);
		}
	}

	private static void storeIfFull(Formicarium formicarium)
	{
		if(formicarium.getNestType() == null)
		{
			return;
		}
		Boolean isFullAnzucht = formicarium.getNestType().equals(NestType.TEST_TUBE) &&
				formicarium.getFormicariumDetails().getNestZustand() >= 90;
		Boolean isJustFull = formicarium.getFormicariumDetails().getNestZustand() > 95 ||
				formicarium.getFormicariumDetails().getFormicariumZustand() > 95;
		Boolean isFinished = Objects.equals(formicarium.getBrut().getDrones(),
							   formicarium.getAmeisenType().getMaxPopulation());

		if(isFullAnzucht || isJustFull || isFinished)
		{
			String message = isFullAnzucht ? "Anzucht beendet" : "Lagerung";
			message = isFinished ? "Ausgewachsen" : message;
			Dispatcher.storeFormicarium(player, formicarium);
			Util.log(formicarium.toLogString() + message);
		}
	}

	private static void setOutNewFormicarium()
	{
		Double plätzeFrei = möbelList.stream()
									 .mapToDouble(Möbel::getPlätzeFrei)
									 .sum();
		Boolean hasFuttertierbox = lager.getZubehör().stream()
										.anyMatch(lagerItem -> lagerItem.getName().contains("Zuchtbehälter"));

		while(plätzeFrei >= 2 && hasFuttertierbox)
		{
			createTwoFuttertierboxen();
			plätzeFrei -= 2;
		}

		if(plätzeFrei > 0 && plätzeFrei < 2)
		{
			createNewFormicarien(plätzeFrei);
		}
	}

	private static void createTwoFuttertierboxen()
	{
		Map<String, String> httpData = new HashMap<>();
		httpData.put("cat","8");
		httpData.put("specie","407");
		httpData.put("amount","500");
		httpData.put("box","581");
		httpData.put("loc","finishedcreateBox");

		Dispatcher.post(player, Links.AMEISENZIMMER, httpData);
		Dispatcher.post(player, Links.AMEISENZIMMER, httpData);
		Util.log("2 Futtertierboxen aufgestellt");
	}

	private static void createNewFormicarien(Double plätzeFrei)
	{
		FormicariumType biggestFormicarium = FormicariumType.MINI_ARENA; //fetchBiggestPossible(plätzeFrei);
		Integer formicarienLeft = fetchFormicariumLeft(biggestFormicarium);
		LinkedList<String> queenIds = fetchQueenIds();
		List<Queen> queens = fetchQueens(biggestFormicarium);
		Map<String, String> httpData = prepareHttpData();
		String queenId, queen;

		while (plätzeFrei >= biggestFormicarium.getRegalplatz()
				&& formicarienLeft > 0
				&& queenIds.size() > 0
				)
		{
			plätzeFrei -= biggestFormicarium.getRegalplatz();
			formicarienLeft--;
			queen = queenIds.pop();
			queenId = queen.substring(0, queen.indexOf(" ")).trim();
			httpData.put("colo", queenId);
			httpData.put("arena", biggestFormicarium.getIdString());
			Dispatcher.post(player, Links.AMEISENZIMMER, httpData);
			Util.log("Anzucht begonnen\t" + queen);
		}
	}

	private static Integer fetchFormicariumLeft(FormicariumType formicariumType)
	{
		List<LagerItem> formicarienNester = lager.getFormicarienNester();
		Integer formicarienLeft = 0;
		for(LagerItem lagerItem : formicarienNester)
		{
			if(lagerItem.getName().equals(formicariumType.getName()))
			{
				formicarienLeft = lagerItem.getMenge();
				break;
			}
		}

		return formicarienLeft;
	}

	private static Integer fetchMiniArenasLeft()
	{
		List<LagerItem> formicarienNester = lager.getFormicarienNester();
		Integer miniArensLeft = 0;
		for(LagerItem lagerItem : formicarienNester)
		{
			if(lagerItem.getName().equals("Mini Arena"))
			{
				miniArensLeft = lagerItem.getMenge();
				break;
			}
		}

		return miniArensLeft;
	}

	private static List<Queen> fetchQueens(FormicariumType biggestFormicarium)
	{
		Elements storedColonies = Collector.fetchColonyElements(player);
		List<Queen> queens = storedColonies.stream()
										   .map(FormicariumProcessor::extractQueen)
										   .collect(Collectors.toList());
		return queens.stream()
					 .filter(queen ->
									 queen.getAmeisenType().getMaxFormicarium().smallerOrEqual(biggestFormicarium))
					 .sorted(Comparator.comparing(Queen::getNestType))
					 .collect(Collectors.toList());
	}

	private static Queen extractQueen(Element storedColony)
	{
		//     <option value="2748536">Lasius niger (1K. 0A. im Reagenzglas )</option>

		String colonyString = storedColony.text();
		Integer openBracketIndex = colonyString.indexOf("(");
		Integer closeBracketIndex = colonyString.indexOf(")");
		Integer kIndex = colonyString.indexOf("K.");
		Integer aIndex = colonyString.indexOf("A.");
		Integer imIndex = colonyString.indexOf(" im ");
		String name = colonyString
				.substring(0, openBracketIndex).trim();
		String queens = colonyString
				.substring(openBracketIndex, kIndex)
				.replace("(", "").trim();
		String workers = colonyString
				.substring(kIndex + "K.".length(), aIndex)
				.replace("K.", "").trim();
		String nestString = colonyString
				.substring(imIndex + " im ".length(), closeBracketIndex)
				.replace("im", "").trim();

		return new Queen(
				storedColony.val(),
				AmeisenType.fromString(name),
				Integer.valueOf(queens),
				Integer.valueOf(workers),
				NestType.fromString(nestString)
		);
	}

	private static LinkedList<String> fetchQueenIds()
	{
		Elements storedColonies = Collector.fetchColonyElements(player);
		LinkedList<String> queenIds = new LinkedList<>();

		String name;

		for(Element storedColony : storedColonies)
		{
			name = storedColony.text().substring(0, storedColony.text().indexOf("(")).trim();

			if(storedColony.text().contains(" 0A."))
			{
				queenIds.add(storedColony.val() + " " + name);
			}
		}

		return queenIds;
	}

	private static Map<String, String> prepareHttpData()
	{
		Map<String, String> httpData = new HashMap<>();

		httpData.put("ausbruch", AusbruchschutzType.PTFE.getIdString());
		httpData.put("loc", "finishedcreateFormi");
		httpData.put("nest", "-1");

		return httpData;
	}
}
