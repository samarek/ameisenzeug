package pa.soft.processors.workerant;

import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Futtertierbox;
import pa.soft.data.managers.ameisenzimmer.FuttertierboxManager;
import pa.soft.http.Dispatcher;
import pa.soft.http.Links;
import pa.soft.processors.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class FuttertierboxProcessor
{
	private static Spieler player;
	private static List<Futtertierbox> futtertierboxen;

	public static void process(Spieler player)
	{
		FuttertierboxProcessor.player = player;
		initialiseFuttertierboxen();

		final Optional<Futtertierbox> overcrowdedBox = futtertierboxen.stream().filter(futtertierbox -> futtertierbox.getRoom() >= 75).findAny();
		final Integer total = overcrowdedBox.isPresent() ? handleBoxes() : 0;

		final Integer futtertierboxenSize = futtertierboxen.size();
		if(total > 0)
		{
			final Integer average = (total / futtertierboxenSize);
			Util.log("Futtertier Lagerung\tTotal\t" + total
							 + "\tDurchschnitt\t" + average
							 + "\tFuttertierboxen\t" + futtertierboxen.size());
		}
		if(futtertierboxenSize > 0
				&& futtertierboxen.get(futtertierboxenSize - 1).getSpeciesStorage() > 50_000)
		{
			sellWaldschaben();
			sellWaldschaben();
		}
	}

	private static Integer handleBoxes()
	{
		Integer total = 0;
		for(Futtertierbox futtertierbox : futtertierboxen)
		{
			total += handleFuttertierbox(futtertierbox);
		}
		return total;
	}

	private static void initialiseFuttertierboxen()
	{
		if(futtertierboxen == null)
		{
			futtertierboxen = FuttertierboxManager.fetchFuttertierboxen(player);
		}
	}

	private static Integer handleFuttertierbox(Futtertierbox futtertierbox)
	{
		if(futtertierbox.getState() <= 75)
		{
			takeCareOf(futtertierbox);
		}

		Integer maxPopulation = futtertierbox.getBig() ? 500 : 250;
		Integer amount = futtertierbox.getCount() - maxPopulation;
		reduceAmount(futtertierbox, amount + "");

		return amount;
	}

	private static void takeCareOf(Futtertierbox futtertierbox)
	{
		Map<String, String> httpData = new HashMap<>();

		httpData.put("care", "Versorgen");
		httpData.put("cat", "8");
		httpData.put("action", "care");
		httpData.put("boxid", futtertierbox.getId() + "");

		Dispatcher.post(player, Links.AMEISENZIMMER, httpData);
	}

	private static void reduceAmount(Futtertierbox futtertierbox, String amount)
	{
		Map<String, String> httpData = new HashMap<>();

		httpData.put("amount", amount);
		httpData.put("remove", "-");
		httpData.put("cat", "8");
		httpData.put("action", "changeamount");
		httpData.put("boxid", futtertierbox.getId() + "");

		Dispatcher.post(player, Links.AMEISENZIMMER, httpData);
	}

	private static void sellWaldschaben()
	{
		Map<String, String> httpData = new HashMap<>();

		String menge = "2750";
		String preis = "9.5";

		httpData.put("action", "sellitem");
		httpData.put("cat", "2");
		httpData.put("itemid", "407");
		httpData.put("price_cent", preis);
		httpData.put("amount", menge);

		Util.log("Kleinanzeige\t" + menge + " Waldschaben\t" + preis + "€");

		Dispatcher.post(player, Links.INTERNET, httpData);
	}

	public static void setFuttertierboxen(final List<Futtertierbox> futtertierboxen)
	{
		FuttertierboxProcessor.futtertierboxen = futtertierboxen;
	}
}
