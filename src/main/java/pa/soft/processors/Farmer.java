package pa.soft.processors;

import pa.soft.Main;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Futtertierbox;
import pa.soft.data.entities.ameisenzimmer.Lager;
import pa.soft.data.entities.internet.details.KleinanzeigeAmeisen;
import pa.soft.data.managers.SpielerManager;
import pa.soft.data.managers.ameisenzimmer.FuttertierboxManager;
import pa.soft.data.managers.ameisenzimmer.LagerManager;
import pa.soft.data.managers.internet.KleinanzeigenManager;
import pa.soft.processors.workerant.FarmingAccountProcessor;
import pa.soft.processors.workerant.FuttertierboxProcessor;

import java.util.List;
import java.util.function.Predicate;

public class Farmer
{
	private List<Futtertierbox> futtertierboxen;
	private List<KleinanzeigeAmeisen> kleinanzeigenAmeisen;
	private Lager lager;
	private Predicate<KleinanzeigeAmeisen> kleinanzeigenFilter
			= kleinanzeigeAmeisen -> kleinanzeigeAmeisen.getVerkäufer().equalsIgnoreCase("cumeartand");

	public void run()
	{
		String spielerName = Main.ARGS.get("playername");
		Spieler spieler = SpielerManager.fetchSpieler(spielerName);

		futtertierboxen = FuttertierboxManager.fetchFuttertierboxen(spieler);
		kleinanzeigenAmeisen = KleinanzeigenManager.fetchKleinanzeigenAmeisen(spieler);
		lager = LagerManager.fetchLager(spieler);

		process(spieler);
	}

	void process(final Spieler spieler)
	{
		FarmingAccountProcessor.setLager(lager);
		FarmingAccountProcessor.process(spieler);

		if(futtertierboxen != null && futtertierboxen.size() > 0)
		{
			FuttertierboxProcessor.setFuttertierboxen(futtertierboxen);
			FuttertierboxProcessor.process(spieler);
		}
		if(kleinanzeigenAmeisen != null && kleinanzeigenAmeisen.size() > 0)
		{
			KleinanzeigenProcessor.setKleinanzeigenFilter(kleinanzeigenFilter);
			KleinanzeigenProcessor.setKleinanzeigenAmeisen(kleinanzeigenAmeisen);
			KleinanzeigenProcessor.process(spieler);
		}
	}
}
