package pa.soft.processors;

import pa.soft.data.entities.Spieler;
import pa.soft.data.managers.SpielerManager;
import pa.soft.processors.workerant.FuttertierboxProcessor;

import java.util.List;

public class Futtertierboxer
{
	public static void run()
	{
		List<Spieler> spielerList = SpielerManager.fetchSpieler();
		for(Spieler spieler : spielerList)
		{
			spieler = SpielerManager.spielerEinloggen(spieler);
			FuttertierboxProcessor.process(spieler);
		}
	}
}
