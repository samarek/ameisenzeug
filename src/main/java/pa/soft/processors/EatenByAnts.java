package pa.soft.processors;

import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Formicarium;
import pa.soft.data.managers.SpielerManager;
import pa.soft.data.managers.ameisenzimmer.*;
import pa.soft.data.managers.draussen.AmeisenSuchenManager;
import pa.soft.data.managers.draussen.ErgebnisManager;
import pa.soft.data.managers.draussen.FutterSuchenManager;
import pa.soft.data.managers.internet.ArbeitManager;
import pa.soft.data.managers.internet.ForschungManager;
import pa.soft.data.managers.internet.ImmobilienManager;
import pa.soft.data.managers.internet.KleinanzeigenManager;

import java.util.List;

public class EatenByAnts
{
	public static void run()
	{
		List<Spieler> spielerList = SpielerManager.fetchSpieler();

		for(Spieler spieler : spielerList)
		{
			spieler = SpielerManager.spielerEinloggen(spieler);

			List<Formicarium> formicarien = FormicarienManager.fetchFormicarien(spieler);

			System.out.println("Yo");

//			spieler.setAnkaufPreise(AnkaufPreisManager.fetchAnkaufPreise(spieler));

//			spieler.setAmeisenSchutzWarte(AmeisenSchutzWarteManager.fetchAmeisenSchutzWarte(spieler));
//			spieler = initialiseSpieler(spieler);

//			System.out.println(spieler);
		}
	}

	private static Spieler initialiseSpieler(Spieler spieler)
	{
		// Ameisenzimmer
		spieler.setFormicarien(FormicarienManager.fetchFormicarien(spieler));
		spieler.setLager(LagerManager.fetchLager(spieler));
		spieler.setEreignisse(EreignisManager.fetchEreignisse(spieler));
		spieler.setMöbel(MöbelManager.fetchMöbel(spieler));
		spieler.setAktionen(AktionManager.fetchAktionen(spieler));
		spieler.setFuttertierboxen(FuttertierboxManager.fetchFuttertierboxen(spieler));
		// Internet
		spieler.setForschungen(ForschungManager.fetchForschungen(spieler));
		spieler.setKleinanzeigenAmeisen(KleinanzeigenManager.fetchKleinanzeigenAmeisen(spieler));
		spieler.setKleinanzeigenSonstiges(KleinanzeigenManager.fetchKleinanzeigenSonstiges(spieler));
		spieler.setArbeiten(ArbeitManager.fetchArbeiten(spieler));
		spieler.setImmobilien(ImmobilienManager.fetchImmobilien(spieler));
		spieler.setMöbelAngebote(MöbelManager.fetchMöbelAngebote(spieler));
		// draussen
		spieler.setAmeisenSuchen(AmeisenSuchenManager.fetchAmeisenSuchen(spieler));
		spieler.setFutterSuchen(FutterSuchenManager.fetchFutterSuchen(spieler));
		spieler.setAmeisenSuchenErgebnisse(ErgebnisManager.fetchDraussenErgebnisseAmeisen(spieler));
		spieler.setFutterSuchenErgebnisse(ErgebnisManager.fetchDraussenErgebnisseFuttertiere(spieler));

		return spieler;
	}
}
