package pa.soft.processors;

import pa.soft.data.entities.Spieler;
import pa.soft.data.managers.SpielerManager;
import pa.soft.processors.ameisenkurser.BrutPlotter;
import pa.soft.processors.ameisenkurser.PreisPlotter;

import java.util.List;

public class AmeisenKurser extends AbstractProcessor
{
	public static Spieler spieler;

	@Override
	void process(final Spieler spieler)
	{
		AmeisenKurser.spieler = spieler;

		PreisPlotter.run(spieler);
		BrutPlotter.run(spieler);
	}
}
