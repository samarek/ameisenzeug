package pa.soft.processors;

import pa.soft.Main;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.lexikon.LexikonAmeise;
import pa.soft.data.managers.SpielerManager;
import pa.soft.data.managers.lexikon.LexikonAmeisenManager;

import java.util.Arrays;
import java.util.List;

public class LexikonPrinter
{
	private static final String LEXIKON_ARG = "lexikon";

	private static Spieler spieler;

	public static void run()
	{
		List<Spieler> spielerList = SpielerManager.fetchSpieler();
		for(Spieler spieler : spielerList)
		{
			spieler = SpielerManager.spielerEinloggen(spieler);
			print(spieler);
		}
	}

	private static void print(Spieler spieler)
	{
		LexikonPrinter.spieler = spieler;

		if(Main.ARGS.containsKey(LEXIKON_ARG))
		{
			switch(Main.ARGS.get(LEXIKON_ARG).toLowerCase())
			{
				case "ameisen":
					printAmeisen();
					break;
				default:
					break;
			}
		}
	}

	private static void printAmeisen()
	{
		List<LexikonAmeise> ameisen = LexikonAmeisenManager.fetchLexikonAmeisen(spieler);

		FormicarienNestFinder.findFormicarienNestForLexikonAmeisen(ameisen);

		StringBuilder ameisenTypes = new StringBuilder("AmeisenTypes");
		for(LexikonAmeise lexikonAmeise : ameisen)
		{
			ameisenTypes.append(System.lineSeparator());
			ameisenTypes.append(lexikonAmeise.toAmeisenType());

			closeLine(ameisen, ameisenTypes, ameisen.indexOf(lexikonAmeise));
			breakAfterRegions(ameisenTypes, lexikonAmeise);
		}

		Util.log(ameisenTypes.toString());
	}

	private static void closeLine(List<LexikonAmeise> ameisen, StringBuilder ameisenTypes, int index)
	{
		if((index + 1) == ameisen.size())
		{
			ameisenTypes.append(";");
		}
		else
		{
			ameisenTypes.append(",");
		}
	}

	private static void breakAfterRegions(StringBuilder ameisenTypes, LexikonAmeise lexikonAmeise)
	{
		List<String> lastTypes = Arrays.asList("Temnothorax nylanderi", "Plagiolepis pygmaea");
		if(lastTypes.contains(lexikonAmeise.getName()))
		{
			ameisenTypes.append(System.lineSeparator());
		}
	}
}
