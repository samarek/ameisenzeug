package pa.soft.processors.ameisenkurser;

import pa.soft.Main;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Formicarium;
import pa.soft.data.entities.plotter.ArbeiterListe;
import pa.soft.data.entities.plotter.KursList;
import pa.soft.data.managers.ameisenzimmer.FormicarienManager;
import pa.soft.data.managers.plotter.ArbeiterlistenManager;
import pa.soft.data.types.FormicariumType;
import pa.soft.data.types.TemplateType;
import pa.soft.processors.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BrutPlotter
{
	private static List<KursList> arbeiterinnenListe = new ArrayList<>();

	private static final String FILE_PATH ="/root/EatenByAnts/out";


	public static void run(Spieler spieler)
	{
		File brutFile;
		File anzuchtFile;
		File bis50kFile;
		File bis250kFile;
		if(Main.ARGS.containsKey("brutfile"))
		{
			brutFile = new File(Main.ARGS.get("brutfile"));
			anzuchtFile = new File(Main.ARGS.get("brutfile").replace("bruten", TemplateType.ANZUCHT.getName()));
			bis50kFile = new File(Main.ARGS.get("brutfile").replace("bruten", TemplateType.BIS50K.getName()));
			bis250kFile = new File(Main.ARGS.get("brutfile").replace("bruten", TemplateType.BIS250K.getName()));
		}
		else
		{
			brutFile = Util.createFile(FILE_PATH, "bruten");
			anzuchtFile = Util.createFile(FILE_PATH, "anzucht");
			bis50kFile = Util.createFile(FILE_PATH, "bis50k");
			bis250kFile = Util.createFile(FILE_PATH, "bis250k");
		}

		Map<String, KursList> arbeiterlisten = ArbeiterlistenManager.fetchArbeiterlisten(brutFile);
		arbeiterinnenListe = new ArrayList<>(arbeiterlisten.values());
		if(!(Main.ARGS.containsKey("mode") && Main.ARGS.get("mode").equalsIgnoreCase("justPlot")))
		{
			arbeiterinnenListe = new ArrayList<>();

			List<Formicarium> formicariumList = FormicarienManager.fetchFormicarien(spieler);
			generateArbeiterinnenListe(formicariumList, arbeiterlisten);

			Writer.saveData(brutFile, arbeiterinnenListe);
		}

		List<KursList> formicarien = arbeiterinnenListe.stream()
													   .filter(kursList->((ArbeiterListe) kursList).getFormicarium() !=
															   null)
													   .filter(kursList->!((ArbeiterListe) kursList).getFormicarium()
																									.getFormicariumType()
																									.equals(FormicariumType.MINI_ARENA))
													   .sorted(Comparator.comparing(KursList::getMaxFromList))
													   .collect(Collectors.toList());
		List<KursList> anzuchtArenen = arbeiterinnenListe.stream()
														 .filter(kursList->
																		 ((ArbeiterListe) kursList).getFormicarium() !=
																				 null)
														 .filter(kursList->((ArbeiterListe) kursList).getFormicarium()
																									 .getFormicariumType()
																									 .equals(FormicariumType.MINI_ARENA))
														 .sorted(Comparator.comparing(KursList::getMaxFromList))
														 .collect(Collectors.toList());
		List<KursList> bis50k = arbeiterinnenListe.stream()
												  .filter(kursList->((ArbeiterListe) kursList).getFormicarium() != null)
												  .filter(kursList->!((ArbeiterListe) kursList).getFormicarium()
																							   .getFormicariumType()
																							   .equals(FormicariumType.MINI_ARENA))
												  .filter(kursList->((ArbeiterListe) kursList).getFormicarium()
																							  .getAmeisenType()
																							  .getMaxPopulation() <=
														  50_000)
												  .sorted(Comparator.comparing(KursList::getMaxFromList))
												  .collect(Collectors.toList());
		List<KursList> bis250k = arbeiterinnenListe.stream()
												   .filter(kursList->((ArbeiterListe) kursList).getFormicarium() !=
														   null)
												   .filter(kursList->!((ArbeiterListe) kursList).getFormicarium()
																								.getFormicariumType()
																								.equals(FormicariumType.MINI_ARENA))
												   .filter(kursList->((ArbeiterListe) kursList).getFormicarium()
																							   .getAmeisenType()
																							   .getMaxPopulation() <=
														   250_000)
												   .sorted(Comparator.comparing(KursList::getMaxFromList))
												   .collect(Collectors.toList());

		Writer.writeHtml(brutFile, formicarien, TemplateType.ARBEITERINNENLISTE);
		Writer.writeHtml(anzuchtFile, anzuchtArenen, TemplateType.ANZUCHT);
		Writer.writeHtml(bis50kFile, bis50k, TemplateType.BIS50K);
		Writer.writeHtml(bis250kFile, bis250k, TemplateType.BIS250K);
	}

	private static void generateArbeiterinnenListe(List<Formicarium> formicariumList,
												   Map<String, KursList> arbeiterlisten)
	{
		KursList arbeiterListe;

		for(Formicarium formicarium : formicariumList)
		{
			arbeiterListe = arbeiterlisten.get(formicarium.idString());
			if(arbeiterListe != null)
			{
				arbeiterListe.addElement(formicarium.getBrut().getDrones());
			}
			else
			{
				arbeiterListe = new ArbeiterListe(formicarium);
			}
			arbeiterinnenListe.add(arbeiterListe);
		}
	}
}
