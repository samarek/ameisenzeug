package pa.soft.processors.ameisenkurser;

import pa.soft.data.entities.plotter.KursList;
import pa.soft.data.types.TemplateType;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.List;

public class TemplateProcessor
{
	private static final Integer HEIGHT = 850;
	private static final Integer WIDTH = 1600;
	private static TemplateType templateType;
	private static Double maxX = 0d, minX = Double.MAX_VALUE, maxY = 0d, minY = Double.MAX_VALUE;

	private static final DecimalFormat df = new DecimalFormat("#0.#");

	private static final String TEMPLATE = "<!DOCTYPE html>\n" + "<html lang=\"en\">\n" + "<head>\n" +
			"\t<meta charset=\"UTF-8\">\n" + "\t<meta http-equiv=\"refresh\" content=\"300\">\n" +
			"\t<title>[TITLE]</title>\n" + "\t<style>\n" + "\t\t<!--\n" + "\t\tbody {\n" + "\t\t\tcolor: white;\n" +
			"\t\t\tbackground-color: black;\n" + "\t\t}\n" + "\t\t.antbutton\n" + "\t\t{\n" +
			"\t\t\tborder: 1px solid;\n" + "\t\t\tmargin: 5px;\n" + "\t\t\tpadding: 10px;\n" +
			"\t\t\tcursor: pointer;\n" + "\t\t\tfont-family: sans-serif;\n" + "\t\t\tcolor: black;\n" +
			"\t\t\ttext-shadow: 1px 1px white;\n" + "\t\t}\n" + "\t\t/* Tootlip Code */\n" + "\t\t.note\n" + "\t\t{\n" +
			"\t\t\tposition: relative;\n" + "\t\t\tborder-top-width: medium;\n" + "\t\t}\n" + "\t\t.note .notetext\n" +
			"\t\t{\n" + "\t\t\tvisibility: hidden;\n" + "\t\t\twidth: auto;\n" + "\t\t\tbackground-color: black;\n" +
			"\t\t\tcolor: #fff;\n" + "\t\t\ttext-align: center;\n" + "\t\t\tborder-radius: 6px;\n" +
			"\t\t\tpadding: 10px;\n" + "\t\t\tposition: absolute;\n" + "\t\t\tz-index: 1;\n" +
			"\t\t\tbottom: -150%;\n" + "\t\t\tleft: 50%;\n" + "\t\t\tmargin-left: -50%;\n" + "\t\t}\n" +
			"\t\t.note:hover .notetext\n" + "\t\t{\n" + "\t\t\tvisibility: visible;\n" + "\t\t\topacity: 1;\n" +
			"\t\t}\n" + "\t\t/* Tootlip Code Ende */\n" + "\t\t-->\n" + "\t</style>\n" + "\t<script>\n" + "\t\t<!--\n" +
			"\t\tfunction toggleGraph(id)\n" + "\t\t{\n" +
			"\t\t\tvar button = document.getElementById(\"button\" + id);\n" +
			"\t\t\tvar polyline = document.getElementById(\"polyline\" + id);\n" +
			"\t\t\tif (polyline.style.display === \"none\")\n" + "\t\t\t{\n" +
			"\t\t\t\tbutton.style.backgroundSize = \"100%\";\n" + "\t\t\t\tpolyline.style.display = \"block\";\n" +
			"\t\t\t}\n" + "\t\t\telse\n" + "\t\t\t{\n" + "\t\t\t\tbutton.style.backgroundSize = \"0\";\n" +
			"\t\t\t\tpolyline.style.display = \"none\";\n" + "\t\t\t}\n" + "\t\t}\n" +
			"\t\tfunction toggleGraphBackground(id, off)\n" + "\t\t{\n" +
			"\t\t\tvar background = document.getElementById(\"polygon\" + id);\n" + "\t\t\tif (off === '0')\n" +
			"\t\t\t{\n" + "\t\t\t\tbackground.style.fillOpacity = \"0.0\";\n" + "\t\t\t}\n" + "\t\t\telse\n" +
			"\t\t\t{\n" + "\t\t\t\tbackground.style.fillOpacity = [OPACITY];\n" + "\t\t\t}\n" + "\t\t}\n" +
			"\t\t-->\n" + "\t</script>\n" + "</head>\n" + "<body>\n" + "<div style=\"display: flex;\">\n" +
			"\t<div style=\"overflow-y:scroll; height: [HEIGHT]px\">\n" + "[BUTTONS]\n" + "\t</div>\n" +
			"\t<svg width=\"[WIDTH]\" height=\"[HEIGHT]\" style=\"background-color: #ccc;\">\n" +
			"\t\t<line x1=\"50\" y1=\"25\" x2=\"50\" y2=\"[HEIGHTminus50]\" style=\"stroke:black;stroke-width:2\"/>\n" +
			"\t\t<line x1=\"10\" y1=\"[HEIGHTminus100]\" x2=\"[WIDTHminus25]\" y2=\"[HEIGHTminus100]\" style=\"stroke:black;stroke-width:2\"/>\n" +
			"[SCALEX]\n" + "[SCALEY]\n" + "[BODY]\n" + "\t</svg>\n" + "</div>\n" + "</body>\n" + "</html>";

	private static List<KursList> kursLists;

	private static KursList currentKurslist;

	public static String process(List<KursList> kursLists, TemplateType templateType)
	{
		TemplateProcessor.templateType = templateType;

		TemplateProcessor.kursLists = kursLists;
		String buttons = makeButtons();
		String body = makeBody();
		String scaleX = makeScaleX();
		String scaleY = makeScaleY();

		return TEMPLATE
				.replace("[TITLE]", TemplateProcessor.templateType.getName())
				.replace("[OPACITY]", TemplateProcessor.templateType.equals(TemplateType.PREISLISTE) ? "0.2" : "1.0")
				.replace("[BUTTONS]", buttons)
				.replace("[BODY]", body)
				.replace("[SCALEX]", scaleX)
				.replace("[SCALEY]", scaleY)
				.replace("[WIDTH]", WIDTH + "")
				.replace("[WIDTHminus25]", (WIDTH - 25) + "")
				.replace("[WIDTHminus100]", (WIDTH - 100) + "")
				.replace("[HEIGHT]", HEIGHT + "")
				.replace("[HEIGHTminus50]", (HEIGHT - 50) + "")
				.replace("[HEIGHTminus100]", (HEIGHT - 100) + "");
	}

	private static String makeButtons()
	{
		StringBuilder buttons = new StringBuilder();

		int i = 0;
		for(KursList  kursList : kursLists)
		{
			buttons.append(kursList.generateButton(++i));
		}

		return buttons.toString();
	}

	private static String makeBody()
	{
		String polyline = "\t\t<polyline id=\"polyline[ID]\" " +
				"style=\"fill:none;stroke:[COLOR];stroke-width:3\" " +
				"points=\"[POINTS]\" />\n";
		String polygon = "\t\t<polygon id=\"polygon[ID]\" " +
				"style=\"fill:[COLOR];fill-opacity:0.0\" " +
				"points=\"[POINTS]\" />\n";
		StringBuilder body = new StringBuilder();

		int i = 0;
		for(KursList  kursList : kursLists)
		{
			currentKurslist = kursList;
			++i;
			body.append(polyline.replace("[COLOR]", kursList.getColor())
							   .replace("[ID]", i + "")
							   .replace("[POINTS]", makePolylinePoints(kursList)));
			body.append(polygon.replace("[COLOR]", kursList.getColor())
								.replace("[ID]", i + "")
								.replace("[POINTS]", makePolygonPoints()));
		}

		return body.toString();
	}

	private static String makePolylinePoints(KursList preisListe)
	{
		StringBuilder points = new StringBuilder();

		Double stepWidth = (WIDTH - 100d) / generateMaxLength();
		Double stepHeight = (HEIGHT - 150d) / generateMaxValue();
		Double bottomLine = HEIGHT - 100d;

		maxX = 0d;
		minX = Double.MAX_VALUE;
		maxY = 0d;
		minY = Double.MAX_VALUE;
		int i = 0;
		for(Integer preis : preisListe.getListe())
		{
			Double x = 50 + (i++ * stepWidth);
			Double y = bottomLine - (stepHeight * preis);
			minX = Math.min(minX, x);
			maxX = Math.max(maxX, x);
			minY = Math.min(minY, y);
			maxY = Math.max(maxY, y);

			points.append(df.format(x).replace(",", "."))
				  .append(",")
				  .append(df.format(y).replace(",", "."))
				  .append(" ");
		}

		return points.toString();
	}

	private static String makePolygonPoints()
	{
		String points = "";
		Double bottomLine = HEIGHT - 100d;
		switch(templateType)
		{
			case PREISLISTE:
				points = minX + "," + maxY + " "
						+ minX + "," + minY + " "
						+ maxX + "," + minY + " "
						+ maxX + "," + maxY;
				break;
			default:
				Double stepHeight = (HEIGHT - 150d) / generateMaxValue();

				Double yPosition = bottomLine - (stepHeight * currentKurslist.getAmeisenType().getMaxPopulation());
				points = "50," + df.format(yPosition).replace(",", ".") + " "
						+ "50," + df.format(yPosition + 1).replace(",", ".")  + " "
						+ "1550," + df.format(yPosition + 1).replace(",", ".") + " "
						+ "1550," + df.format(yPosition).replace(",", ".") + " ";
				break;
		}

		return points;
	}

	private static String makeScaleY()
	{
		Integer scaleSteps = 10;
		Integer stepSize = generateMaxValue() / scaleSteps;
		Integer steps = generateMaxValue() / (stepSize == 0 ? 1 : stepSize);
		StringBuilder lines = new StringBuilder();

		String y;
		for (int i = 0; i < steps; i++)
		{
			y = df.format(50 + i * ((HEIGHT - 150d) / steps)).replace(",", ".");
			lines.append("\t\t<text style=\"font-size: small;\" x=\"4\" y=\"")
				 .append(Double.valueOf(y) + 11)
				 .append("\">")
				 .append(makeScaleCaption(generateMaxValue() - (stepSize * i)))
				 .append("</text>");

			lines.append("\t\t<line x1=\"25\" y1=\"")
				 .append(y)
				 .append("\" x2=\"50\" y2=\"")
				 .append(y)
				 .append("\" style=\"stroke:black;stroke-width:2\"/>\n");

			lines.append("\t\t<line x1=\"50\" y1=\"")
				 .append(y)
				 .append("\" x2=\"")
				 .append(WIDTH - 50)
				 .append("\" y2=\"")
				 .append(y)
				 .append("\" style=\"stroke:#aaa;stroke-width:2\"/>\n");
		}

		return lines.toString();
	}

	private static Integer generateMaxValue()
	{
		Integer maxValue = 0;

		for(KursList  kursList : kursLists)
		{
			maxValue = Math.max(maxValue, Collections.max(kursList.getListe()));
		}
		maxValue += (maxValue / 20);

		return maxValue;
	}

	private static String makeScaleCaption(Integer value)
	{
		String out = "";
		switch(templateType)
		{
			case PREISLISTE:
				Integer cents = value % 100;
				out += value / 100 + "," + (cents < 10 ? "0" + cents : cents);
				out += "&euro;";
				break;
			default:
				out += makeNumber(value);
				break;
		}

		return out;
	}

	public static String makeNumber(Integer integer)
	{
		StringBuilder out = new StringBuilder();
		while(integer >= 1_000)
		{
			StringBuilder value = new StringBuilder(integer % 1_000 + "");
			for (int i = value.length(); i < 3; i++)
			{
				value.insert(0, "0");
			}

			out.insert(0, "." + value);
			integer /= 1_000;
		}
		out.insert(0, integer);

		return out.toString();
	}

	private static String makeScaleX()
	{
		Integer steps = generateMaxLength() / 24;
		StringBuilder lines = new StringBuilder();
		String bottomLine = df.format((HEIGHT - 100d)).replace(",", ".");

		String x;
		for (int i = 1; i <= steps; i++)
		{
			x = df.format(50 + i * ((WIDTH - 100d) / steps)).replace(",", ".");

			if(steps < 20 || i % 5 == 0)
			{
				lines.append("\t\t<text x=\"")
					 .append(Double.valueOf(x) - 10)
					 .append("\" y=\"")
					 .append(Double.valueOf(bottomLine) + 35)
					 .append("\">")
					 .append(i < 10 ? "&nbsp;" + i : i)
					 .append("</text>");
			}

			lines.append("\t\t<line x1=\"")
				 .append(x)
				 .append("\" y1=\"")
				 .append(bottomLine)
				 .append("\" x2=\"")
				 .append(x)
				 .append("\" y2=\"")
				 .append(Double.valueOf(bottomLine) + 20)
				 .append("\" style=\"stroke:black;stroke-width:2\"/>\n");
		}
		lines.append("\t\t<text x=\"")
			 .append(WIDTH - 40d)
			 .append("\" y=\"")
			 .append(Double.valueOf(bottomLine) + 35)
			 .append("\">Tage</text>");

		return lines.toString();
	}

	private static Integer generateMaxLength()
	{
		Integer maxLength = 0;

		for(KursList  kursList : kursLists)
		{
			maxLength = Math.max(maxLength, kursList.getListe().size());
		}
		maxLength += Math.min((maxLength / 10), 15);

		return maxLength;
	}
}
