package pa.soft.processors.ameisenkurser;

import pa.soft.data.entities.plotter.KursList;
import pa.soft.data.types.TemplateType;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Writer
{
	public static void saveData(File file, List<KursList> kursListen)
	{
		try
		{
			FileWriter fileWriter = new FileWriter(file);
			for(KursList kursList : kursListen)
			{
				fileWriter.write(kursList.toFileString());
			}
			fileWriter.flush();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public static void writeHtml(File file, List<KursList> kursListen, TemplateType templateType)
	{
		try
		{
			String content = TemplateProcessor.process(kursListen, templateType);

			file = new File(file.getAbsoluteFile() + ".html");
			FileWriter fileWriter = new FileWriter(file);
			fileWriter.write(content);
			fileWriter.write(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
			fileWriter.flush();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
}
