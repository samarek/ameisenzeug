package pa.soft.processors.ameisenkurser;

import pa.soft.Main;
import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenshop.AnkaufPreis;
import pa.soft.data.entities.plotter.KursList;
import pa.soft.data.entities.plotter.PreisListe;
import pa.soft.data.managers.plotter.PreislistenManager;
import pa.soft.data.managers.shop.AnkaufPreisManager;
import pa.soft.data.types.TemplateType;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PreisPlotter
{
	private static List<KursList> ankaufPreisListen;

	public static void run(Spieler spieler)
	{
		File listenFile = new File("/root/AmeisenKurser/listen");
		if(Main.ARGS.containsKey("listfile"))
		{
			listenFile = new File(Main.ARGS.get("listfile"));
		}

		Map<String, KursList> preisListen = PreislistenManager.fetchPreislisten(listenFile);
		ankaufPreisListen = new ArrayList<>(preisListen.values());
		if (!(Main.ARGS.containsKey("mode") && Main.ARGS.get("mode").equalsIgnoreCase("justPlot")))
		{
			ankaufPreisListen = new ArrayList<>();
			List<AnkaufPreis> ankaufPreise = AnkaufPreisManager.fetchAnkaufPreise(spieler);

			generateAnkaufPreisListen(preisListen, ankaufPreise);
			Writer.saveData(listenFile, ankaufPreisListen);
		}

		Writer.writeHtml(listenFile, ankaufPreisListen, TemplateType.PREISLISTE);
	}

	private static void generateAnkaufPreisListen(Map<String, KursList> preisListen, List<AnkaufPreis> ankaufPreise)
	{
		PreisListe preisListe;

		for(AnkaufPreis ankaufPreis : ankaufPreise)
		{
			preisListe = (PreisListe) preisListen.get(ankaufPreis.getName());
			if(preisListe != null)
			{
				preisListe.addElement(ankaufPreis.getPreis());
			}
			else
			{
				preisListe = new PreisListe(ankaufPreis);
			}
			ankaufPreisListen.add(preisListe);
			ankaufPreisListen.sort((kursList, t1) -> (-1) * kursList.getMaxFromList().compareTo(t1.getMaxFromList()));
		}
	}
}
