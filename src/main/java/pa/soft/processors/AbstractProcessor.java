package pa.soft.processors;

import pa.soft.Main;
import pa.soft.data.entities.Spieler;
import pa.soft.data.managers.SpielerManager;
import pa.soft.processors.ameisenkurser.BrutPlotter;
import pa.soft.processors.ameisenkurser.PreisPlotter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractProcessor
{
	public static Spieler spieler;

	public void run()
	{
		String spielerName = Main.ARGS.getOrDefault("playername", "cumeartand");
		this.spieler = SpielerManager.fetchSpieler(spielerName);
		process(spieler);
	}

	abstract void process(Spieler spieler);
}
