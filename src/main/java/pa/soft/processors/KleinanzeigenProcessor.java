package pa.soft.processors;

import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.internet.details.KleinanzeigeAmeisen;
import pa.soft.http.Dispatcher;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class KleinanzeigenProcessor
{
	private static List<KleinanzeigeAmeisen> kleinanzeigenAmeisen;
	private static Predicate<KleinanzeigeAmeisen> kleinanzeigenFilter = kleinanzeigeAmeisen -> true;

	public static void process(Spieler spieler)
	{
		for(KleinanzeigeAmeisen kleinanzeigeAmeise : kleinanzeigenAmeisen)
		{
			if(kleinanzeigeAmeise.getPreis() < spieler.getGeld())
			{
				Dispatcher.buyAnt(spieler, kleinanzeigeAmeise);
				Util.log(kleinanzeigeAmeise.getKolonie().getSpezies() + " gekauft\tPreis:\t" + kleinanzeigeAmeise.getPreis() + "\tVerkäufer:\t" + kleinanzeigeAmeise.getVerkäufer());
			}
		}
	}

	public static void setKleinanzeigenAmeisen(final List<KleinanzeigeAmeisen> kleinanzeigenAmeisen)
	{
		KleinanzeigenProcessor.kleinanzeigenAmeisen = kleinanzeigenAmeisen.stream().filter(kleinanzeigenFilter).collect(Collectors.toList());
	}

	public static void setKleinanzeigenFilter(final Predicate kleinanzeigenFilter)
	{
		KleinanzeigenProcessor.kleinanzeigenFilter = kleinanzeigenFilter;
	}
}
