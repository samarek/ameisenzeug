package pa.soft.processors.logdisplayer;

import pa.soft.data.entities.Eintrag;
import pa.soft.processors.Util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LogParser
{
	public static Map<String, List<Eintrag>> parseLogFiles(String logFilePfad)
	{
		Map<String, List<Eintrag>> outputMap = new HashMap<>();

		File[] logFiles = new File(logFilePfad).listFiles();
		String key;
		List<Eintrag> log;
		String currentSection = "";
		for(File logFile : logFiles)
		{
			try
			{
				key = logFile.getName()
							 .replace("EatenByAnts_", "")
							 .replace(".log", "");
				log = new ArrayList<>();

				List<String> lines = readFile(logFile);
				StringBuilder exceptionLine = new StringBuilder();
				for(String line : lines)
				{
					if(line.contains("==="))
					{
						currentSection = line;
					}
					else if(line.contains("Exception") || !line.contains("->"))
					{
						if(line.contains("Exception ### null"))
						{
							log.add(new Eintrag(currentSection));
							log.add(new Eintrag(line));
						}
						else if(line.trim().length() == 0)
						{
							log.add(new Eintrag(currentSection));
							log.add(new Eintrag(exceptionLine.toString()));
							exceptionLine = new StringBuilder();
						}
						else
						{
							exceptionLine.append(line).append(System.lineSeparator());
						}
					}
					else
					{
						log.add(new Eintrag(line));
					}
				}
				outputMap.put(key, log);
			}
			catch(Exception e)
			{
				Util.logException(e);

				e.printStackTrace();
			}
		}

		return outputMap;
	}

	private static List<String> readFile(File logFile)
	{
		List<String> outputList = new ArrayList<>();
		try
		{
			FileReader fileReader = new FileReader(logFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);

			String line;
			while((line = bufferedReader.readLine()) != null)
			{
				outputList.add(line);
			}
		}
		catch(Exception e)
		{
			Util.logException(e);

			e.printStackTrace();
		}

		return outputList;
	}
}
