package pa.soft.processors;

import pa.soft.data.entities.Spieler;
import pa.soft.processors.workerant.SpielerProcessor;

public class Employer extends AbstractProcessor
{
	@Override
	void process(Spieler spieler)
	{
		SpielerProcessor.process(spieler);
	}
}
