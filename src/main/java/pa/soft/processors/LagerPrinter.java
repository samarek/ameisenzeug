package pa.soft.processors;

import pa.soft.data.entities.Spieler;
import pa.soft.data.entities.ameisenzimmer.Lager;
import pa.soft.data.entities.ameisenzimmer.details.Ameisen;
import pa.soft.data.managers.ameisenzimmer.LagerManager;
import pa.soft.data.types.AmeisenType;
import pa.soft.data.types.NestType;

import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

public class LagerPrinter extends AbstractProcessor
{
	private static final String ANT_TYPE = "Ameisenart";
	private static final String JUST_QUEENS = "Königinnen";
	private static final String GROWN = "Angezüchtet";
	private static final String JUST_STORED = "Ausgewachsen";
	private static final String TOTAL = "Gesamt";
	private static final String V_SEPERATOR = "|\t";
	private static final String H_SEPERATOR = "-";
	private StringBuilder output;

	@Override
	void process(final Spieler spieler)
	{
		Lager lager = LagerManager.fetchLager(spieler);

		printAmeisen(lager.getAmeisen());
	}

	private void printAmeisen(final List<Ameisen> ameisen)
	{
		output = new StringBuilder("Stored Ants" + System.lineSeparator());

		LinkedHashSet<AmeisenType> ameisenTypeSet = ameisen.stream()
														   .map(Ameisen::getAmeisenType)
														   .sorted(Comparator.comparing(AmeisenType::getName))
														   .collect(Collectors.toCollection(LinkedHashSet::new));
		output.append(addSpaces(ANT_TYPE)).append(V_SEPERATOR);
		output.append(JUST_QUEENS).append(generateSpaces(5)).append(V_SEPERATOR);
		output.append(GROWN).append(generateSpaces(4)).append(V_SEPERATOR);
		output.append(JUST_STORED).append(generateSpaces(5)).append(V_SEPERATOR);
		output.append(TOTAL).append(System.lineSeparator());
		output.append(generateLineSeparator());

		generateSeparateTypes(ameisen, ameisenTypeSet);

		output.append(generateLineSeparator());

		String total = TOTAL + " (Arten: " + ameisenTypeSet.size() + ")";

		output.append(addSpaces(total))
			  .append(V_SEPERATOR);
		generateLineValues(ameisen);

		Util.log(output.toString());
	}

	private void generateSeparateTypes(final List<Ameisen> ameisen, final LinkedHashSet<AmeisenType> ameisenTypeSet)
	{

		for(AmeisenType ameisenType : ameisenTypeSet)
		{
			List<Ameisen> typedAnts = ameisen.stream()
											 .filter(ameisen1 ->
															 ameisen1.getAmeisenType().equals(ameisenType))
											 .collect(Collectors.toList());
			output.append(ameisenType.getLogName())
				  .append(V_SEPERATOR);
			generateLineValues(typedAnts);
		}
	}

	private void generateLineValues(final List<Ameisen> ameisen)
	{
		output.append(generateSpaces(JUST_QUEENS.length() / 2))
			  .append(calculateQueenCount(ameisen))
			  .append(generateSpaces(JUST_QUEENS.length() / 2))
			  .append(V_SEPERATOR);
		output.append(generateSpaces(GROWN.length() / 2))
			  .append(calculateGrownCount(ameisen))
			  .append(generateSpaces(GROWN.length() / 2))
			  .append(V_SEPERATOR);
		output.append(generateSpaces(JUST_STORED.length() / 2))
			  .append(calculateStoredCount(ameisen))
			  .append(generateSpaces(JUST_STORED.length() / 2))
			  .append(V_SEPERATOR);
		output.append(generateSpaces(TOTAL.length() / 2))
			  .append(ameisen.size())
			  .append(System.lineSeparator());
	}

	private String calculateQueenCount(final List<Ameisen> typedAnts)
	{
		long count = typedAnts.stream()
							  .filter(ameisen ->
											  ameisen.getNestType() != null
													  && ameisen.getNestType().equals(NestType.TEST_TUBE)
													  && ameisen.getDrones() == 0)
							  .count();

		return addSpaces(5, count + "");
	}

	private String calculateGrownCount(final List<Ameisen> typedAnts)
	{
		long count = typedAnts.stream()
							  .filter(ameisen ->
											  ameisen.getNestType() != null
													  && ameisen.getNestType().equals(NestType.TEST_TUBE)
													  && ameisen.getDrones() > 0)
							  .filter(ameisen ->
											  !ameisen.getDrones().equals(ameisen.getAmeisenType().getMaxPopulation()))
							  .count();

		return addSpaces(5, count + "");
	}

	private String calculateStoredCount(final List<Ameisen> typedAnts)
	{
		long count = typedAnts.stream()
							  .filter(ameisen ->
										ameisen.getDrones().equals(ameisen.getAmeisenType().getMaxPopulation()))
							  .count();

		return addSpaces(5, count + "");
	}

	private String generateLineSeparator()
	{
		StringBuilder lineSeparator = new StringBuilder();
		String outputString = output.toString();
		int beginIndex = outputString.indexOf(System.lineSeparator());
		int endIndex = outputString.indexOf(System.lineSeparator(), beginIndex + 1);
		endIndex = endIndex < 0 ? outputString.length() : endIndex;

		outputString = outputString.substring(beginIndex, endIndex).trim();
		int lineLength = 12 + outputString.length();
		for (int i = 0; i < lineLength; i++)
		{
			lineSeparator.append(H_SEPERATOR);
		}

		return lineSeparator + System.lineSeparator();
	}

	private String addSpaces(String text)
	{
		return addSpaces(30, text);
	}

	private String addSpaces(Integer spaceCount, String text)
	{
		Integer spaceFaktor = spaceCount - text.length();

		return text + generateSpaces(spaceFaktor);
	}

	private String generateSpaces(final Integer spaceFaktor)
	{
		StringBuilder spaces = new StringBuilder();
		for(int i = 0; i < spaceFaktor; i++)
		{
			spaces.append(' ');
		}

		return spaces.toString();
	}
}
